;
if (!Function.prototype.bind) {
    Function.prototype.bind = function(a) {
        if (typeof this !== "function") {
            throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable")
        }
        var e = Array.prototype.slice.call(arguments, 1),
            d = this,
            b = function() {},
            c = function() {
                return d.apply(this instanceof b && a ? this : a, e.concat(Array.prototype.slice.call(arguments)))
            };
        b.prototype = this.prototype;
        c.prototype = new b();
        return c
    }
}
var g_Cookies = {
    getItem: function(a) {
        if (!a || !this.hasItem(a)) {
            return null
        }
        return unescape(document.cookie.replace(new RegExp("(?:^|.*;\\s*)" + escape(a).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*"), "$1"))
    },
    setItem: function(d, g, c, b, a, e) {
        if (!d || /^(?:expires|max\-age|path|domain|secure)$/i.test(d)) {
            return
        }
        var f = "";
        if (c) {
            switch (c.constructor) {
                case Number:
                    f = c === Infinity ? "; expires=Tue, 19 Jan 2038 03:14:07 GMT" : "; max-age=" + c;
                    break;
                case String:
                    f = "; expires=" + c;
                    break;
                case Date:
                    f = "; expires=" + c.toGMTString();
                    break
            }
        }
        document.cookie = escape(d) + "=" + escape(g) + f + (a ? "; domain=" + a : "") + (b ? "; path=" + b : "") + (e ? "; secure" : "")
    },
    removeItem: function(b, a) {
        if (!b || !this.hasItem(b)) {
            return
        }
        document.cookie = escape(b) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (a ? "; path=" + a : "")
    },
    hasItem: function(a) {
        return (new RegExp("(?:^|;\\s*)" + escape(a).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie)
    },
    keys: function() {
        var a = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
        for (var b = 0; b < a.length; b++) {
            a[b] = unescape(a[b])
        }
        return a
    }
};
var g_Sub = {};
g_Sub.selectable = {
    filter: ".itemMember",
    cancel: "a",
    selecting: function(a, b) {
        if (g_editmode) {
            var c = $(b.selecting);
            if (c.length) {
                onItemClick(c)
            }
        }
    },
    unselecting: function(a, b) {
        if (g_editmode) {
            var c = $(b.unselecting);
            if (c.length) {
                onItemClick(c)
            }
        }
    },
    stop: function(a, b) {
        $("#wrapper .itemMember").removeClass("ui-selected")
    }
};
g_Sub.draggable = {
    distance: 5,
    zIndex: 60,
    refreshPositions: true,
    handle: ".coverImage",
    start: function(d, b) {
        var a = b.helper.get(0);
        var c = b.helper.context;
        if ($(".fileCheckbox", c).hasClass("fileCheckbox_sel")) {
            a.selectedFiles = $(".itemMember .fileCheckbox_sel").closest(".box")
        } else {
            a.selectedFiles = $(c)
        }
        a.hasFolder = false;
        a.hasFile = false;
        a.selectedFiles.each(function() {
            $(this).css("opacity", 0.5);
            $(this).css("filter", "Alpha(Opacity=50)");
            $(this).addClass("dragging");
            if (this.selectedFiles == null) {
                $(this).addClass("dragging-hidden");
                $(this).animate({
                    left: c.orgLeft,
                    top: c.orgTop
                }, 100);
                $(this).draggable("disable");
                $(this).droppable("disable")
            }
            if ($(this).hasClass("isFolder")) {
                a.hasFolder = true
            } else {
                a.hasFile = true
            }
        })
    },
    drag: function(c, b) {
        var a = b.helper.get(0);
        a.selectedFiles.each(function() {
            if (this.selectedFiles == null) {
                $(this).not(":animated").css("top", b.position.top);
                $(this).not(":animated").css("left", b.position.left)
            }
        })
    },
    stop: function(c, b) {
        if (this.enabled == false) {
            return
        }
        var a = b.helper.get(0);
        $("#bookShelfContents .box").removeClass("dragging-hidden");
        setItemPosition();
        a.selectedFiles.each(function() {
            $(this).stop().animate({
                left: this.orgLeft,
                top: this.orgTop
            }, 200);
            var d = this;
            setTimeout(function() {
                $(d).removeClass("dragging");
                $(d).css("opacity", 1);
                $(d).css("filter", "Alpha(Opacity=100)");
                $(d).css("height", "")
            }, 200);
            if (this.selectedFiles == null) {
                $(this).draggable("enable");
                $(this).droppable("enable")
            }
        });
        a.selectedFiles = null;
        if (q_orderChanged) {
            q_orderChanged = false;
            setItemOrder()
        }
    }
};

function copy_clipboard(b) {
    if (window.clipboardData) {
        var a = $(b).text();
        window.clipboardData.setData("Text", a)
    } else {
        $(b).select();
        document.execCommand("copy")
    }
}
var g_gridObj = {};

function createFolderObject(d) {
    var c = $("#folderTemplate");
    var b = $(c).clone(true);
    $("#folderAddSpace").before(b);
    $(b).attr("id", "objFolder_" + d.FOLSEQ);
    $(b).addClass("itemMember");
    $(b).css("display", "block");
    $(".fileName, .fileNameForList", b).text(d.FOLNM);
    $(".atrVER", b).text(d.FOLPOSTDATE.split("-").join("/"));
    $(".atrDETAIL", b).text(d.FOLDETAIL);
    if (d.FILMODE < 200) {
        $("div.accessFailure", b).css("display", "");
        $(b).removeAttr("onclick")
    }
    var e = Math.floor(new Date(d.FOLPOSTDATE.split("-").join("/")).getTime() / 86400000);
    var a = Math.floor(new Date().getTime() / 86400000) - g_newLimit;
    if (g_newLimit > 0 && e > a) {
        $(".newIcon", b).css("display", "")
    }
    if (d.RECOMMEND > 0) {
        $(".recIcon", b).css("display", "")
    }
    $(".coverImage", b).attr("src", "./folderimg/folder" + ("0" + d.FOLIMG).slice(-2) + ".png");
    $(b).droppable({
        over: function(j, i) {
            if (g_editmode) {
                if (!g_recoAdmin) {
                    $(".coverImage", this).stop(false, true).animate({
                        top: -30
                    }, 100);
                    this.isDroppable = true
                }
                var g = i.helper.get(0);
                if (g.hasFolder && !g.hasFile && g_sort == 0) {
                    var h = i.helper.context;
                    var k = this;
                    var f = null;
                    f = function() {
                        var m = (parseInt($(h).css("left")) + parseInt($(h).css("width")) / 2) - (parseInt($(k).css("left")) + parseInt($(k).css("width")) / 2);
                        var p = (parseInt($(h).css("top")) + parseInt($(h).css("height")) / 2) - (parseInt($(k).css("top")) + parseInt($(k).css("height")) / 2);
                        var o = Math.sqrt(Math.pow(m, 2) + Math.pow(p, 2));
                        if (o < 35) {
                            $(".coverImage", k).stop(false, true).animate({
                                top: 0
                            }, 100);
                            k.isDroppable = false;
                            var n = parseInt($("#bookShelfContents").css("width"));
                            var q = k.orgTop * n + k.orgLeft;
                            var l = h.orgTop * n + h.orgLeft;
                            g.selectedFiles.each(function() {
                                if (q > l) {
                                    $(k).after($(this))
                                } else {
                                    $(k).before($(this))
                                }
                            });
                            q_orderChanged = true;
                            setItemPosition()
                        } else {
                            k.chkTimer = setTimeout(f, 100)
                        }
                    };
                    f()
                }
            }
        },
        out: function(g, f) {
            if (g_editmode) {
                clearTimeout(this.chkTimer);
                $(".coverImage", this).stop(false, true).animate({
                    top: 0
                }, 100);
                this.isDroppable = false
            }
        },
        drop: function(j, i) {
            if (g_editmode) {
                clearTimeout(this.chkTimer);
                if (this.isDroppable) {
                    var h = i.helper.get(0);
                    var g = new Array();
                    var k = new Array();
                    var f = parseInt($(this).attr("id").split("_")[1]);
                    h.selectedFiles.each(function() {
                        this.enabled = false;
                        var m = this;
                        aryKey = $(this).attr("id").split("_");
                        var l = parseInt(aryKey[1]);
                        if (aryKey[0] == "objFolder") {
                            g.push(l)
                        } else {
                            k.push(l)
                        }
                        $(this).fadeOut(200, function() {
                            $(m).empty();
                            $(m).remove();
                            if (this.selectedFiles != null) {
                                moveItem(g_folseq, f, g, k, 0);
                                setItemPosition()
                            }
                        })
                    })
                }
                $(".coverImage", this).stop(false, true).animate({
                    top: 0
                }, 100);
                this.isDroppable = false
            }
        }
    });
    return b
}

function createFileObject(f) {
    var c = $("#fileTemplate");
    var b = $(c).clone(true);
    $("#fileAddSpace").before(b);
    $(b).attr("id", "objFile_" + f.FILSEQ);
    $(b).addClass("itemMember");
    var e = /(mp4|m4v|m4a|webm|wmv|wma|avi|asf|mov|3gp|3g2|mov|flv|rm|ogv|ogg|mpeg|mpg|mp3|wav|aif)$/i;
    if (e.test(f.FILEXT)) {
        $(b).addClass("movieFile");
        $(".labelNum", b).text("蜀咲函譎る俣");
        $(".atrPAGE", b).text(f.FILPAGE + "遘�")
    } else {
        $(".atrPAGE", b).text(f.FILPAGE + "繝壹�繧ｸ")
    }
    $(b).css("display", "block");
    $(".fileName, .fileNameForList", b).text(f.FILTITLE);
    $(".atrVER", b).text(f.FILEDITDATE.split("-").join("/"));
    $(".atrDETAIL", b).text(f.FILDETAIL);
    if (f.FILMODE < 200) {
        $("div.accessFailure", b).css("display", "");
        $(b).removeAttr("onclick")
    }
    if (f.F_FILSTTS > 0) {
        $(".coverImage", b).attr("src", "./images/loading.gif")
    } else {
        $(".coverImage", b).attr("src", g_dataURL + f.FILCD + "/cover.png?v=" + f.FILFVER);
        var d = Math.floor(new Date(f.FILEDITDATE.split("-").join("/")).getTime() / 86400000);
        var a = Math.floor(new Date().getTime() / 86400000) - g_newLimit;
        if (g_newLimit > 0 && d > a) {
            $(".newIcon", b).css("display", "")
        }
        if (f.RECOMMEND > 0) {
            $(".recIcon", b).css("display", "")
        }
    }
    $(".coverImage", b).error(function() {
        $(this).attr("src", "./images/notLoading.png");
        var g = $(this).closest(".box");
        $(g).addClass("isErrorFile");
        return false
    });
    $(b).droppable({
        over: function(m, j) {
            if (g_editmode) {
                var i = j.helper.get(0);
                if (i.hasFolder || g_sort > 0) {
                    return
                }
                var l = j.helper.context;
                var h = parseInt($("#bookShelfContents").css("width"));
                var k = this.orgTop * h + this.orgLeft;
                var g = l.orgTop * h + l.orgLeft;
                var n = this;
                i.selectedFiles.each(function() {
                    if (k > g) {
                        $(n).after($(this))
                    } else {
                        $(n).before($(this))
                    }
                });
                q_orderChanged = true;
                setItemPosition()
            }
        }
    });
    if (f.F_FILSTTS > 0) {
        $(b).addClass("needStatusCheck");
        $(b).attr("filstts", f.F_FILSTTS);
        $(b).attr("filcd", f.FILCD)
    }
    if (g_timer == null) {
        g_timer = setInterval("checkFileStatus()", 1000)
    }
    return b
}

function setItemPosition(a) {
    a === undefined ? 300 : a;
    var k = parseInt($("#bookShelfContents").css("width"));
    var t = parseInt($("#bottomBack").height());
    var m = $("#bookShelfContents .itemMember").toArray();
    var p = parseInt($("#bookShelfContents").css("margin-left"));
    var l = {
        filCnt: 0,
        folCnt: 0
    };
    var g = false;
    if (g_viewStyle.style == 0) {
        var e = 180;
        var b = 135;
        var q = 20;
        var j = 150;
        var h = 0;
        var f = 0;
        var s = parseInt(k / (j + q));
        var o = (k - (s * j)) / s;
        $(".box").css("width", j);
        for (var r = 0; r < m.length; r++) {
            if ($(m[r]).hasClass("dragging-hidden")) {
                g = true;
                continue
            }
            var d = parseInt($(m[r]).css("width"));
            var c = h * (d + o) + (o / 2);
            if (c + d > k) {
                h = 0;
                f++;
                c = h * (d + o) + (o / 2)
            }
            c += p;
            var n = f * e + b;
            m[r].orgLeft = c;
            m[r].orgTop = n;
            if (!$(m[r]).hasClass("dragging")) {
                if (parseInt($(m[r]).css("top")) != n) {
                    $(m[r]).droppable("disable");
                    $(m[r]).css("opacity", 1);
                    $(m[r]).css("filter", "Alpha(Opacity=100)")
                }
                $(m[r]).stop().animate({
                    left: c + "px",
                    top: n + "px"
                }, a, null, function() {
                    $(this).droppable("enable")
                })
            }
            h++;
            if ($(m[r]).hasClass("isFile")) {
                l.filCnt++
            }
            if ($(m[r]).hasClass("isFolder")) {
                l.folCnt++
            }
        }
    } else {
        var e = 120;
        var b = 110;
        var q = 20;
        var j = 150;
        var h = 0;
        var f = 0;
        var s = 1;
        var o = 0;
        $(".box").css("width", $("#bookShelfContents").css("width"));
        for (var r = 0; r < m.length; r++) {
            if ($(m[r]).hasClass("dragging-hidden")) {
                g = true;
                continue
            }
            var d = k;
            var c = h * (d + o) + (o / 2);
            if (c + d > k) {
                h = 0;
                f++;
                c = h * (d + o) + (o / 2)
            }
            c += p;
            var n = f * e + b;
            m[r].orgLeft = c;
            m[r].orgTop = n;
            if (!$(m[r]).hasClass("dragging")) {
                if (parseInt($(m[r]).css("top")) != n) {
                    $(m[r]).droppable("disable");
                    $(m[r]).css("opacity", 1);
                    $(m[r]).css("filter", "Alpha(Opacity=100)")
                }
                $(m[r]).stop().animate({
                    left: c + "px",
                    top: n + "px"
                }, a, null, function() {
                    $(this).droppable("enable")
                })
            }
            h++;
            if ($(m[r]).hasClass("isFile")) {
                l.filCnt++
            }
            if ($(m[r]).hasClass("isFolder")) {
                l.folCnt++
            }
        }
    }
    if (!g) {
        $("#bottom .bottomInfo").text("繝輔か繝ｫ繝:" + l.folCnt + "莉ｶ 繝輔ぃ繧､繝ｫ:" + l.filCnt + "莉ｶ")
    }
    $("#bookShelfContents").css("height", (f + 1) * e + b + t + "px");
    if (g_treeView.visible) {
        $(".leftFlexColumn").css("bottom", t)
    }
}

function checkFileStatus() {
    var aryItem = $(".needStatusCheck").toArray();
    if (aryItem.length > 0) {
        jQuery.ajax({
            url: "./ajax/ajax_CheckFileStatus.php?folpseq=" + g_folseq,
            type: "get",
            success: function(data) {
                var arySTTS = eval("(" + data + ")");
                for (var i = 0; i < aryItem.length; i++) {
                    var key = $(aryItem[i]).attr("id");
                    if (key in arySTTS) {
                        if (arySTTS[key]["F_FILSTTS"] == 3) {
                            $(".openDetail", aryItem[i]).css("display", "none");
                            $(".fileNameBack, .progressBar", aryItem[i]).css("display", "block");
                            $(".progressBar", aryItem[i]).css("background-position", "-140px 0")
                        } else {
                            if (arySTTS[key]["F_FILSTTS"] == 4) {
                                $(".openDetail", aryItem[i]).css("display", "none");
                                $(".fileNameBack, .progressBar", aryItem[i]).css("display", "block");
                                var total = parseInt(arySTTS[key]["FILPAGE"]);
                                if (total > 0) {
                                    var progress = -140 + (arySTTS[key]["F_FILRETCODE"] / total) * 140;
                                    $(".progressBar", aryItem[i]).css("background-position", progress + "px 0")
                                }
                            } else {
                                if (arySTTS[key]["F_FILSTTS"] < 0) {
                                    $(".openDetail", aryItem[i]).css("display", "block");
                                    $(".progressBar", aryItem[i]).css("display", "none");
                                    if (g_viewStyle.style == 1) {
                                        $(".fileNameBack", aryItem[i]).css("display", "none")
                                    }
                                    $(aryItem[i]).removeClass("needStatusCheck");
                                    $(aryItem[i]).addClass("isErrorFile");
                                    $(".coverImage", aryItem[i]).attr("src", "./images/notLoading.png")
                                }
                            }
                        }
                    } else {
                        $(".openDetail", aryItem[i]).css("display", "block");
                        $(".progressBar", aryItem[i]).css("display", "none");
                        if (g_viewStyle.style == 1) {
                            $(".fileNameBack", aryItem[i]).css("display", "none")
                        }
                        var filcd = $(aryItem[i]).attr("filcd");
                        $(aryItem[i]).removeClass("needStatusCheck");
                        $(".coverImage", aryItem[i]).attr("src", g_dataURL + filcd + "/cover.png");
                        if (g_newLimit > 0) {
                            $(".newIcon", aryItem[i]).css("display", "")
                        }
                    }
                }
            }
        })
    } else {
        clearInterval(g_timer);
        g_timer = null
    }
}

function onItemClick(c) {
    if ($(c).hasClass("dragging")) {
        return false
    }
    if (g_editmode) {
        $(".fileCheckbox", $(c)).toggleClass("fileCheckbox_sel")
    } else {
        var b = $(c).attr("id").split("_");
        var a = parseInt(b[1]);
        if (b[0] == "objFolder") {
            location.href = "./?folseq=" + a
        } else {
            if (!($(c).hasClass("needStatusCheck") || $(c).hasClass("isErrorFile"))) {
                if ($(c).hasClass("movieFile")) {
                    openMoviePlayer(a)
                } else {
                    openEBook5(a)
                }
            }
        }
    }
    return false
}

function onItemInfoClick(d, c) {
    $.Event(c).stopPropagation();
    if ($(d).closest(".box").hasClass("dragging")) {
        return false
    }
    var b = $(d).closest(".box").attr("id").split("_");
    var a = parseInt(b[1]);
    if (b[0] == "objFolder") {
        location.href = "./folderInfo.php?folseq=" + a
    } else {
        if (!($(d).closest(".box").hasClass("needStatusCheck"))) {
            location.href = "./fileInfo.php?filseq=" + a
        }
    }
    return false
}

function editMode(c) {
    $(".itemMember .fileCheckbox").removeClass("fileCheckbox_sel");
    $("#buttonAllSelect a").text("蜈ｨ驕ｸ謚�");
    var d = $("#buttonStartEdit a");
    var a = $(".itemMember").toArray();
    if (d.text() == "邱ｨ髮�" || c == true) {
        $("#bottom .isEditButton").css("display", "block");
        $(".itemMember .fileCheckbox").css("display", "block");
        $("#wrapper").selectable(g_Sub.selectable);
        for (var b = 0; b < a.length; b++) {
            $(a[b]).draggable(g_Sub.draggable)
        }
        d.text("螳御ｺ�");
        g_editmode = true
    } else {
        $("#bottom .isEditButton").css("display", "none");
        $(".itemMember .fileCheckbox").css("display", "none");
        for (var b = 0; b < a.length; b++) {
            $(a[b]).draggable("destroy")
        }
        $("#wrapper").selectable("destroy");
        d.text("邱ｨ髮�");
        g_editmode = false
    }
}

function setRecommend() {
    var a = $(".itemMember .fileCheckbox_sel").closest(".box").toArray();
    if (a.length == 0) {
        $.Zebra_Dialog("縺翫☆縺吶ａ縺ｫ蜉�縺医ｋ蟇ｾ雎｡繧帝∈謚槭＠縺ｦ縺上□縺輔＞", {
            type: "error",
            title: "縺翫☆縺吶ａ縺ｫ蜉�縺医ｋ"
        })
    } else {
        $.Zebra_Dialog("繝√ぉ繝�け縺励◆ " + a.length + " 莉ｶ縺ｮ繧｢繧､繝�Β繧偵♀縺吶☆繧√↓險ｭ螳壹＠縺ｾ縺吶°��", {
            type: "warning",
            title: "縺翫☆縺吶ａ縺ｫ蜉�縺医ｋ",
            buttons: ["OK", "繧ｭ繝｣繝ｳ繧ｻ繝ｫ"],
            onClose: function(c) {
                if (c == "OK") {
                    var e = new Array();
                    var f = new Array();
                    for (var d = 0; d < a.length; d++) {
                        var g = a[d];
                        if ($(".recIcon", $(g)).css("display") == "none") {
                            aryKey = $(g).attr("id").split("_");
                            var b = parseInt(aryKey[1]);
                            if (aryKey[0] == "objFolder") {
                                e.push(b)
                            } else {
                                f.push(b)
                            }
                            $(".recIcon", $(g)).css("display", "")
                        }
                    }
                    if (f.length > 0 || e.length > 0) {
                        jQuery.ajax({
                            url: "./ajax/ajax_SetRecommend.php?set",
                            type: "post",
                            data: {
                                aryFolSeq: e,
                                aryFilSeq: f
                            },
                            success: function(h) {}
                        })
                    }
                    setItemPosition()
                }
            }
        })
    }
}

function unsetRecommend(b) {
    var a = $(".itemMember .fileCheckbox_sel").closest(".box").toArray();
    if (a.length == 0) {
        $.Zebra_Dialog("縺翫☆縺吶ａ繧定ｧ｣髯､縺吶ｋ蟇ｾ雎｡繧帝∈謚槭＠縺ｦ縺上□縺輔＞", {
            type: "error",
            title: "縺翫☆縺吶ａ縺ｮ隗｣髯､"
        })
    } else {
        $.Zebra_Dialog("繝√ぉ繝�け縺励◆ " + a.length + " 莉ｶ縺ｮ縺翫☆縺吶ａ繧｢繧､繝�Β繧定ｧ｣髯､縺励∪縺吶°��", {
            type: "warning",
            title: "縺翫☆縺吶ａ縺ｮ隗｣髯､",
            buttons: ["OK", "繧ｭ繝｣繝ｳ繧ｻ繝ｫ"],
            onClose: function(d) {
                if (d == "OK") {
                    var f = new Array();
                    var g = new Array();
                    for (var e = 0; e < a.length; e++) {
                        var h = a[e];
                        aryKey = $(h).attr("id").split("_");
                        var c = parseInt(aryKey[1]);
                        if (aryKey[0] == "objFolder") {
                            f.push(c)
                        } else {
                            g.push(c)
                        }
                        if (b) {
                            $(a[e]).empty();
                            $(a[e]).remove()
                        } else {
                            $(".recIcon", $(a[e])).css("display", "none")
                        }
                    }
                    if (g.length > 0 || f.length > 0) {
                        jQuery.ajax({
                            url: "./ajax/ajax_SetRecommend.php?unset",
                            type: "post",
                            data: {
                                aryFolSeq: f,
                                aryFilSeq: g
                            },
                            success: function(i) {}
                        })
                    }
                    setItemPosition()
                }
            }
        })
    }
}

function allSelect() {
    var a = $("#buttonAllSelect a");
    $(".itemMember .fileCheckbox").removeClass("fileCheckbox_sel");
    if (a.text() == "蜈ｨ驕ｸ謚�") {
        $(".itemMember .fileCheckbox").addClass("fileCheckbox_sel");
        a.text("蜈ｨ隗｣髯､")
    } else {
        a.text("蜈ｨ驕ｸ謚�")
    }
}

function makeFolder(folseq) {
    jQuery.ajax({
        url: "./ajax/ajax_MakeFolder.php?folpseq=" + folseq,
        type: "get",
        success: function(data) {
            if (data != "err") {
                var aryFolderInfo = eval("(" + data + ")");
                var newFolder = createFolderObject(aryFolderInfo);
                $(newFolder).css("left", 0);
                $(newFolder).css("top", 0);
                $(".fileCheckbox", newFolder).css("display", "");
                $(newFolder).draggable(g_Sub.draggable);
                setItemPosition();
                g_treeView.redraw()
            }
        }
    })
}

function deleteSelectedItem() {
    var a = $(".itemMember .fileCheckbox_sel").closest(".box").toArray();
    if (a.length == 0) {
        $.Zebra_Dialog("蜑企勁縺ｮ蟇ｾ雎｡繧帝∈謚槭＠縺ｦ縺上□縺輔＞", {
            type: "error",
            title: "繧｢繧､繝�Β縺ｮ蜑企勁"
        })
    } else {
        $.Zebra_Dialog("繝√ぉ繝�け縺励◆ " + a.length + " 莉ｶ縺ｮ繧｢繧､繝�Β繧貞炎髯､縺励∪縺吶°��", {
            type: "warning",
            title: "繧｢繧､繝�Β縺ｮ蜑企勁",
            buttons: ["OK", "繧ｭ繝｣繝ｳ繧ｻ繝ｫ"],
            onClose: function(c) {
                if (c == "OK") {
                    var e = new Array();
                    var f = new Array();
                    for (var d = 0; d < a.length; d++) {
                        var g = a[d];
                        aryKey = $(g).attr("id").split("_");
                        var b = parseInt(aryKey[1]);
                        if (aryKey[0] == "objFolder") {
                            e.push(b)
                        } else {
                            f.push(b)
                        }
                        $(a[d]).empty();
                        $(a[d]).remove()
                    }
                    if (f.length > 0 || e.length > 0) {
                        jQuery.ajax({
                            url: "./ajax/ajax_DeleteFile.php?folpseq=" + g_folseq,
                            type: "post",
                            data: {
                                aryFolSeq: e,
                                aryFilSeq: f
                            },
                            success: function(h) {
                                if (e.length > 0) {
                                    g_treeView.redraw()
                                }
                            }
                        })
                    }
                    setItemPosition()
                }
            }
        })
    }
}

function showMoveTargetDlg(c) {
    var a = $(".itemMember .fileCheckbox_sel").closest(".box").toArray();
    if (a.length == 0) {
        $.Zebra_Dialog("遘ｻ蜍輔�蟇ｾ雎｡繧帝∈謚槭＠縺ｦ縺上□縺輔＞", {
            type: "error",
            title: "繧｢繧､繝�Β縺ｮ遘ｻ蜍�"
        })
    } else {
        var e = new Array();
        for (var d = 0; d < a.length; d++) {
            var f = a[d];
            aryKey = $(f).attr("id").split("_");
            var b = parseInt(aryKey[1]);
            if (aryKey[0] == "objFolder") {
                e.push(b)
            }
        }
        $.ajax({
            url: "./ajax/ajax_SelectMoveTarget.php?folseq=" + c,
            type: "post",
            data: {
                arySeq: e
            },
            success: function(g) {
                $.fancybox({
                    content: g,
                    fitToView: true,
                    autoSize: false,
                    autoHeight: true,
                    minWidth: 240,
                    maxWidth: "90%",
                    height: 520,
                    closeClick: false,
                    openEffect: "elastic",
                    closeEffect: "elastic",
                    scrolling: "auto",
                    arrows: false,
                    nextClick: false,
                    mouseWheel: false,
                    helpers: {
                        overlay: {
                            speedIn: 300,
                            speedOut: 300,
                            opacity: 0.5
                        }
                    }
                })
            }
        })
    }
}

function moveSelectedItem(g, e) {
    var a = $(".itemMember .fileCheckbox_sel").closest(".box").toArray();
    var f = new Array();
    var b = new Array();
    for (var d = 0; d < a.length; d++) {
        var h = a[d];
        aryKey = $(h).attr("id").split("_");
        var c = parseInt(aryKey[1]);
        if (aryKey[0] == "objFolder") {
            f.push(c)
        } else {
            if (aryKey[0] == "objFile") {
                b.push(c)
            }
        }
        if (e == 0) {
            $(h).empty();
            $(h).remove()
        }
    }
    $.fancybox.close();
    moveItem(g_folseq, g, f, b, e);
    setItemPosition()
}

function moveItem(b, e, d, a, c) {
    jQuery.ajax({
        url: "./ajax/ajax_MoveItem.php?cmd=" + c + "&src=" + b + "&dst=" + e,
        type: "post",
        dataType: "json",
        data: {
            aryFolseq: d,
            aryFilseq: a
        },
        success: function(g) {
            var f = g.message;
            if (c == 0) {
                $.Zebra_Dialog("驕ｸ謚槭＆繧後◆繧｢繧､繝�Β繧堤ｧｻ蜍輔＠縺ｾ縺励◆<br /><br />遘ｻ蜍募���<br /><a href='./?folseq=" + e + "'>" + f + "</a>", {
                    type: "confirmation",
                    title: "繧｢繧､繝�Β縺ｮ遘ｻ蜍包ｼ剰､�｣ｽ",
                    buttons: ["OK"]
                })
            } else {
                if (c == 1) {
                    $.Zebra_Dialog("驕ｸ謚槭＆繧後◆繝輔か繝ｫ繝髫主ｱ､讒矩�繧定､�｣ｽ縺励∪縺励◆<br /><br />隍�｣ｽ蜈茨ｼ�<br /><a href='./?folseq=" + e + "'>" + f + "</a>", {
                        type: "confirmation",
                        title: "繧｢繧､繝�Β縺ｮ遘ｻ蜍包ｼ剰､�｣ｽ",
                        buttons: ["OK"]
                    })
                }
            }
            g_treeView.redraw()
        }
    })
}

function setItemOrder() {
    var a = $(".itemMember").toArray();
    var e = new Array();
    var b = new Array();
    for (var d = 0; d < a.length; d++) {
        var f = a[d];
        aryKey = $(f).attr("id").split("_");
        var c = parseInt(aryKey[1]);
        if (aryKey[0] == "objFolder") {
            e.push(c)
        } else {
            if (aryKey[0] == "objFile") {
                b.push(c)
            }
        }
    }
    if (g_recoAdmin) {
        jQuery.ajax({
            url: "./ajax/ajax_SetRecommOrder.php",
            type: "post",
            data: {
                aryFolseq: e,
                aryFilseq: b
            },
            success: function(g) {}
        })
    } else {
        jQuery.ajax({
            url: "./ajax/ajax_SetItemOrder.php?folpseq=" + g_folseq,
            type: "post",
            data: {
                aryFolseq: e,
                aryFilseq: b
            },
            success: function(g) {}
        })
    }
}

function initUploader() {
    $("#fileupload").fileupload();
    $("#fileupload").fileupload("option", {
        sequentialUploads: true,
        minFileSize: 1,
        maxFileSize: 512000000,
        acceptFileTypes: /(\.|\/)(pdf|mp4|m4v|m4a|webm|wmv|wma|avi|asf|mov|3gp|3g2|mov|flv|rm|ogv|ogg|mpeg|mpg|mp3|wav|aif)$/i
    });
    $("#fileupload").bind("fileuploadadd", function(b, a) {
        $.fancybox.open("#blueimpUploader", {
            fitToView: true,
            autoSize: false,
            minWidth: 320,
            maxWidth: "90%",
            scrolling: "auto",
            closeClick: false,
            openEffect: "none",
            closeEffect: "none",
            helpers: {
                overlay: {
                    speedIn: 300,
                    speedOut: 300,
                    opacity: 0.5
                }
            }
        })
    }).bind("fileuploadsubmit", function(b, a) {
        $.each(a.files, function(c, d) {})
    }).bind("fileuploaddone", function(b, a) {
        $.each(a.result.files, function(c, d) {
            newFileMoveToQueue(d)
        })
    }).bind("process", function(b, a) {});
    $(".blueimpUploader").fancybox({
        fitToView: true,
        autoSize: false,
        minWidth: 320,
        maxWidth: "90%",
        scrolling: "auto",
        closeClick: false,
        openEffect: "elastic",
        closeEffect: "elastic",
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            }
        }
    })
}

function newFileMoveToQueue(file) {
    jQuery.ajax({
        url: "./ajax/ajax_MakeFile.php?folpseq=" + g_folseq,
        type: "post",
        data: {
            file: file.name,
            orgname: file.orgn
        },
        success: function(data) {
            if (data.substr(0, 4) == "msg:") {
                $.Zebra_Dialog(data.substr(4), {
                    type: "error",
                    title: "繝輔ぃ繧､繝ｫ繧｢繝��繝ｭ繝ｼ繝�"
                })
            } else {
                if (data != "fail") {
                    var aryFileInfo = eval("(" + data + ")");
                    var newFile = createFileObject(aryFileInfo);
                    $(newFile).css("left", 0);
                    $(newFile).css("top", 0);
                    if (g_editmode) {
                        $(".fileCheckbox", newFile).css("display", "");
                        $(newFile).draggable(g_Sub.draggable)
                    }
                    setItemPosition()
                }
            }
        }
    })
}
var multiDownloadTasks;

function showMultiDownloadDlg() {
    var e = $(".itemMember .fileCheckbox_sel").closest(".box").toArray();
    var j = new Array();
    for (var g = 0; g < e.length; g++) {
        var f = e[g];
        var c = $(f).attr("id").split("_");
        var b = {};
        b.seq = parseInt(c[1]);
        b.fname = $("div.fileInfo div.fileNameForList", f).text();
        if (c[0] == "objFile") {
            j.push(b)
        }
    }
    if (j.length == 0) {
        $.Zebra_Dialog("荳諡ｬ繝繧ｦ繝ｳ繝ｭ繝ｼ繝峨☆繧九ヵ繧｡繧､繝ｫ繧�1縺､莉･荳企∈謚槭＠縺ｦ縺上□縺輔＞", {
            type: "error",
            title: "荳諡ｬ繝繧ｦ繝ｳ繝ｭ繝ｼ繝�"
        })
    } else {
        multiDownloadTasks = new Array();
        for (var g = 0; g < j.length; g++) {
            if ($("#objDLFile_" + j[g].seq)[0]) {
                continue
            }
            var d = $("#dllistTemplate");
            var h = $(d).clone(true);
            $(h).attr("id", "objDLFile_" + j[g].seq);
            $("#dllistTemplate").before(h);
            $(h).addClass("dlitemMember");
            $(h).css("display", "");
            $("td:eq(0) p", h).text(j[g].fname);
            var a = {};
            a.func = function(k) {
                var i = this.seq;
                var l = k ? "&withmemo" : "";
                $("#objDLFile_" + i + " td:eq(1) p").text("繝繧ｦ繝ｳ繝ｭ繝ｼ繝画ｺ門ｙ荳ｭ");
                return $.ajax({
                    url: "./ajax/ajax_MakeDownload.php?json&filseq=" + i + l,
                    type: "get",
                    dataType: "json",
                    success: function(m) {
                        if (m.fileName !== undefined) {
                            downloadMultipleFile("dl/dl_docfile.php?dl&filseq=" + i + "&cd=" + m.fileName, m.fileSize, "objDLFile_" + i)
                        }
                    },
                    complete: function(n, m) {}
                }).pipe(function(m) {
                    console.log("common Complete");
                    return m
                }, function() {
                    return "task1 fail"
                })
            }.bind(a);
            a.seq = j[g].seq;
            multiDownloadTasks.push(a)
        }
        if (multiDownloadTasks.length > 0) {
            $("#multipleFileDownloadDlg button").prop("disabled", false);
            $("#multipleFileDownloadDlg input").prop("disabled", false)
        }
        $.fancybox.open("#multipleFileDownloadDlg", {
            fitToView: true,
            autoSize: false,
            minWidth: 320,
            maxWidth: "90%",
            scrolling: "auto",
            closeClick: false,
            openEffect: "none",
            closeEffect: "none",
            helpers: {
                overlay: {
                    speedIn: 300,
                    speedOut: 300,
                    opacity: 0.5
                }
            }
        })
    }
}

function startMultipleDownload() {
    $("#multipleFileDownloadDlg button").prop("disabled", true);
    $("#multipleFileDownloadDlg input").prop("disabled", true);
    var a = $("#iF_MEMO").prop("checked");
    $.extend({
        sequence: function(e) {
            var c = $.Deferred();
            var b = c.promise();
            $.each(e, function(f, d) {
                b = b.pipe(d.func(a))
            });
            c.resolve();
            return b
        }
    });
    $.sequence(multiDownloadTasks).fail(function(b) {
        console.log("common fail", b)
    }).always(function(b) {})
}

function downloadMultipleFile(a, i, f) {
    var b = a;
    var d = $(".download_file").length;
    var c = 0;
    var h = [];
    var e = function(k, n, m, j) {
        var o = new XMLHttpRequest();
        var l = n;
        o.onprogress = function(q) {
            var p = (q.loaded / 1024).toFixed(2) + " / " + (l / 1024).toFixed(2) + " KB";
            var r = 100;
            if (l > 0) {
                r = parseInt(100 * q.loaded / l)
            }
            $("#" + m + " td:eq(1) p").text(p);
            $("#" + m + " td:eq(2) div.bar").css("width", r + "%")
        };
        o.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    var p = this.getResponseHeader("User-UTF8FileName");
                    p = decodeURIComponent(p);
                    j(this.response, p)
                } else {
                    j(null)
                }
            }
        };
        o.open("GET", k, true);
        o.responseType = "blob";
        o.send(null)
    };
    var g = function(k, j) {
        var l = window.URL.createObjectURL(j);
        var m = document.createElement("a");
        document.body.appendChild(m);
        m.href = l;
        m.download = k;
        if (!window.TextRange) {
            m.click()
        } else {
            window.navigator.msSaveBlob(j, k)
        }
        document.body.removeChild(m)
    };
    e(b, i, f, function(k, j) {
        c++;
        if (k != null) {
            g(j, k);
            $("#" + f + " td:eq(2)").html("螳御ｺ�")
        } else {
            h.push(j);
            $("#" + f + " td:eq(2)").html("Err")
        }
        if (d == c) {
            if (h.length != 0) {}
        }
    })
}
var g_viewerWindow = null;

function openEBook5(f, e, d) {
    var c = (d === undefined) ? "" : "&q=" + d;
    var b = "./bookview/?filseq=" + f;
    if (!Object.keys) {
        b = "./viewer/index.php?filseq=" + f
    }
    if (e > 0) {
        b += "&page=" + e
    }
    b += c;
    var a = "SBViewer";
    if ((g_viewerWindow) && (!g_viewerWindow.closed)) {
        g_viewerWindow.close()
    }
    g_viewerWindow = window.open(b, a);
    g_viewerWindow.focus()
}

function openMoviePlayer(a) {
    $.fancybox.close(true);
    $.fancybox({
        href: "./movie/?filseq=" + a + "&iframe",
        type: "iframe",
        iframe: {
            scrolling: "no"
        },
        maxWidth: 720,
        minHeight: 404,
        closeClick: false,
        openEffect: "elastic",
        closeEffect: "elastic",
        scrolling: "no",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            }
        }
    })
}

function gridctl_setControlPanel(b) {
    $("#" + b + " .selectAllButton a").text("蜈ｨ驕ｸ謚�");
    var a = $("#" + b + " .gridctl_menuTABSelect").attr("title");
    $("#" + b + " .gridctl_Buttons div").css("display", "none");
    if (b == "gridctl_useredit") {
        $("#" + b + " .selectAllButton").css("display", "");
        if (a == "user") {
            $("#" + b + " .makeUserButton").css("display", "");
            $("#" + b + " .deletUserButton").css("display", "");
            $("#" + b + " .resetUserButton").css("display", "");
            $("#" + b + " .batchProcessButton").css("display", "")
        } else {
            if (a == "group") {
                $("#" + b + " .makeGroupButton").css("display", "");
                $("#" + b + " .deletGroupButton").css("display", "")
            } else {
                if (a == "member") {
                    $("#" + b + " .renameGroupButton").css("display", "");
                    $("#" + b + " .setUserButton").css("display", "");
                    $("#" + b + " .unsetUserButton").css("display", "")
                }
            }
        }
    } else {
        if (b == "gridctl_accessdlg") {
            $("#" + b + " .selectAllButton").css("display", "");
            $("#" + b + " .addUserAccess").css("display", "");
            $("#" + b + " .selectAccessButton").css("display", "")
        } else {
            if (b == "gridctl_userseldlg") {
                $("#" + b + " .selectAllButton").css("display", "");
                $("#" + b + " .selectUserButton").css("display", "");
                $("#" + b + " div[title^='group']").css("display", "none")
            } else {
                if (b == "gridctl_userseldlg2" || b == "gridctl_userseldlg3" || b == "gridctl_userseldlg5") {
                    $("#" + b + " .selectAllButton").css("display", "none");
                    $("#" + b + " .selectUserButton").css("display", "none");
                    $("#" + b + " div[title^='user']").css("display", "none");
                    $("#" + b + " div[title^='group']").css("display", "none")
                } else {
                    if (b == "gridctl_userseldlg4") {
                        $("#" + b + " .selectAllButton").css("display", "");
                        $("#" + b + " .selectMetUserButton").css("display", "")
                    }
                }
            }
        }
    }
}

function setFexiGridForUserList(i, f, b, g) {
    if (g_gridObj[i] !== undefined) {
        $("#" + g_gridObj[i].tbDomID).remove();
        delete g_gridObj[i];
        $("#" + i + "_dxgrid").remove()
    }
    var c = $("#" + i + " .gridctl_clone");
    var h = $(c).clone();
    $(c).after(h);
    $(h).css("display", "");
    $(h).attr("id", i + "_dxgrid");
    var e = (i == "gridctl_userseldlg2" || i == "gridctl_userseldlg3" || i == "gridctl_userseldlg5");
    g_gridObj[i] = new SBGrid(i + "_dxgrid");
    g_gridObj[i].bottomSpace = 62;
    g_gridObj[i].setMultiSelect(!e);
    if (i != "gridctl_useredit" && i != "gridctl_userseldlg5") {
        var a = 640;
        if (window.innerHeight) {
            a = window.innerHeight
        } else {
            if (document.documentElement.clientHeight) {
                a = document.documentElement.clientHeight
            }
        }
        g_gridObj[i].fixHeight = a - 240
    }
    g_gridObj[i].onClickSelAllButton = function(k) {
        gridctl_AllSelect(i);
        return true
    };
    var j = 1;
    if (i == "gridctl_useredit") {
        j = 0
    }
    var d = "grpseq=" + f + "&dlg=" + j + (g == 1 ? "&cu" : "");
    g_gridObj[i].load("./ajax/ajax_GetUserList.php?header&" + d, "./ajax/ajax_GetUserList.php?" + d)
}

function setFexiGridForGroupList(d) {
    if (g_gridObj[d] !== undefined) {
        $("#" + g_gridObj[d].tbDomID).remove();
        delete g_gridObj[d];
        $("#" + d + "_dxgrid").remove()
    }
    var b = $("#" + d + " .gridctl_clone");
    var a = $(b).clone();
    $(b).after(a);
    $(a).css("display", "");
    $(a).attr("id", d + "_dxgrid");
    g_gridObj[d] = new SBGrid(d + "_dxgrid");
    g_gridObj[d].bottomSpace = 62;
    g_gridObj[d].setMultiSelect(true);
    if (d != "gridctl_useredit" && d != "gridctl_userseldlg5") {
        var c = 640;
        if (window.innerHeight) {
            c = window.innerHeight
        } else {
            if (document.documentElement.clientHeight) {
                c = document.documentElement.clientHeight
            }
        }
        g_gridObj[d].fixHeight = c - 240
    }
    var e = "target=" + d;
    g_gridObj[d].load("./ajax/ajax_GetGroupList.php?header&" + e, "./ajax/ajax_GetGroupList.php?" + e)
}

function gridctl_TabSelect(b, c) {
    $("#" + b + " .gridctl_menuMemberTAB").css("display", "none");
    $("#" + b + " .gridctl_menuTAB").removeClass("gridctl_menuTABSelect");
    var a = $("#" + b + " .gridctl_menuTAB").toArray();
    $(a[c]).addClass("gridctl_menuTABSelect");
    gridctl_setControlPanel(b);
    if (c == 1) {
        setFexiGridForGroupList(b)
    } else {
        m_setFexiGridForUserList(b, 0)
    }
}

function gridctl_AddMemberTag(a, b) {
    $("#" + a + " .gridctl_menuTAB").removeClass("gridctl_menuTABSelect");
    $("#" + a + " .gridctl_menuMemberTAB").css("display", "");
    $("#" + a + " .gridctl_menuMemberTAB").addClass("gridctl_menuTABSelect");
    $("#" + a + " .gridctl_block div").remove();
    $("#" + a + " input[name='grpseq']").val(b);
    jQuery.ajax({
        url: "./ajax/ajax_GetGroupName.php?grpseq=" + b,
        type: "get",
        success: function(c) {
            $("#" + a + " .gridctl_menuMemberTAB a").html("縲�" + c + "縲阪�繝｡繝ｳ繝舌�荳隕ｧ")
        }
    });
    gridctl_setControlPanel(a);
    m_setFexiGridForUserList(a, b)
}

function gridctl_AllSelect(b) {
    g_gridObj[b].setAllSelection(false);
    var a = $("#" + b + " .selectAllButton a");
    if (a.text() == "蜈ｨ驕ｸ謚�") {
        g_gridObj[b].setAllSelection(true);
        a.text("蜈ｨ隗｣髯､")
    } else {
        a.text("蜈ｨ驕ｸ謚�")
    }
}

function editGroupName() {
    var a = $("#gridctl_useredit input[name='grpseq']").val();
    $.ajax({
        url: "./ajax/ajax_ShowGroupInfo.php",
        type: "get",
        data: {
            mode: "edit",
            grpseq: a
        },
        success: function(b) {
            $.fancybox({
                content: b,
                fitToView: true,
                autoSize: false,
                autoHeight: true,
                minWidth: 320,
                maxWidth: "90%",
                closeClick: false,
                scrolling: "auto",
                openEffect: "elastic",
                closeEffect: "elastic",
                arrows: false,
                nextClick: false,
                mouseWheel: false,
                helpers: {
                    overlay: {
                        speedIn: 300,
                        speedOut: 300,
                        opacity: 0.5
                    }
                }
            })
        }
    })
}

function postUserEdit(d, f) {
    var a = g_gridObj[d].getSelectedRowId();
    var c = $("#gridctl_useredit input[name='grpseq']").val();
    if (a.length == 0) {
        $.Zebra_Dialog("蜈医↓蟇ｾ雎｡繧帝∈謚槭＠縺ｦ縺上□縺輔＞", {
            type: "error",
            title: "繝ｦ繝ｼ繧ｶ繝ｼ邂｡逅�"
        })
    } else {
        var e = "";
        var b = "";
        if (f == "deleteUser") {
            e = "驕ｸ謚槭＠縺溘Θ繝ｼ繧ｶ繝ｼ繧貞炎髯､縺励∪縺吶°��";
            b = "warning"
        } else {
            if (f == "deleteGroup") {
                e = "驕ｸ謚槭＠縺溘げ繝ｫ繝ｼ繝励ｒ蜑企勁縺励∪縺吶°��";
                b = "warning"
            } else {
                if (f == "unsetGroupUser") {
                    e = "驕ｸ謚槭＠縺溘Θ繝ｼ繧ｶ繝ｼ繧偵げ繝ｫ繝ｼ繝励Γ繝ｳ繝舌�縺九ｉ螟悶＠縺ｾ縺吶°��";
                    b = "question"
                } else {
                    if (f == "addGroupUser") {
                        e = "驕ｸ謚槭＠縺溘Θ繝ｼ繧ｶ繝ｼ繧偵げ繝ｫ繝ｼ繝励Γ繝ｳ繝舌�縺ｫ霑ｽ蜉�縺励∪縺吶°��";
                        b = "question"
                    }
                }
            }
        }
        $.Zebra_Dialog(e, {
            type: b,
            title: "繝ｦ繝ｼ繧ｶ繝ｼ邂｡逅�",
            buttons: ["OK", "繧ｭ繝｣繝ｳ繧ｻ繝ｫ"],
            onClose: function(g) {
                if (g == "OK") {
                    $.ajax({
                        type: "post",
                        url: "./ajax/ajax_PostFromUserList.php",
                        data: {
                            grpseq: c,
                            seqs: JSON.stringify(a),
                            cmd: f
                        },
                        success: function(h) {
                            $("#gridctl_useredit .selectAllButton a").text("蜈ｨ驕ｸ謚�");
                            g_gridObj.gridctl_useredit.reload();
                            closePopupAjax()
                        }
                    })
                }
            }
        })
    }
}

function setAccessUserSelection(f) {
    var c = g_gridObj[f].getSelectedRowId();
    var d = $("#" + f + " .gridctl_menuTABSelect").attr("title");
    var a = [];
    for (var b = 0; b < c.length; b++) {
        var e = {};
        if (d == "group") {
            var g = g_gridObj[f].objGrid.cells(c[b], 1).getValue();
            g = g.substring(0, g.indexOf("^"));
            e.GRPNM = g;
            e.USRNM = null;
            e.USRMAIL = null;
            e.GRPSEQ = c[b];
            e.USRSEQ = "0"
        } else {
            e.GRPNM = null;
            e.USRNM = g_gridObj[f].objGrid.cells(c[b], 1).getValue();
            e.USRMAIL = g_gridObj[f].objGrid.cells(c[b], 2).getValue();
            e.GRPSEQ = "0";
            e.USRSEQ = c[b]
        }
        e.FACSEQ = "0";
        e.FACDATEFR = $("#" + f + " input[name='iFACDATEFR']").val();
        e.FACDATETO = $("#" + f + " input[name='iFACDATETO']").val();
        e.FILMODE = $("#" + f + " select[name='iFILMODE']").val();
        a[b] = e;
        addUserAcountForm(e)
    }
    closePopupAjax()
}

function setMetUserSelection(e) {
    var b = g_gridObj[e].getSelectedRowId();
    var c = $("#" + e + " .gridctl_menuTABSelect").attr("title");
    var g = [];
    var h = $("body").data("aryAddMetMember");
    if (h) {
        for (var a = 0; a < h.length; a++) {
            g.push(h[a])
        }
    }
    for (var a = 0; a < b.length; a++) {
        var d = {};
        if (c == "group") {
            var f = g_gridObj[e].objGrid.cells(b[a], 1).getValue();
            f = f.substring(0, f.indexOf("^"));
            d.GRPNM = f;
            d.USRNM = null;
            d.USRMAIL = null;
            d.GRPSEQ = b[a];
            d.USRSEQ = "0"
        } else {
            d.GRPNM = null;
            d.USRNM = g_gridObj[e].objGrid.cells(b[a], 1).getValue();
            d.USRMAIL = g_gridObj[e].objGrid.cells(b[a], 2).getValue();
            d.GRPSEQ = "0";
            d.USRSEQ = b[a]
        }
        g.push(d)
    }
    $("body").data("aryAddMetMember", g);
    $.fancybox.jumpto(2)
}

function postUserInfo(d) {
    var c = {};
    c.USRSEQ = $("table.EditUserInfo input[name='usrseq']").val();
    c.USRNM = $("table.EditUserInfo input[name='usrnm']").val();
    c.USRCOMNM = $("table.EditUserInfo input[name='usrcomnm']").val();
    c.USRSECNM = $("table.EditUserInfo input[name='usrsecnm']").val();
    c.USRPOSNM = $("table.EditUserInfo input[name='usrposnm']").val();
    c.USRPOSID = $("table.EditUserInfo input[name='usrposid']").val();
    c.LIMITATION = $("table.EditUserInfo select[name='limitation']").val();
    c.USRFOLSEQ = $("table.EditUserInfo input[name='usrfolseq']").val();
    c.USESUMMARY = $("table.EditUserInfo select[name='useSummary']").val();
    c.SUMMARYURL = $("table.EditUserInfo input[name='summaryURL']").val();
    c.F_USRHOMEEDIT = $("table.EditUserInfo select[name='usrhomeedit']").val();
    c.USRMAIL = $("table.EditUserInfo input[name='usrmail']").val();
    c.USRPW = $("table.EditUserInfo input[name='usrpw']").val();
    c.PWEDITABLE = $("table.EditUserInfo select[name='pweditable']").val();
    c.ADMINMENUACCESS = $("table.EditUserInfo select[name='adminmenu']").val();
    var b = ($("#setdef").prop("checked")) ? "?isdef" : "";
    c.USRNM = c.USRNM.replace(/^[\s縲]+|[\s縲]+$/g, "");
    if (c.USRNM == "") {
        $.Zebra_Dialog("蜷榊燕縺ｯ蠢��磯��岼縺ｧ縺�", {
            type: "error",
            title: "繧｢繧ｫ繧ｦ繝ｳ繝域ュ蝣ｱ",
            onClose: function() {
                $("table.EditUserInfo input[name='usrnm']").focus()
            }
        });
        return
    }
    c.USRMAIL = c.USRMAIL.replace(/^[\s縲]+|[\s縲]+$/g, "");
    if (c.USRMAIL == "") {
        $.Zebra_Dialog("繝ｦ繝ｼ繧ｶ繝ｼ蜷阪�蠢��磯��岼縺ｧ縺�", {
            type: "error",
            title: "繧｢繧ｫ繧ｦ繝ｳ繝域ュ蝣ｱ",
            onClose: function() {
                $("table.EditUserInfo input[name='usrmail']").focus()
            }
        });
        return
    }
    c.USRPW = c.USRPW.replace(/^[\s縲]+|[\s縲]+$/g, "");
    if (c.USRPW.length < 8) {
        if (d || c.USRPW.length != 0) {
            $.Zebra_Dialog("繝代せ繝ｯ繝ｼ繝峨�8譁�ｭ嶺ｻ･荳翫ｒ險ｭ螳壹＠縺ｦ縺上□縺輔＞", {
                type: "error",
                title: "繧｢繧ｫ繧ｦ繝ｳ繝域ュ蝣ｱ",
                onClose: function() {
                    $("table.EditUserInfo input[name='usrpw']").focus()
                }
            });
            return
        }
    }
    if (c.USRPW != $("table.EditUserInfo input[name='usrpwconf']").val()) {
        $.Zebra_Dialog("繝代せ繝ｯ繝ｼ繝峨′遒ｺ隱榊�蜉帙→荳閾ｴ縺励∪縺帙ｓ", {
            type: "error",
            title: "繧｢繧ｫ繧ｦ繝ｳ繝域ュ蝣ｱ",
            onClose: function() {
                $("table.EditUserInfo input[name='usrpw']").focus()
            }
        });
        return
    }
    if (c.USESUMMARY == 1 && c.SUMMARYURL != "" && c.SUMMARYURL !== undefined) {
        if (c.SUMMARYURL.indexOf("http://", 0) != 0 && c.SUMMARYURL.indexOf("https://", 0) != 0) {
            $.Zebra_Dialog("豁｣縺励＞URL繧貞�蜉帙＠縺ｦ縺上□縺輔＞", {
                type: "error",
                title: "繧｢繧ｫ繧ｦ繝ｳ繝域ュ蝣ｱ",
                onClose: function() {
                    $("table.EditUserInfo input[name='summaryURL']").focus()
                }
            });
            return
        }
    }
    var a = function(e) {
        $.ajax({
            type: "post",
            url: "./ajax/ajax_PostFromUserInfo.php" + b,
            data: {
                userinfo: e
            },
            success: function(f) {
                if (f == "OK") {
                    $("#gridctl_useredit .selectAllButton a").text("蜈ｨ驕ｸ謚�");
                    if (g_gridObj.gridctl_useredit !== undefined) {
                        g_gridObj.gridctl_useredit.reload()
                    }
                    closePopupAjax()
                } else {
                    if (f == "UNQERR") {
                        $.Zebra_Dialog("縺薙�繝ｦ繝ｼ繧ｶ繝ｼ蜷阪�譌｢縺ｫ菴ｿ逕ｨ縺輔ｌ縺ｦ縺�∪縺�", {
                            type: "error",
                            title: "繧｢繧ｫ繧ｦ繝ｳ繝域ュ蝣ｱ"
                        })
                    } else {
                        $.Zebra_Dialog("繝ｦ繝ｼ繧ｶ繝ｼ諠��ｱ縺ｮ譖ｴ譁ｰ縺ｫ螟ｱ謨励＠縺ｾ縺励◆", {
                            type: "error",
                            title: "繧｢繧ｫ繧ｦ繝ｳ繝域ュ蝣ｱ"
                        })
                    }
                }
            }
        })
    };
    if (!d && c.USRPW.length == 0) {
        $.Zebra_Dialog("繝代せ繝ｯ繝ｼ繝臥┌縺励↓縺吶ｋ縺ｨ縲∬ｪｰ縺ｧ繧ゅΟ繧ｰ繧､繝ｳ縺ｧ縺阪ｋ繧ｲ繧ｹ繝医Θ繝ｼ繧ｶ繝ｼ縺ｮ繧医≧縺ｪ菴咲ｽｮ縺･縺代↓縺ｪ繧翫∪縺吶�\n縺ｾ縺溘∫ｮ｡逅��ｻ･螟悶�繝ｦ繝ｼ繧ｶ繝ｼ諠��ｱ縺ｮ螟画峩縺後〒縺阪↑縺上↑繧翫∪縺吶�\n繧医ｍ縺励＞縺ｧ縺吶°��", {
            type: "warning",
            title: "繧｢繧ｫ繧ｦ繝ｳ繝域ュ蝣ｱ",
            buttons: ["OK", "繧ｭ繝｣繝ｳ繧ｻ繝ｫ"],
            onClose: function(e) {
                if (e == "OK") {
                    a(c)
                } else {
                    $("table.EditUserInfo input[name='usrpw']").focus();
                    return
                }
            }
        })
    } else {
        a(c)
    }
}

function postUserBatch() {
    var d = {};
    if ($("table.EditUserInfo input[name='iF_usrcomnm']").prop("checked")) {
        d.USRCOMNM = $("table.EditUserInfo input[name='usrcomnm']").val()
    }
    if ($("table.EditUserInfo input[name='iF_usrsecnm']").prop("checked")) {
        d.USRSECNM = $("table.EditUserInfo input[name='usrsecnm']").val()
    }
    if ($("table.EditUserInfo input[name='iF_usrposnm']").prop("checked")) {
        d.USRPOSNM = $("table.EditUserInfo input[name='usrposnm']").val()
    }
    if ($("table.EditUserInfo input[name='iF_limitation']").prop("checked")) {
        d.LIMITATION = $("table.EditUserInfo select[name='limitation']").val()
    }
    if ($("table.EditUserInfo input[name='iF_usrfolseq']").prop("checked")) {
        d.USRFOLSEQ = $("table.EditUserInfo input[name='usrfolseq']").val()
    }
    if ($("table.EditUserInfo input[name='iF_useSummary']").prop("checked")) {
        d.USESUMMARY = $("table.EditUserInfo select[name='useSummary']").val()
    }
    if ($("table.EditUserInfo input[name='iF_usrhomeedit']").prop("checked")) {
        d.F_USRHOMEEDIT = $("table.EditUserInfo select[name='usrhomeedit']").val()
    }
    if ($("table.EditUserInfo input[name='iF_pweditable']").prop("checked")) {
        d.PWEDITABLE = $("table.EditUserInfo select[name='pweditable']").val()
    }
    if ($("table.EditUserInfo input[name='iF_adminmenu']").prop("checked")) {
        d.ADMINMENUACCESS = $("table.EditUserInfo select[name='adminmenu']").val()
    }
    var a = 0;
    if ($("table.ResetUserInfo input[name='iF_rst_bookmark']").prop("checked")) {
        a += 1
    }
    if ($("table.ResetUserInfo input[name='iF_rst_figure']").prop("checked")) {
        a += 10
    }
    if ($("table.ResetUserInfo input[name='iF_rst_password']").prop("checked")) {
        a += 100
    }
    if (d.USESUMMARY == 1) {
        d.SUMMARYURL = $("table.EditUserInfo input[name='summaryURL']").val();
        if (d.SUMMARYURL.indexOf("http://", 0) != 0 && d.SUMMARYURL.indexOf("https://", 0) != 0) {
            $.Zebra_Dialog("豁｣縺励＞URL繧貞�蜉帙＠縺ｦ縺上□縺輔＞", {
                type: "error",
                title: "繧｢繧ｫ繧ｦ繝ｳ繝域ュ蝣ｱ",
                onClose: function() {
                    $("table.EditUserInfo input[name='summaryURL']").focus()
                }
            });
            return
        }
    }
    var e = "驕ｸ謚槭＆繧後◆繝ｦ繝ｼ繧ｶ繝ｼ縺ｮ諠��ｱ繧剃ｸ諡ｬ險ｭ螳壹＠縺ｦ繧ゅｈ繧阪＠縺�〒縺吶°��";
    var b = "question";
    var c;
    if (a > 0) {
        e = "繝ｦ繝ｼ繧ｶ繝ｼ蛻晄悄蛹夜��岼縺梧欠螳壹＆繧後※縺�∪縺吶�<br />驕ｸ謚槭＆繧後◆繝ｦ繝ｼ繧ｶ繝ｼ縺ｮ諠��ｱ繧剃ｸ諡ｬ險ｭ螳壹＠縺ｦ繧ゅｈ繧阪＠縺�〒縺吶°��";
        b = "warning";
        c = "dlg_danger"
    }
    $.Zebra_Dialog(e, {
        type: b,
        title: "繝ｦ繝ｼ繧ｶ繝ｼ邂｡逅�",
        buttons: ["OK", "繧ｭ繝｣繝ｳ繧ｻ繝ｫ"],
        custom_class: c,
        onClose: function(f) {
            if (f == "OK") {
                var g = g_gridObj.gridctl_useredit.getSelectedRowId();
                $.ajax({
                    type: "post",
                    url: "./ajax/ajax_PostFromUserBatch.php",
                    dataType: "json",
                    data: {
                        seqs: JSON.stringify(g),
                        userinfo: d,
                        resetmod: a
                    },
                    success: function(j) {
                        g_gridObj.gridctl_useredit.reload();
                        $("#gridctl_useredit .selectAllButton a").text("蜈ｨ驕ｸ謚�");
                        if (j.pwlist !== undefined) {
                            $("#batchConfig").css("display", "none");
                            $("#showNewPasswrod").css("display", "block");
                            var l = "";
                            for (var h = 0; h < j.pwlist.length; h++) {
                                var k = j.pwlist[h];
                                l += k.USRSEQ + '\t"' + k.USRNM + '"\t"' + k.USRPW + '"\n'
                            }
                            $("#showNewPasswrod textarea").text(l);
                            $.fancybox.update()
                        } else {
                            closePopupAjax()
                        }
                    }
                })
            }
        }
    })
}

function postGroupInfo(b) {
    var a = {};
    a.GRPSEQ = b;
    a.GRPNM = $("table.EditGroupInfo input[name='grpnm']").val();
    a.GRPNM = a.GRPNM.replace(/^[\s縲]+|[\s縲]+$/g, "");
    if (a.GRPNM == "") {
        $.Zebra_Dialog("繧ｰ繝ｫ繝ｼ繝怜錐縺ｯ蠢��磯��岼縺ｧ縺�", {
            type: "error",
            title: "繧ｰ繝ｫ繝ｼ繝玲ュ蝣ｱ",
            onClose: function() {
                $("table.EditUserInfo input[name='grpnm']").focus()
            }
        });
        return
    }
    $.ajax({
        type: "post",
        url: "./ajax/ajax_PostFromGroupInfo.php",
        data: {
            groupinfo: a
        },
        success: function(c) {
            if (b == 0) {
                $("#gridctl_useredit .selectAllButton a").text("蜈ｨ驕ｸ謚�");
                g_gridObj.gridctl_useredit.reload()
            } else {
                if (c != "cannot access Database System") {
                    $("#gridctl_useredit .gridctl_menuMemberTAB a").html("縲�" + c + "縲阪�繝｡繝ｳ繝舌�荳隕ｧ")
                }
            }
            closePopupAjax()
        }
    })
}

function rpNonTag(a) {
    a = a.replace("< ", "&lt;");
    a = a.replace("> ", "&gt;");
    return a
}

function setPopupAjax(b, a) {
    if (a === undefined) {
        a = false
    }
    $(b).addClass("fancybox.ajax");
    $(b).fancybox({
        fitToView: true,
        autoSize: a,
        autoHeight: true,
        minWidth: 320,
        maxWidth: "90%",
        closeClick: false,
        scrolling: "auto",
        openEffect: "elastic",
        closeEffect: "elastic",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            }
        }
    })
}

function showTagList() {
    var a = 0;
    if ($("#iF_FOLDER").prop("checked")) {
        a = $("#iF_FOLDER").val()
    }
    $.ajax({
        url: "./ajax/ajax_ShowTagList.php",
        type: "get",
        data: {
            folseq: a
        },
        success: function(b) {
            $.fancybox({
                content: b,
                fitToView: true,
                autoSize: false,
                autoHeight: true,
                minWidth: 320,
                maxWidth: 800,
                closeClick: false,
                scrolling: "auto",
                openEffect: "elastic",
                closeEffect: "elastic",
                arrows: false,
                nextClick: false,
                mouseWheel: false,
                helpers: {
                    overlay: {
                        speedIn: 300,
                        speedOut: 300,
                        opacity: 0.5
                    }
                }
            })
        }
    });
    return false
}

function showQRCode(a) {
    if (a == "") {
        return false
    }
    $.ajax({
        url: "./ajax/ajax_ShowQRCode.php",
        type: "get",
        data: {
            data: a
        },
        success: function(b) {
            $.fancybox({
                content: b,
                fitToView: true,
                autoSize: false,
                autoHeight: true,
                minHeight: 150,
                maxWidth: 400,
                closeClick: false,
                openEffect: "elastic",
                closeEffect: "elastic",
                scrolling: "auto",
                arrows: false,
                nextClick: false,
                mouseWheel: false,
                helpers: {
                    overlay: {
                        speedIn: 300,
                        speedOut: 300,
                        opacity: 0.5
                    }
                }
            })
        }
    });
    return false
}

function popupBookMark() {
    $.fancybox.open([{
        type: "ajax",
        href: "./ajax/ajax_ShowBookmark.php?typ=0&b=0"
    }, {
        type: "ajax",
        href: "./ajax/ajax_ShowBookmark.php?typ=1&b=0"
    }, {
        type: "ajax",
        href: "./ajax/ajax_ShowBookmark.php?typ=2&b=0"
    }, {
        type: "ajax",
        href: "./ajax/ajax_ShowBookmark.php?typ=3&b=0"
    }], {
        fitToView: true,
        autoSize: false,
        autoHeight: true,
        minWidth: 320,
        maxWidth: "90%",
        closeClick: false,
        scrolling: "auto",
        openEffect: "elastic",
        closeEffect: "elastic",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        keys: {
            next: {},
            prev: {},
            play: {}
        },
        index: 1,
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            },
            title: {
                type: "inside",
                position: "top"
            }
        },
        afterLoad: function() {
            this.title = "<div id='bookmarkMenuFrame'></div>";
            backToTopList = false
        },
        afterShow: function() {
            if (backToTopList) {
                $.fancybox.jumpto(0)
            } else {
                $("#bookmarkMenuFrame").html($("#bookmarkMenu").html());
                $.fancybox.update()
            }
        }
    });
    return false
}

function popupFigureList() {
    $.fancybox.open([{
        type: "ajax",
        href: "./ajax/ajax_ShowFigureList.php?typ=0"
    }, {
        type: "ajax",
        href: "./ajax/ajax_ShowFigureList.php?typ=1"
    }], {
        fitToView: true,
        autoSize: false,
        autoHeight: true,
        minWidth: 320,
        maxWidth: "90%",
        closeClick: false,
        scrolling: "auto",
        openEffect: "elastic",
        closeEffect: "elastic",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        keys: {
            next: {},
            prev: {},
            play: {}
        },
        index: 0,
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            },
            title: {
                type: "inside",
                position: "top"
            }
        },
        afterLoad: function() {
            this.title = "<div id='figurelistMenuFrame'></div>";
            backToTopList = false
        },
        afterShow: function() {
            if (backToTopList) {
                $.fancybox.jumpto(0)
            } else {
                $("#figurelistMenuFrame").html($("#figurelistMenu").html());
                $.fancybox.update()
            }
        }
    });
    return false
}

function popupMeeting() {
    $.fancybox.open([{
        type: "ajax",
        href: "./ajax/ajax_ShowMeeting.php?typ=0"
    }, {
        type: "ajax",
        href: "./ajax/ajax_ShowMeeting.php?typ=1"
    }, {
        type: "ajax",
        href: "./ajax/ajax_ShowMeeting.php?typ=2"
    }, {
        type: "ajax",
        href: "./ajax/ajax_ShowUserList.php?target=gridctl_userseldlg4&folpseq=0",
        fitToView: false,
        width: "90%"
    }, {
        type: "iframe",
        href: "./vote/",
        autoSize: true
    }], {
        fitToView: true,
        autoSize: false,
        autoHeight: true,
        minWidth: 320,
        maxWidth: "90%",
        closeClick: false,
        scrolling: "auto",
        openEffect: "elastic",
        closeEffect: "elastic",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        keys: {
            next: {},
            prev: {},
            play: {}
        },
        index: 0,
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            },
            title: {
                type: "inside",
                position: "top"
            }
        },
        afterLoad: function() {
            this.title = "<div id='meetingMenuFrame'></div>";
            backToTopList = false
        },
        afterShow: function() {
            if (backToTopList) {
                $.fancybox.jumpto(0)
            } else {
                $("#meetingMenuFrame").html($("#meetingMenu").html());
                $.fancybox.update()
            }
        },
        afterClose: function() {
            $("body").removeData("meetingInfo");
            $("body").removeData("aryAddMetMember")
        },
        onUpdate: function() {
            if (g_gridObj.gridctl_userseldlg4 !== undefined) {
                g_gridObj.gridctl_userseldlg4.adjustGridHeight()
            }
        }
    });
    return false
}

function popupUserList(c, a, e, d) {
    var b = (a == 1 ? "&cu" : "");
    b += (d !== undefined ? "&folpseq=" + d : "");
    $.fancybox.open({
        type: "ajax",
        href: "./ajax/ajax_ShowUserList.php?target=" + c + b,
        fitToView: false,
        autoSize: false,
        autoHeight: true,
        minWidth: 320,
        width: "90%",
        closeClick: false,
        scrolling: "auto",
        openEffect: "elastic",
        closeEffect: "elastic",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        afterShow: function() {
            g_gridObj[c].adjustGridHeight();
            if (e !== undefined) {
                e()
            }
        },
        onUpdate: function() {
            if (g_gridObj[c] !== undefined) {
                g_gridObj[c].adjustGridHeight()
            }
        },
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            }
        }
    });
    return false
}

function closePopupAjax() {
    $.fancybox.close()
};