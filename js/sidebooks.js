/*!
 * Cloud book reader縲茎idebooks.js縲� version 1.0.24
 * Copyright 2010-2019 TOKYO IP. All rights Reserved.
 */
;

function init() {
    SBDebug.ENABLED = false;
    if (!Object.keys) {
        return
    }
    g_sbViewer = new SBViewer(document.body, g_docInfo, function() {
        var b = function() {
            g_sbViewer.setFrame(document.documentElement.clientWidth, document.documentElement.clientHeight)
        };
        window.onresize = b;
        var c = new Meeting(1, g_docInfo.DATAURL);
        var a = new Date().getTime();
        if (g_docInfo.LOGOUTTIME > 0 && g_docInfo.USRDATA == 1) {
            g_sbViewer.userInteractiveAction = function() {
                var e = new Date().getTime();
                var d = (e - a > 60000);
                if (d && c.currentMetSeq == 0) {
                    a = e;
                    c.chkMeetingParticipate(0)
                }
            }
        }
        b()
    })
}



function popupPageIndex() {
    $.fancybox.open({
        href: "#pageIndexContainer"
    }, {
        fitToView: true,
        autoSize: false,
        maxWidth: "90%",
        closeClick: false,
        scrolling: "auto",
        openEffect: "elastic",
        closeEffect: "elastic",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        keys: {
            next: {},
            prev: {},
            play: {}
        },
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            },
            title: {
                type: "inside",
                position: "top"
            }
        },
        afterLoad: function() {
            backToTopList = false;
            g_sbViewer.enabled = false
        },
        afterShow: function() {},
        afterClose: function() {
            g_sbViewer.enabled = true
        }
    });
    return false
}

function popupBookMark() {
    var a = 0;
    if (g_sbViewer) {
        a = g_sbViewer.config.dualPage ? 1 : 2
    }
    $.fancybox.open([{
        type: "ajax",
        href: "../ajax/ajax_ShowBookmark.php?typ=0&b=" + a
    }, {
        type: "ajax",
        href: "../ajax/ajax_ShowBookmark.php?typ=1&b=" + a
    }, {
        type: "ajax",
        href: "../ajax/ajax_ShowBookmark.php?typ=2&b=" + a
    }, {
        type: "ajax",
        href: "../ajax/ajax_ShowBookmark.php?typ=3&b=" + a
    }, {
        type: "ajax",
        href: "../ajax/ajax_ShowBookmark.php?typ=4&b=" + a
    }], {
        fitToView: true,
        autoSize: false,
        maxWidth: "90%",
        closeClick: false,
        scrolling: "auto",
        openEffect: "elastic",
        closeEffect: "elastic",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        keys: {
            next: {},
            prev: {},
            play: {}
        },
        index: 1,
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            },
            title: {
                type: "inside",
                position: "top"
            }
        },
        afterLoad: function() {
            this.title = "<div id='bookmarkMenuFrame'></div>";
            backToTopList = false;
            g_sbViewer.enabled = false
        },
        afterShow: function() {
            if (backToTopList) {
                $.fancybox.jumpto(0)
            } else {
                $("#bookmarkMenuFrame").html($("#bookmarkMenu").html());
                $.fancybox.update()
            }
        },
        afterClose: function() {
            g_sbViewer.enabled = true
        }
    });
    return false
}

function popupPageSearch(a) {
    $.fancybox.open([{
        type: "ajax",
        href: "../ajax/ajax_ShowPageSearch.php?id=" + g_sbViewer.docInfo.FILSEQ
    }], {
        fitToView: true,
        autoSize: false,
        maxWidth: "90%",
        closeClick: false,
        scrolling: "auto",
        openEffect: "elastic",
        closeEffect: "elastic",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        index: 0,
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            },
            title: {
                type: "inside",
                position: "top"
            }
        },
        afterLoad: function() {
            this.title = "<div id='pagesearchMenuFrame'></div>";
            g_sbViewer.enabled = false
        },
        afterShow: function() {
            $("#pagesearchMenuFrame").html($("#pagesearchMenu").html());
            if (a != "") {
                $("input[name='iSKEYWORD']").val(a);
                searchText()
            }
            $.fancybox.update();
            $("input[name='iSKEYWORD']").focus()
        },
        afterClose: function() {
            g_sbViewer.enabled = true
        }
    });
    return false
}

function popupMeeting(c) {
    var a = 0;
    if (g_sbViewer) {
        a = g_sbViewer.config.dualPage ? 1 : 2
    }
    $.fancybox.open([{
        type: "ajax",
        href: "../ajax/ajax_ShowMeeting.php?typ=1&id=" + c + "&b=" + a
    }], {
        fitToView: true,
        autoSize: false,
        maxWidth: "90%",
        closeClick: false,
        scrolling: "auto",
        openEffect: "elastic",
        closeEffect: "elastic",
        arrows: false,
        nextClick: false,
        mouseWheel: false,
        keys: {
            next: {},
            prev: {},
            play: {}
        },
        helpers: {
            overlay: {
                speedIn: 300,
                speedOut: 300,
                opacity: 0.5
            },
            title: {
                type: "inside",
                position: "top"
            }
        },
        afterLoad: function() {
            this.title = "<div id='meetingMenuFrame'></div>";
            backToTopList = false;
            g_sbViewer.enabled = false
        },
        afterShow: function() {
            if (backToTopList) {
                $.fancybox.jumpto(0)
            } else {
                $("#meetingMenuFrame").html($("#meetingMenu").html());
                $.fancybox.update()
            }
        },
        afterClose: function() {
            $("body").removeData("meetingInfo");
            $("body").removeData("aryAddMetMember");
            g_sbViewer.enabled = true
        }
    });
    return false
}

function downloadFile() {
    g_sbViewer.sbControl.titleBar.btDownload.setEnabled(false);
    var a = new $.Zebra_Dialog("繝輔ぃ繧､繝ｫ繝繧ｦ繝ｳ繝ｭ繝ｼ繝我ｸｭ繝ｻ繝ｻ繝ｻ", {
        type: "information",
        title: g_sbViewer.docInfo.TITLE,
        buttons: false
    });
    jQuery.ajax({
        url: "../ajax/ajax_MakeDownload.php?filseq=" + g_sbViewer.docInfo.FILSEQ,
        type: "get",
        success: function(b) {
            if (b != "") {
                location.href = "../dl/dl_docfile.php?dl&filseq=" + g_sbViewer.docInfo.FILSEQ + "&cd=" + b
            }
        },
        complete: function(c, b) {
            a.close();
            g_sbViewer.sbControl.titleBar.btDownload.setEnabled(true);
            g_sbViewer.refresh()
        }
    })
}

function rpNonTag(a) {
    a = a.replace("< ", "&lt;");
    a = a.replace("> ", "&gt;");
    return a
}
var SBViewer = function(b, a, c) {
    this.docInfo = a;
    this.sbStorage = new SBStorage(this.docInfo.PATH.substr(1), this.docInfo.USRID, this.docInfo.FILSEQ);
    if (SBViewer.RESOLUTION < 2 && this.sbStorage.config.highResolution) {
        SBViewer.RESOLUTION = 2
    }
    this.renderer = PIXI.autoDetectRenderer(0, 0, {
        antialias: false,
        resolution: SBViewer.RESOLUTION
    }, true);
    b.appendChild(this.renderer.view);
    this.docInfo.CURLMODE = this.sbStorage.config.curlMode;
    if (this.docInfo.OPENPAGE == 0) {
        this.docInfo.OPENPAGE = this.sbStorage.currentHistory.pg
    } else {
        this.sbStorage.currentHistory.pg = this.docInfo.OPENPAGE
    }
    if (this.sbStorage.currentHistory.dir < 0) {
        this.sbStorage.currentHistory.dir = this.docInfo.DIRECTION
    } else {
        this.docInfo.DIRECTION = this.sbStorage.currentHistory.dir
    }
    if (this.sbStorage.currentHistory.cv < 0) {
        this.sbStorage.currentHistory.cv = this.docInfo.HASCOVER ? 1 : 0
    } else {
        this.docInfo.HASCOVER = this.sbStorage.currentHistory.cv == 0 ? false : true
    }
    this.sbStorage.currentHistory.ts = Math.floor(new Date().getTime() / 1000);
    this.sbStorage.setHistory(this.sbStorage.currentHistory);
    this.sbStorage.sortHistory();
    this.sbStorage.saveHistory();
    this.config = {
        screenSize: new PIXI.Rectangle(0, 0, 0, 0),
        pageSize: new PIXI.Rectangle(0, 0, 0, 0),
        dualPage: false,
        figureMode: false,
        devPageWidth: 0,
        frameMargin: new PIXI.Rectangle(0, 0, 0, 0),
        pageMargin: 0,
        totalDevPages: 0,
        currentPage: 0,
        turnPageAreaWidth: 0,
        barHeight: 10,
        fullScreen: false
    };
    this.userInteractiveAction = function() {};
    this.resLoader = new PIXI.loaders.Loader();
    this.resLoader.add("shadow", "js/res/shadow.png");
    this.resLoader.add("search", "js/res/search.png");
    this.resLoader.add("figure_edit", "js/res/figure_edit.png");
    this.resLoader.add("bookmark", "js/res/bookmark.png");
    this.resLoader.add("pageindex", "js/res/pageindex.png");
    this.resLoader.add("print", "js/res/print.png");
    this.resLoader.add("change", "js/res/change.png");
    this.resLoader.add("figure_pen", "js/res/figure_pen.png");
    this.resLoader.add("figure_alpha1", "js/res/figure_alpha1.png");
    this.resLoader.add("figure_alpha2", "js/res/figure_alpha2.png");
    this.resLoader.add("figure_redo", "js/res/figure_redo.png");
    this.resLoader.add("figure_select", "js/res/figure_select.png");
    this.resLoader.add("figure_type_pen", "js/res/figure_type_pen.png");
    this.resLoader.add("figure_type_line", "js/res/figure_type_line.png");
    this.resLoader.add("figure_type_line_state", "js/res/figure_type_line_state.png");
    this.resLoader.add("figure_type_pict", "js/res/figure_type_pict.png");
    this.resLoader.add("figure_type_keyboard", "js/res/figure_type_keyboard.png");
    this.resLoader.add("figure_undo", "js/res/figure_undo.png");
    this.resLoader.add("dragpin", "js/res/dragpin.png");
    this.resLoader.add("fullscreen1", "js/res/fullscreen1.png");
    this.resLoader.add("fullscreen2", "js/res/fullscreen2.png");
    this.resLoader.add("download", "js/res/download.png");
    this.resLoader.add("pageIndexData", this.docInfo.DATAURL + this.docInfo.FILCD + "/index.xml?ver=" + this.docInfo.FILFVER);
    this.resLoader.add("linkannotData", this.docInfo.DATAURL + this.docInfo.FILCD + "/linkannot.txt?ver=" + this.docInfo.FILFVER);
    this.resLoader.add("figureData", "../json/getFigureData.php?filseq=" + this.docInfo.FILSEQ + "&web=1&api=3&ver=" + new Date().getTime());
    this.resLoader.once("complete", function() {
        this.lang = new SBLanguage();
        this.sbPageManager = new SBPageManager(this);
        this.sbContainer = new SBContainer(this);
        this.sbInteraction = new SBInteraction(this, this.sbContainer);
        this.sbFigControl = new SBFigControl(this);
        this.sbContainer.container.addChild(this.sbFigControl.control);
        this.sbControl = new SBControl(this);
        this.sbContainer.container.addChild(this.sbControl.control);
        this.sbInteraction.overlapObject = this.sbControl.control;
        this.sbLinkAnnot = new SBLinkAnnot(this.resLoader.resources.linkannotData.data);
        this.sbFigure = new SBFigure(this.sbFigControl);
        this.sbIndex = new SBIndex(this, this.resLoader.resources.pageIndexData.data);
        this.animationPool = new Object();
        this.animationPlaying = false;
        this.animationContinuity = false;
        this.lastAnimationFrameID = 0;
        this.lastTextEditTime = 0;
        this.lastSearchWord = this.docInfo.QUERY;
        this.setControlButtonEvents();
        this.enabled = true;
        g_SBDebug = new SBDebug(this.sbContainer.container);
        c()
    }.bind(this));
    this.resLoader.load()
};
SBViewer.PIXELRATIO = window.devicePixelRatio && window.devicePixelRatio > 1 ? window.devicePixelRatio : 1;
SBViewer.RESOLUTION = SBViewer.PIXELRATIO;
SBViewer.RETINASCALE = 2;
SBViewer.PAGESPRITEPOOLLENGTH = 7;
SBViewer.prototype.setControlButtonEvents = function() {
    this.sbControl.titleBar.btExit.label = this.lang.resStr.btn_Close;
    this.sbControl.titleBar.btExit.onClick = function() {
        window.close()
    }.bind(this);
    this.sbControl.titleBar.btMeeting.label = this.lang.resStr.btn_Meeting;
    this.sbControl.titleBar.btMeeting.onClick = function() {
        popupMeeting(this.currentMetSeq)
    }.bind(this);
    this.sbControl.titleBar.btDownload.onClick = function() {
        downloadFile()
    }.bind(this);
    this.sbControl.toolBar.btFullScr.onClick = function() {
        this.config.fullScreen = !this.config.fullScreen;
        SBFunction.setFullScreen(this.config.fullScreen)
    }.bind(this);
    this.sbControl.toolBar.btCurl.label = this.docInfo.CURLMODE ? this.lang.resStr.btn_Curl : this.lang.resStr.btn_Slide;
    this.sbControl.toolBar.btCurl.onClick = function() {
        this.docInfo.CURLMODE = !this.docInfo.CURLMODE;
        this.sbControl.toolBar.btCurl.label = this.docInfo.CURLMODE ? this.lang.resStr.btn_Curl : this.lang.resStr.btn_Slide;
        this.initConfig();
        this.sbStorage.config.curlMode = this.docInfo.CURLMODE;
        this.sbStorage.saveConfig()
    }.bind(this);
    this.sbControl.toolBar.btCover.label = this.docInfo.HASCOVER ? this.lang.resStr.btn_hasCover : this.lang.resStr.btn_noCover;
    this.sbControl.toolBar.btCover.onClick = function() {
        this.docInfo.HASCOVER = !this.docInfo.HASCOVER;
        this.sbControl.toolBar.btCover.label = this.docInfo.HASCOVER ? this.lang.resStr.btn_hasCover : this.lang.resStr.btn_noCover;
        this.initConfig()
    }.bind(this);
    this.sbControl.toolBar.btMargin.label = this.lang.resStr.btn_Margin;
    this.sbControl.toolBar.btDirection.label = this.docInfo.DIRECTION == 0 ? this.lang.resStr.btn_LtoR : this.lang.resStr.btn_RtoL;
    this.sbControl.toolBar.btDirection.onClick = function() {
        this.docInfo.DIRECTION = this.docInfo.DIRECTION == 0 ? 1 : 0;
        this.sbControl.toolBar.btDirection.label = this.docInfo.DIRECTION == 0 ? this.lang.resStr.btn_LtoR : this.lang.resStr.btn_RtoL;
        this.initConfig()
    }.bind(this);
    this.sbControl.toolBar.btResolution.label = this.sbStorage.config.highResolution ? this.lang.resStr.btn_HighReso : this.lang.resStr.btn_NormalReso;
    this.sbControl.toolBar.btResolution.onClick = function() {
        this.sbStorage.config.highResolution = !this.sbStorage.config.highResolution;
        this.sbStorage.saveConfig();
        location.reload()
    }.bind(this);
    this.sbControl.infoBox.btSearch.onClick = function() {
        popupPageSearch(this.lastSearchWord)
    }.bind(this);
    this.sbControl.infoBox.btFigure.onClick = function() {
        this.changeFigureEditMode(true)
    }.bind(this);
    this.sbControl.infoBox.btIndex.onClick = function() {
        popupPageIndex()
    }.bind(this);
    this.sbControl.infoBox.btBookmark.onClick = function() {
        popupBookMark()
    }.bind(this)
};
SBViewer.prototype.setFrame = function(a, i) {
    if (this.sbFigure.textEditMode || new Date().getTime() - this.lastTextEditTime < 1000) {
        return
    }
    if (a < 64 || i < 64) {
        return
    }
    if (this.config.screenSize.width == a && this.config.screenSize.height == i) {
        return
    }
    this.renderer.view.style.width = a + "px";
    this.renderer.view.style.height = i + "px";
    this.renderer.resize(a, i);
    for (var h in this.animationPool) {
        this.animationPool[h].skip();
        delete this.animationPool[h]
    }
    var b = a < i ? a : i;
    this.config.barHeight = parseInt(b / 12);
    if (b < 285) {
        this.config.barHeight = parseInt(b / 8)
    } else {
        if (b < 580) {
            this.config.barHeight = parseInt(b / 10)
        } else {
            if (b > 720) {
                this.config.barHeight = parseInt(b / 16)
            }
        }
    }
    this.sbInteraction.reset();
    this.sbContainer.backView.width = a;
    this.sbContainer.backView.height = i;
    this.sbContainer.innerContainer.scale.x = 1;
    this.sbContainer.innerContainer.scale.y = 1;
    this.sbContainer.scale = 1;
    this.config.screenSize.width = a;
    this.config.screenSize.height = i;
    var e = new PIXI.Rectangle();
    var c = 1;
    var f = Math.abs((a / i) - (this.docInfo.WIDTH / this.docInfo.HEIGHT));
    var d = Math.abs((a / i) - (this.docInfo.WIDTH * 2 / this.docInfo.HEIGHT));
    var g = (f > d) ? 2 : 1;
    if (a / i > this.docInfo.WIDTH * g / this.docInfo.HEIGHT) {
        e.height = parseInt(i * c);
        e.width = parseInt(i * c * this.docInfo.WIDTH / this.docInfo.HEIGHT)
    } else {
        e.width = parseInt((a / g) * c);
        e.height = parseInt((a / g) * c * this.docInfo.HEIGHT / this.docInfo.WIDTH)
    }
    this.config.pageSize = e;
    this.initConfig()
};
SBViewer.prototype.initConfig = function() {
    if (this.config.pageSize.width * 2 <= this.config.screenSize.width) {
        this.config.dualPage = true;
        this.config.devPageWidth = this.config.pageSize.width * 2;
        this.config.totalDevPages = Math.ceil((this.docInfo.FILPAGE + (this.docInfo.HASCOVER ? 1 : 0)) / 2)
    } else {
        this.config.dualPage = false;
        this.config.devPageWidth = this.config.pageSize.width;
        this.config.totalDevPages = this.docInfo.FILPAGE
    }
    this.config.frameMargin.width = Math.ceil((this.config.screenSize.width - this.config.devPageWidth) / 2);
    this.config.frameMargin.height = Math.ceil((this.config.screenSize.height - this.config.pageSize.height) / 2);
    this.config.pageMargin = parseInt(this.config.screenSize.width * 0.1);
    this.config.turnPageAreaWidth = this.config.frameMargin.width + parseInt((this.config.screenSize.width < this.config.screenSize.height ? this.config.screenSize.width : this.config.screenSize.height) / 12);
    if (this.config.frameMargin.width > this.config.pageMargin) {
        this.config.pageMargin = this.config.frameMargin.width
    }
    this.sbContainer.innerContainer.wldWidth = (this.config.devPageWidth + this.config.pageMargin) * this.config.totalDevPages;
    this.sbContainer.innerContainer.wldHeight = this.config.screenSize.height;
    this.sbPageManager.setSize(this.config.pageSize.width, this.config.pageSize.height);
    this.sbControl.setSize(this.config.screenSize.width, this.config.screenSize.height);
    this.sbFigControl.setSize(this.config.screenSize.width, this.config.screenSize.height);
    this.movePage(this.docInfo.OPENPAGE > 0 ? this.docInfo.OPENPAGE : this.config.currentPage, false);
    this.docInfo.OPENPAGE = 0
};
SBViewer.prototype.loading = function(a) {
    this.enabled = !a;
    if (a) {
        this.sbContainer.container.alpha = 0.3
    } else {
        this.sbContainer.container.alpha = 1
    }
    this.refresh()
};
SBViewer.prototype.drawPages = function(e) {
    var b = e === undefined ? false : e;
    var d = this.getCenterDevPage();
    var a = this.getDevPageMember(d);
    if (!b && this.sbContainer.scale > 1 && this.config.currentPage == a.firstPage) {
        return
    }
    this.config.currentPage = a.firstPage;
    this.sbPageManager.referencePage = this.config.currentPage;
    this.setPageInfo(a);
    if (this.sbStorage.currentHistory.pg != this.config.currentPage || this.sbStorage.currentHistory.dir != this.docInfo.DIRECTION || this.sbStorage.currentHistory.cv != this.docInfo.HASCOVER ? 1 : 0) {
        this.sbStorage.currentHistory.pg = this.config.currentPage;
        this.sbStorage.currentHistory.dir = this.docInfo.DIRECTION;
        this.sbStorage.currentHistory.cv = this.docInfo.HASCOVER ? 1 : 0;
        this.sbStorage.currentHistory.ts = Math.floor(new Date().getTime() / 1000);
        this.sbStorage.setHistory(this.sbStorage.currentHistory);
        this.sbStorage.saveHistory()
    }
    for (var c = 0; c < this.sbPageManager.pageSpritePool.length; c++) {
        this.sbPageManager.pageSpritePool[c].visible = false;
        this.sbPageManager.pageSpritePool[c].pivot.x = 0;
        this.sbPageManager.pageSpritePool[c].pivot.y = 0;
        this.sbPageManager.pageSpritePool[c].rotation = 0;
        this.sbPageManager.pageSpritePool[c].layLink.visible = false;
        this.sbPageManager.pageSpritePool[c].layFigure.visible = false;
        SBFunction.setEmptyGraphics(this.sbPageManager.pageSpritePool[c].selfMask, this.config.pageSize.width, this.config.pageSize.height, 65280, 1)
    }
    this.sbContainer.spriteRev.visible = false;
    this.sbPageManager.curlShadow.visible = false;
    this.drawPage(d);
    if (this.docInfo.DIRECTION == 0) {
        this.drawPage(d + 1);
        this.drawPage(d - 1)
    } else {
        this.drawPage(d - 1);
        this.drawPage(d + 1)
    }
    this.refresh()
};
SBViewer.prototype.drawPage = function(c) {
    var a = this.getDevPageMember(c);
    if (a.leftPage > 0) {
        var b = this.sbPageManager.getPageSprite(a.leftPage);
        b.x = this.config.pageMargin + (c - 1) * (this.config.devPageWidth + this.config.pageMargin);
        b.y = this.config.frameMargin.height;
        b.width = this.config.pageSize.width;
        b.height = this.config.pageSize.height;
        b.visible = true;
        b.layLink.visible = !this.config.figureMode;
        b.layFigure.visible = true
    }
    if (a.rightPage > 0 && a.leftPage != a.rightPage) {
        var b = this.sbPageManager.getPageSprite(a.rightPage);
        b.x = this.config.pageMargin + (c - 1) * (this.config.devPageWidth + this.config.pageMargin) + this.config.pageSize.width;
        b.y = this.config.frameMargin.height;
        b.width = this.config.pageSize.width;
        b.height = this.config.pageSize.height;
        b.visible = true;
        b.layLink.visible = !this.config.figureMode;
        b.layFigure.visible = true
    }
};
SBViewer.prototype.setPageInfo = function(a) {
    this.sbControl.infoBox.pagelabel.text = a.firstPage + " / " + this.docInfo.FILPAGE;
    this.sbControl.infoBox.pagelabel.x = parseInt((this.sbControl.infoBox.width - this.sbControl.infoBox.pagelabel.width) / 2);
    this.sbControl.slider.slider.setValue(a.devPage)
};
SBViewer.prototype.movePage = function(e, b) {
    if (e < 1 || e > this.docInfo.FILPAGE) {
        e = 1
    }
    var c = this.convDevPageFromPage(e);
    var f = this.getCardinalPoint(c, this.sbContainer.scale);
    var a = f.x;
    if (b) {
        if ("movePage" in this.animationPool) {
            if (this.animationPool.movePage.playing) {
                this.animationPool.movePage.skip()
            }
        }
        var d = new SBAnime(this.sbContainer.innerContainer);
        d.positionTo = new PIXI.Point(a, 0);
        d.duration = 800;
        d.acceleration = SBAnime.EaseOutCubic;
        d.onEnd = function(g) {
            this.drawPages()
        }.bind(this);
        this.addAnimation(d, "movePage", 1)
    } else {
        this.sbContainer.innerContainer.x = a;
        this.sbContainer.innerContainer.y = 0;
        this.drawPages()
    }
};
SBViewer.prototype.changeFigureEditMode = function(a) {
    this.config.figureMode = a;
    if (a) {
        this.sbControl.setVisible(false);
        this.sbFigControl.setEnableUndoRedoButton();
        this.sbFigControl.setVisible(true);
        this.sbInteraction.overlapObject = this.sbFigControl.control
    } else {
        this.sbControl.setVisible(true);
        this.sbFigControl.setVisible(false);
        this.sbInteraction.overlapObject = this.sbControl.control;
        if (this.docInfo.CURLMODE && this.sbContainer.scale == 1) {
            if (this.getCardinalPoint(this.getCenterDevPage(), 1).x != this.sbContainer.innerContainer.x) {
                this.movePage(this.getDevPageMember(this.getCenterDevPage()).firstPage, true)
            }
        }
    }
    this.drawPages(true)
};
SBViewer.prototype.getCardinalPoint = function(b, d) {
    var a = -((b - 1) * d * (this.config.devPageWidth + this.config.pageMargin)) - this.config.pageMargin;
    var c = (this.config.screenSize.width - this.config.devPageWidth * d) / 2;
    return new PIXI.Point(parseInt(a + c), 0)
};
SBViewer.prototype.getCenterDevPage = function() {
    var a = this.getDevPageFromPoint(new PIXI.Point(this.config.screenSize.width / 2, 0));
    return a
};
SBViewer.prototype.getDevPageFromPoint = function(a) {
    var b = Math.ceil((a.x - this.config.pageMargin * this.sbContainer.innerContainer.scale.x / 2 - this.sbContainer.innerContainer.x) / this.sbContainer.innerContainer.scale.x / (this.config.devPageWidth + this.config.pageMargin));
    return b
};
SBViewer.prototype.getPageFromPoint = function(b) {
    var d = 0;
    var c = this.getDevPageFromPoint(b);
    var a = this.getDevPageMember(c);
    if (this.config.dualPage) {
        var e = (b.x - this.config.pageMargin * this.sbContainer.innerContainer.scale.x / 2 - this.sbContainer.innerContainer.x) / this.sbContainer.innerContainer.scale.x / (this.config.devPageWidth + this.config.pageMargin);
        d = (c - e) > 0.5 ? a.leftPage : a.rightPage
    } else {
        d = a.firstPage
    }
    return d
};
SBViewer.prototype.getPageSpriteFromPoint = function(b) {
    var d = this.getDevPageFromPoint(b);
    var a = this.getDevPageMember(d);
    var f = null;
    if (a.leftPage > 0) {
        var c = this.sbPageManager.getPageSprite(a.leftPage);
        var e = this.sbContainer.innerContainer.toLocal(b, this.sbContainer.container);
        if (c.x <= e.x && e.x < c.x + c.width && c.y <= e.y && e.y < c.y + c.height) {
            f = c
        }
    }
    if (a.rightPage > 0 && a.leftPage != a.rightPage) {
        var c = this.sbPageManager.getPageSprite(a.rightPage);
        var e = this.sbContainer.innerContainer.toLocal(b, this.sbContainer.container);
        if (c.x <= e.x && e.x < c.x + c.width && c.y <= e.y && e.y < c.y + c.height) {
            f = c
        }
    }
    return f
};
SBViewer.prototype.convDevPageFromPage = function(d) {
    var a = this.docInfo.HASCOVER ? 1 : 0;
    var b = this.config.dualPage ? 2 : 1;
    if (b == 1) {
        a = 0
    }
    var c = 0;
    if (this.docInfo.DIRECTION == 0) {
        c = parseInt((d + a - 1) / b) + 1
    } else {
        c = this.config.totalDevPages - parseInt((d + a - 1) / b)
    }
    return c
};
SBViewer.prototype.getDevPageMember = function(d) {
    var b = {
        devPage: d,
        firstPage: 0,
        leftPage: 0,
        rightPage: 0
    };
    var a = this.docInfo.HASCOVER ? 1 : 0;
    var c = this.config.dualPage ? 2 : 1;
    if (c == 1) {
        a = 0
    }
    if (d >= 1 && d <= this.config.totalDevPages) {
        if (this.docInfo.DIRECTION == 0) {
            b.leftPage = (d - 1) * c - a + 1;
            b.rightPage = c == 1 ? b.leftPage : b.leftPage + 1
        } else {
            b.rightPage = (this.config.totalDevPages - d) * c - a + 1;
            b.leftPage = c == 1 ? b.rightPage : b.rightPage + 1
        }
        b.leftPage = (b.leftPage >= 1 && b.leftPage <= this.docInfo.FILPAGE) ? b.leftPage : 0;
        b.rightPage = (b.rightPage >= 1 && b.rightPage <= this.docInfo.FILPAGE) ? b.rightPage : 0;
        if (b.leftPage * b.rightPage == 0) {
            b.firstPage = b.leftPage + b.rightPage
        } else {
            b.firstPage = b.leftPage < b.rightPage ? b.leftPage : b.rightPage
        }
    }
    return b
};
SBViewer.prototype._draw = function() {
    if (SBDebug.ENABLED) {
        var b = this.getDevPageFromPoint(new PIXI.Point(0, 0));
        var a = this.getDevPageMember(b);
        g_SBDebug.setInfo("platform", navigator.platform);
        g_SBDebug.setInfo("pageInfo", "pageL=" + a.leftPage + " pageR=" + a.rightPage + " devPage=" + b);
        g_SBDebug.setInfo("pointer", g_SBDebug.mouseEvent.type + "(" + g_SBDebug.mouseEvent.x + "," + g_SBDebug.mouseEvent.y + ")");
        g_SBDebug.setInfo("event", g_SBDebug.mouseEvent.event);
        g_SBDebug.setInfo("window", "(0,0)-(" + this.config.screenSize.width + "," + this.config.screenSize.height + ")");
        g_SBDebug.setInfo("PixelRatio", SBViewer.PIXELRATIO + "(Render:" + SBViewer.RESOLUTION + ")")
    }
    this.sbContainer.relocation();
    this.renderer.render(this.sbContainer.container)
};
SBViewer.prototype.addAnimation = function(c, a, b) {
    if (a in this.animationPool) {
        if (b == 1) {
            this.animationPool[a].skip()
        }
        delete this.animationPool[a]
    }
    this.animationPool[a] = c;
    if (!this.animationPlaying) {
        this.refresh()
    }
};
SBViewer.prototype.setAnimationContinuity = function(a) {
    this.animationContinuity = a;
    if (!this.animationPlaying && a) {
        this.refresh()
    }
};
SBViewer.prototype.refresh = function() {
    cancelAnimationFrame(this.lastAnimationFrameID);
    this.lastAnimationFrameID = requestAnimationFrame(this._startAnimation.bind(this))
};
SBViewer.prototype._startAnimation = function() {
    for (var a in this.animationPool) {
        var b = this.animationPool[a];
        if (b.playing) {
            b.draw()
        }
        if (b.playing == false) {
            delete this.animationPool[a]
        }
    }
    this._draw();
    this.animationPlaying = Object.keys(this.animationPool).length > 0 || this.animationContinuity;
    if (this.animationPlaying) {
        this.refresh()
    } else {}
};
SBViewer.prototype.setMeetingStatus = function(a) {
    var c = !(a == null);
    if (this.sbControl.titleBar.btMeeting.button.visible != c) {
        this.sbControl.titleBar.btMeeting.button.visible = c;
        this.refresh()
    }
    var b = this.currentMetSeq;
    if (a != null) {
        this.currentMetSeq = a.METSEQ
    }
    if (this.currentMetSeq != b) {
        this.refresh()
    }
};
SBViewer.prototype.showThumbView = function() {
    this.sbControl.showThumbView()
};
var SBContainer = function(a) {
    this.sbViewer = a;
    this.container = new PIXI.Container();
    this.innerContainer = new PIXI.Container();
    this.scale = 1;
    this.dragStart = new PIXI.Point(0, 0);
    this.curlStep = 0;
    this.backView = new PIXI.Sprite(SBFunction.createEmptyTexture(10, 10, 0, 1));
    this.container.addChild(this.backView);
    this.container.addChild(this.innerContainer);
    this.drawingContainer = new PIXI.Container();
    this.container.addChild(this.drawingContainer);
    this.container.addChild(this.sbViewer.sbPageManager.curlShadow);
    for (var c = 0; c < SBViewer.PAGESPRITEPOOLLENGTH; c++) {
        var b = SBPageManager.createPageSprite();
        this.innerContainer.addChild(b);
        this.sbViewer.sbPageManager.pageSpritePool[c] = b
    }
    this.spriteRev = SBPageManager.createPageSprite();
    this.spriteRev.layPage.alpha = 0.1;
    this.spriteRev.layLoading.visible = false;
    this.innerContainer.addChild(this.spriteRev)
};
SBContainer.prototype.interactiveCallback = function(s) {
    if (SBDebug.ENABLED) {
        g_SBDebug.mouseEvent.event = s.type + "(" + s.x + "," + s.y + ")-(" + s.dx + "," + s.dy + ") sc:" + s.sc + " sp(" + s.spx + "," + s.spy + ")"
    }
    if ((s.type == "keydown" && s.ctl) || s.pointer == "plus" || s.pointer == "minus") {
        var q = false;
        if (s.pointer == "up" || s.pointer == "plus") {
            s.sc = -1;
            q = true
        } else {
            if (s.pointer == "down" || s.pointer == "minus") {
                s.sc = 1;
                q = true
            }
        }
        if (q) {
            s.type = "wheel";
            s.pointer = "mouse";
            s.x = parseInt(this.sbViewer.config.screenSize.width / 2);
            s.y = parseInt(this.sbViewer.config.screenSize.height / 2)
        }
    }
    var u = true;
    var o = this.sbViewer.convDevPageFromPage(this.sbViewer.config.currentPage);
    switch (s.type) {
        case "down":
            if ("movePage" in this.sbViewer.animationPool) {
                this.sbViewer.animationPool.movePage.skip()
            }
            if ("inertiaMove" in this.sbViewer.animationPool) {
                delete this.sbViewer.animationPool.inertiaMove
            }
            if (this.sbViewer.config.figureMode) {
                this.sbViewer.sbFigure.interactiveCallback(s)
            } else {}
            break;
        case "firstClick":
            if (this.sbViewer.config.figureMode) {
                this.sbViewer.sbFigure.interactiveCallback(s);
                u = false;
                break
            } else {
                var r = s.pointer == "mouse" ? 0 : this.sbViewer.config.barHeight / 12;
                var i = this.sbViewer.sbLinkAnnot.getLinkAnnot(this.sbViewer, new PIXI.Point(s.x, s.y), r);
                if (i) {
                    this.sbViewer.sbControl.setVisible(false);
                    this.sbViewer.setAnimationContinuity(true);
                    if (i.page > 0) {
                        this.sbViewer.movePage(i.page, false)
                    } else {
                        if (i.uri != "") {
                            window.open(i.uri, "_blank")
                        }
                    }
                    u = false;
                    break
                } else {
                    if (s.x < this.sbViewer.config.turnPageAreaWidth || s.x > this.sbViewer.config.screenSize.width - this.sbViewer.config.turnPageAreaWidth) {
                        u = false
                    } else {
                        break
                    }
                }
            }
            case "click":
                if (this.sbViewer.config.figureMode) {
                    this.sbViewer.sbFigure.interactiveCallback(s)
                } else {
                    var b = 0;
                    if (s.x < this.sbViewer.config.turnPageAreaWidth) {
                        b = -1
                    } else {
                        if (s.x > this.sbViewer.config.screenSize.width - this.sbViewer.config.turnPageAreaWidth) {
                            b = 1
                        }
                    }
                    b = o + b > 0 && o + b <= this.sbViewer.config.totalDevPages ? b : 0;
                    if (b != 0) {
                        this.sbViewer.sbControl.setVisible(false);
                        if (this.sbViewer.docInfo.CURLMODE && this.scale == 1) {
                            var l = new SBAnime(this.innerContainer);
                            l.duration = 800;
                            l.acceleration = SBAnime.EaseOutCubic;
                            l.functionTo = function(z) {
                                var y = (b) * z;
                                this.drawCurl(y, s.y * 2 / this.sbViewer.config.screenSize.height - 1)
                            }.bind(this);
                            l.onEnd = function(y) {
                                this.sbViewer.movePage(this.sbViewer.getDevPageMember(o + b).firstPage, false)
                            }.bind(this);
                            this.sbViewer.addAnimation(l, "movePage", 1)
                        } else {
                            this.sbViewer.movePage(this.sbViewer.getDevPageMember(o + b).firstPage, true)
                        }
                    } else {
                        this.sbViewer.sbControl.setVisible(!this.sbViewer.sbControl.visible)
                    }
                }
                break;
            case "dblClick":
                if (this.sbViewer.config.figureMode) {
                    this.sbViewer.sbFigure.interactiveCallback(s)
                } else {
                    this.sbViewer.sbControl.setVisible(false);
                    var l = new SBAnime(this.innerContainer);
                    l.duration = 600;
                    l.acceleration = SBAnime.EaseOutQuint;
                    var v = this.scale;
                    var d = v != 1 ? 1 : 2;
                    var k = new PIXI.Point(s.x, s.y);
                    var w = new PIXI.Point(s.x, s.y);
                    var m = this.innerContainer.toLocal(k, this.container);
                    if (d == 1) {
                        var h = this.sbViewer.getCardinalPoint(this.sbViewer.getDevPageFromPoint(new PIXI.Point(s.x, s.y)), d);
                        w.x = m.x + h.x;
                        w.y = m.y
                    }
                    l.functionTo = function(A) {
                        var z = k.x + (w.x - k.x) * A;
                        var B = k.y + (w.y - k.y) * A;
                        this.scale = v + (d - v) * A;
                        this.moveAndZoom(z, B, m.x, m.y, this.scale)
                    }.bind(this);
                    l.onEnd = function(y) {
                        this.sbViewer.drawPages()
                    }.bind(this);
                    this.sbViewer.addAnimation(l, "movePage", 0)
                }
                break;
            case "dragStart":
                if (this.sbViewer.config.figureMode) {
                    this.sbViewer.sbFigure.interactiveCallback(s)
                } else {
                    this.sbViewer.sbControl.setVisible(false);
                    this.sbViewer.setAnimationContinuity(true);
                    this.dragStart = this.innerContainer.toLocal(new PIXI.Point(s.dx, s.dy), this.container)
                }
                break;
            case "drag":
                if (this.sbViewer.config.figureMode) {
                    this.sbViewer.sbFigure.interactiveCallback(s)
                } else {
                    if (this.sbViewer.docInfo.CURLMODE && this.scale == 1) {
                        var e = (s.dx - s.x) / (this.sbViewer.config.devPageWidth * 0.6);
                        if (e > 1) {
                            e = 1
                        }
                        if (e < -1) {
                            e = -1
                        }
                        var b = e >= 0 ? 1 : -1;
                        e = (o + b > 0 && o + b <= this.sbViewer.config.totalDevPages) ? e : 0;
                        var f = s.y * 2 / this.sbViewer.config.screenSize.height - 1;
                        if (f > 1) {
                            f = 1
                        }
                        if (f < -1) {
                            f = -1
                        }
                        this.drawCurl(e, f)
                    } else {
                        this.moveAndZoom(s.x, s.y, this.dragStart.x, this.dragStart.y, this.scale * s.sc)
                    }
                }
                break;
            case "dragEnd":
                if (this.sbViewer.config.figureMode) {
                    this.sbViewer.sbFigure.interactiveCallback(s)
                } else {
                    this.sbViewer.setAnimationContinuity(false);
                    var t = Math.sqrt(Math.pow(s.spx, 2) + Math.pow(s.spy, 2));
                    if (this.sbViewer.docInfo.CURLMODE && this.scale == 1) {
                        var j = this.curlStep;
                        var c = 0;
                        if (t > 0) {
                            if (s.spx * this.curlStep < 0) {
                                if (this.curlStep > 0) {
                                    c = 1
                                }
                                if (this.curlStep < 0) {
                                    c = -1
                                }
                            }
                        } else {
                            if (Math.abs(this.curlStep) > 0.5) {
                                if (this.curlStep > 0) {
                                    c = 1
                                }
                                if (this.curlStep < 0) {
                                    c = -1
                                }
                            }
                        }
                        if (Math.abs(this.curlStep) == 1) {
                            this.sbViewer.movePage(this.sbViewer.getDevPageMember(o + c).firstPage, false)
                        } else {
                            if (this.curlStep == 0 && this.sbViewer.getCardinalPoint(this.sbViewer.getCenterDevPage(), 1).x != this.innerContainer.x) {
                                if (this.curlStep != 0) {
                                    this.sbViewer.drawPages()
                                }
                                this.sbViewer.movePage(this.sbViewer.getDevPageMember(this.sbViewer.getCenterDevPage()).firstPage, true)
                            } else {
                                var l = new SBAnime(this.innerContainer);
                                l.duration = 800 * Math.abs(this.curlStep - c);
                                l.acceleration = SBAnime.EaseOutCubic;
                                l.functionTo = function(z) {
                                    var y = (c - j) * z + j;
                                    this.drawCurl(y, s.y * 2 / this.sbViewer.config.screenSize.height - 1)
                                }.bind(this);
                                l.onEnd = function(y) {
                                    this.sbViewer.movePage(this.sbViewer.getDevPageMember(o + c).firstPage, false)
                                }.bind(this);
                                this.sbViewer.addAnimation(l, "movePage", 0)
                            }
                        }
                    } else {
                        if (t > 0) {
                            if (this.scale == 1) {
                                var b = (s.spx > 0) ? -1 : 1;
                                if (o + b > 0 && o + b <= this.sbViewer.config.totalDevPages) {
                                    this.sbViewer.movePage(this.sbViewer.getDevPageMember(o + b).firstPage, true)
                                }
                            } else {
                                var x = this.innerContainer.toLocal(new PIXI.Point(s.x, s.y), this.container);
                                x.x = -x.x * this.scale + s.x;
                                x.y = -x.y * this.scale + s.y;
                                var l = new SBAnime(this.innerContainer);
                                l.duration = 1500;
                                l.acceleration = SBAnime.EaseOutCubic;
                                l.functionTo = function(y) {
                                    this.innerContainer.x = x.x + s.spx / 3 * y;
                                    this.innerContainer.y = x.y + s.spy / 3 * y;
                                    this.sbViewer.drawPages()
                                }.bind(this);
                                l.onEnd = function(y) {
                                    this.sbViewer.drawPages()
                                }.bind(this);
                                this.sbViewer.addAnimation(l, "inertiaMove", 0)
                            }
                        } else {
                            this.sbViewer.drawPages()
                        }
                    }
                }
                break;
            case "pinchStart":
                this.sbViewer.sbControl.setVisible(false);
                this.sbViewer.setAnimationContinuity(true);
                this.dragStart = this.innerContainer.toLocal(new PIXI.Point(s.dx, s.dy), this.container);
                if (this.curlStep != 0) {
                    this.sbViewer.drawPages()
                }
                break;
            case "pinch":
                var p = this.scale * s.sc;
                if (p > 1024) {
                    p = 1024
                }
                this.moveAndZoom(s.x, s.y, this.dragStart.x, this.dragStart.y, p);
                break;
            case "pinchEnd":
                this.sbViewer.setAnimationContinuity(false);
                if (this.innerContainer.scale.x < 1.05) {
                    this.scale = 1;
                    this.innerContainer.scale.x = this.innerContainer.scale.y = this.scale;
                    this.curlStep = 0
                }
                this.scale = this.innerContainer.scale.x;
                if (this.sbViewer.docInfo.CURLMODE && this.scale == 1 && !this.sbViewer.config.figureMode) {
                    if (this.sbViewer.getCardinalPoint(this.sbViewer.getCenterDevPage(), 1).x != this.innerContainer.x) {
                        this.sbViewer.movePage(this.sbViewer.getDevPageMember(this.sbViewer.getCenterDevPage()).firstPage, true)
                    }
                }
                break;
            case "wheel":
                if (Math.abs(this.curlStep) in [0, 1]) {
                    if ("movePage" in this.sbViewer.animationPool) {
                        this.sbViewer.animationPool.movePage.skip()
                    }
                    if ("inertiaMove" in this.sbViewer.animationPool) {
                        delete this.sbViewer.animationPool.inertiaMove
                    }
                    var p = s.sc < 0 ? this.scale * 1.1 : this.scale / 1.1;
                    if (p < 1.05) {
                        p = 1
                    }
                    if (p > 1024) {
                        p = 1024
                    }
                    var n = this.innerContainer.toLocal(new PIXI.Point(s.x, s.y), this.container);
                    this.moveAndZoom(s.x, s.y, n.x, n.y, p);
                    this.scale = this.innerContainer.scale.x;
                    if (this.sbViewer.sbFigure.dragPin.visible) {
                        this.relocation();
                        this.sbViewer.sbFigure.drawDragPin()
                    }
                    this.sbViewer.refresh();
                    if (this.sbViewer.docInfo.CURLMODE && this.scale == 1 && !this.sbViewer.config.figureMode) {
                        if (this.sbViewer.getCardinalPoint(this.sbViewer.getCenterDevPage(), 1).x != this.innerContainer.x) {
                            this.sbViewer.movePage(this.sbViewer.getDevPageMember(this.sbViewer.getCenterDevPage()).firstPage, true)
                        }
                    }
                }
                break;
            case "keydown":
                if (s.pointer == "esc") {
                    if (this.sbViewer.config.figureMode) {
                        if (this.sbViewer.sbFigure.textEditMode) {} else {
                            this.sbViewer.sbFigure.sbFigControl.toolBar.btExit.onClick()
                        }
                    } else {
                        if (this.sbViewer.sbControl.visible) {
                            if (this.sbViewer.sbControl.titleBar.btExit.button.visible) {
                                this.sbViewer.sbControl.titleBar.btExit.onClick()
                            }
                        } else {
                            this.sbViewer.sbControl.setVisible(true)
                        }
                    }
                    break
                }
                var b = 0;
                if (s.pointer == "left" && this.scale == 1) {
                    b = -1
                } else {
                    if (s.pointer == "right" && this.scale == 1) {
                        b = 1
                    } else {
                        if (s.pointer == "enter" || s.pointer == "space") {
                            b = this.sbViewer.docInfo.DIRECTION == 0 ? 1 : -1
                        }
                    }
                }
                b = o + b > 0 && o + b <= this.sbViewer.config.totalDevPages ? b : 0;
                if (b != 0) {
                    this.sbViewer.sbControl.setVisible(false);
                    if (this.sbViewer.config.figureMode) {
                        this.sbViewer.sbFigure.pickedFigure = null;
                        this.sbViewer.sbFigure.drawDragPin()
                    }
                    if (this.sbViewer.docInfo.CURLMODE && this.scale == 1) {
                        if ("movePage" in this.sbViewer.animationPool) {
                            this.sbViewer.animationPool.movePage.skip()
                        } else {
                            var l = new SBAnime(this.innerContainer);
                            l.duration = 800;
                            l.acceleration = SBAnime.EaseOutCubic;
                            l.functionTo = function(z) {
                                var y = (b) * z;
                                this.drawCurl(y, 0.5)
                            }.bind(this);
                            l.onEnd = function(y) {
                                this.sbViewer.movePage(this.sbViewer.getDevPageMember(o + b).firstPage, false)
                            }.bind(this);
                            this.sbViewer.addAnimation(l, "movePage", 1)
                        }
                    } else {
                        this.sbViewer.movePage(this.sbViewer.getDevPageMember(o + b).firstPage, true)
                    }
                } else {
                    if (this.scale > 1) {
                        var a = {
                            x: 0,
                            y: 0
                        };
                        if (s.pointer == "left") {
                            a.x = -1
                        } else {
                            if (s.pointer == "right") {
                                a.x = 1
                            } else {
                                if (s.pointer == "up") {
                                    a.y = -1
                                } else {
                                    if (s.pointer == "down") {
                                        a.y = 1
                                    }
                                }
                            }
                        }
                        if (a.x != 0 || a.y != 0) {
                            var g = this.sbViewer.config.barHeight;
                            var k = new PIXI.Point(parseInt(this.sbViewer.config.screenSize.width / 2), parseInt(this.sbViewer.config.screenSize.height / 2));
                            var w = new PIXI.Point(k.x + g * a.x, k.y + g * a.y);
                            var m = this.innerContainer.toLocal(w, this.container);
                            this.moveAndZoom(k.x, k.y, m.x, m.y, this.scale);
                            if (this.sbViewer.sbFigure.dragPin.visible) {
                                this.relocation();
                                this.sbViewer.sbFigure.drawDragPin()
                            }
                            this.sbViewer.drawPages();
                            this.sbViewer.refresh()
                        }
                    }
                }
                break
    }
    return u
};
SBContainer.prototype.moveAndZoom = function(a, e, c, b, d) {
    if (d < 1) {
        d = 1
    }
    this.innerContainer.x = -c * d + a;
    this.innerContainer.y = -b * d + e;
    this.innerContainer.scale.x = this.innerContainer.scale.y = d
};
SBContainer.prototype.relocation = function() {
    var d = this.innerContainer.scale.x;
    var i = (this.sbViewer.config.screenSize.width - this.sbViewer.config.devPageWidth * d) / 2;
    i = i > 0 ? i : 0;
    var h = -this.sbViewer.config.pageMargin * d + i;
    var f = this.sbViewer.config.screenSize.width - this.innerContainer.wldWidth * d - i;
    var g = (this.sbViewer.config.screenSize.height - this.sbViewer.config.pageSize.height * d) / 2;
    g = g > 0 ? g : 0;
    var e = -this.sbViewer.config.frameMargin.height * d + g;
    var c = this.sbViewer.config.screenSize.height - (this.innerContainer.wldHeight - this.sbViewer.config.frameMargin.height) * d - g;
    if (this.innerContainer.x > h) {
        this.innerContainer.x = h
    }
    if (this.innerContainer.x < f) {
        this.innerContainer.x = f
    }
    if (this.innerContainer.y > e) {
        this.innerContainer.y = e
    }
    if (this.innerContainer.y < c) {
        this.innerContainer.y = c
    }
    var b = this.sbViewer.config;
    var a = Math.ceil((b.screenSize.width - b.devPageWidth * d) / 2);
    if (a < 0) {
        a = 0
    }
    b.turnPageAreaWidth = a + parseInt((b.screenSize.width < b.screenSize.height ? b.screenSize.width : b.screenSize.height) / 12)
};
SBContainer.prototype.drawCurl = function(i, m) {
    var v = (m * 160 * Math.PI) / 180;
    var t = this.sbViewer.getCenterDevPage();
    var y = this.sbViewer.config;
    var x = this.sbViewer.docInfo.DIRECTION == 0 ? 1 : -1;
    var q = this.sbViewer.getDevPageMember(t);
    var k = this.sbViewer.sbPageManager.curlShadow;
    var w = i;
    if (this.curlStep * i < 0) {
        this.sbViewer.drawPages()
    }
    this.curlStep = i;
    if (!y.dualPage) {
        if (i < 0 && x == 1) {
            w += 1
        }
        if (i >= 0 && x == -1) {
            w -= 1
        }
    }
    var u = 0;
    var o = 0;
    var p = Math.abs(w) * 1.1;
    if (p > 1) {
        p = 1
    }
    v *= (1 - p);
    var r = new PIXI.Point(0, 0);
    var z = new PIXI.Point(0, 0);
    var l = new PIXI.Point(0, 0);
    var s = new PIXI.Point(0, 0);
    if (w >= 0) {
        r.x = z.x = 0;
        l.x = y.pageSize.width * w;
        s.x = l.x - y.pageSize.height * Math.tan(v / 2) * (v > 0 ? 1 : -1)
    } else {
        r.x = z.x = y.pageSize.width;
        l.x = y.pageSize.width + y.pageSize.width * w;
        s.x = l.x + y.pageSize.height * Math.tan(v / 2) * (v > 0 ? 1 : -1)
    }
    if (v > 0) {
        r.y = l.y = y.pageSize.height;
        z.y = s.y = 0;
        if (s.x < 0) {
            s.x = 0;
            s.y = y.pageSize.height - l.x / Math.tan(v / 2)
        }
        if (s.x > y.pageSize.width) {
            s.x = y.pageSize.width;
            s.y = y.pageSize.height - (y.pageSize.width - l.x) / Math.tan(v / 2)
        }
    } else {
        r.y = l.y = 0;
        z.y = s.y = y.pageSize.height;
        if (s.x < 0) {
            s.x = 0;
            s.y = -l.x / Math.tan(v / 2)
        }
        if (s.x > y.pageSize.width) {
            s.x = y.pageSize.width;
            s.y = -(y.pageSize.width - l.x) / Math.tan(v / 2)
        }
    }
    var n = new PIXI.Point(0, 0);
    var j = new PIXI.Point(l.x, l.y);
    var h = new PIXI.Point(y.pageMargin + (t - 1) * (y.devPageWidth + y.pageMargin), y.frameMargin.height);
    var e = new PIXI.Point(y.pageMargin + (t - 1) * (y.devPageWidth + y.pageMargin) - y.pageSize.width * w, y.frameMargin.height + l.y);
    if (y.dualPage) {
        if (w >= 0) {
            u = q.rightPage + 2 * x;
            o = q.rightPage + 1 * x;
            h.x += y.pageSize.width;
            e.x += y.devPageWidth
        } else {
            u = q.leftPage - 2 * x;
            o = q.leftPage - 1 * x;
            v *= -1
        }
    } else {
        if (x == 1) {
            if (i >= 0) {
                u = q.firstPage + 1;
                o = -q.firstPage;
                h.x += 0;
                e.x += y.devPageWidth
            } else {
                u = q.firstPage;
                o = -(q.firstPage - 1);
                h.x += 0;
                e.x += y.devPageWidth;
                this.sbViewer.sbPageManager.getPageSprite(q.firstPage - 1).position = new PIXI.Point(h.x, h.y)
            }
        } else {
            if (i >= 0) {
                u = q.firstPage;
                o = -(q.firstPage - 1);
                h.x += 0;
                this.sbViewer.sbPageManager.getPageSprite(q.firstPage - 1).position = new PIXI.Point(h.x, h.y)
            } else {
                u = q.firstPage + 1;
                o = -q.firstPage;
                h.x += 0
            }
            v *= -1
        }
    }
    var g = this.sbViewer.sbPageManager.getPageSprite(u);
    this.innerContainer.removeChild(g);
    this.innerContainer.addChild(g);
    g.position = h;
    g.pivot = n;
    g.visible = true;
    var d;
    if (o >= 0) {
        d = this.sbViewer.sbPageManager.getPageSprite(o)
    } else {
        d = this.spriteRev;
        d.layPage.scale = new PIXI.Point(1, 1);
        d.layPage.texture = this.sbViewer.sbPageManager.getPageSprite(-o).layPage.texture;
        d.hasImage = true;
        this.sbViewer.sbPageManager.setPageChildrenFrame(d);
        d.layPage.x += d.layPage.width;
        d.layPage.width *= -1
    }
    this.innerContainer.removeChild(d);
    this.innerContainer.addChild(d);
    d.position = e;
    d.pivot = j;
    d.rotation = v;
    d.visible = true;
    var b = g.selfMask;
    b.clear();
    b.beginFill(65280);
    b.moveTo(y.pageSize.width - r.x, r.y);
    b.lineTo(y.pageSize.width - l.x, l.y);
    b.lineTo(y.pageSize.width - s.x, s.y);
    b.lineTo(y.pageSize.width - z.x, z.y);
    b.endFill();
    var a = d.selfMask;
    a.clear();
    a.beginFill(255, 0.5);
    a.moveTo(r.x, r.y);
    a.lineTo(l.x, l.y);
    a.lineTo(s.x, s.y);
    a.lineTo(z.x, z.y);
    a.endFill();
    var c = (1 - Math.abs(w)) * 10;
    if (c > 1) {
        c = 1
    }
    k.alpha = c;
    k.position = this.innerContainer.toGlobal(e);
    k.pivot = j;
    k.rotation = v;
    k.visible = true;
    k.textureFS.scale.y = 1;
    k.textureFS.position = new PIXI.Point((l.x + s.x) / 2, (l.y + s.y) / 2);
    k.textureFS.rotation = -v / 2 + (w >= 0 ? 0 : Math.PI);
    var f = Math.sqrt(Math.pow(l.x - s.x, 2) + Math.pow(l.y - s.y, 2));
    k.textureFS.height = f + Math.abs(k.textureFS.width / Math.cos(v / 2) * 2);
    k.textureMS.scale.y = 1;
    k.textureMS.position = new PIXI.Point((l.x + s.x) / 2, (l.y + s.y) / 2);
    k.textureMS.rotation = -v / 2 + (w >= 0 ? 0 : Math.PI);
    k.textureMS.height = f + Math.abs(k.textureMS.width / Math.cos(v / 2) * 2);
    k.outerMask.clear();
    k.outerMask.beginFill(16711680, 1);
    k.outerMask.moveTo(r.x, r.y);
    k.outerMask.lineTo(l.x, l.y);
    k.outerMask.lineTo(s.x, s.y);
    k.outerMask.lineTo(z.x, z.y);
    k.outerMask.endFill();
    k.outerMask.pivot = new PIXI.Point((l.x + s.x) / 2, (l.y + s.y) / 2);
    k.outerMask.position = new PIXI.Point((l.x + s.x) / 2, (l.y + s.y) / 2);
    k.outerMask.scale = new PIXI.Point(8, 8);
    k.msMask.clear();
    k.msMask.beginFill(65280, 1);
    k.msMask.moveTo(r.x, r.y);
    k.msMask.lineTo(l.x, l.y);
    k.msMask.lineTo(s.x, s.y);
    k.msMask.lineTo(z.x, z.y);
    k.msMask.endFill()
};
var SBInteraction = function(a, c) {
    this.sbViewer = a;
    this.callbackObserver = c;
    this.touches = new Array();
    this.lastAction = {
        type: "none"
    };
    this.lastAgoAction = {
        type: "none"
    };
    this.lastTime = 0;
    this.lastClickTime = 0;
    this.click = null;
    this.lastPinchEndTime = 0;
    this.interactionManager = new PIXI.interaction.InteractionManager(a.renderer);
    this.overlapObject = null;
    this.draggingSensitive = 1;
    this.dragStartSensitive = 1;
    var b = a.renderer.view;
    if (window.PointerEvent) {
        b.addEventListener("MSHoldVisual", function(d) {
            d.preventDefault()
        }, false);
        b.addEventListener("contextmenu", function(d) {
            d.preventDefault()
        }, false);
        b.addEventListener("pointerdown", this.onPointerAction.bind(this), false);
        b.addEventListener("pointerup", this.onPointerAction.bind(this), false);
        b.addEventListener("pointermove", this.onPointerAction.bind(this), false);
        b.addEventListener("pointercancel", this.reset.bind(this), false);
        b.addEventListener("pointerout", this.onPointerAction.bind(this), false)
    } else {
        container = a.sbContainer.container;
        container.interactive = true;
        container.mousedown = container.touchstart = this.onPointerAction.bind(this);
        container.mouseup = container.touchend = this.onPointerAction.bind(this);
        container.mousemove = container.touchmove = this.onPointerAction.bind(this);
        container.mouseupoutside = container.touchendoutside = this.onPointerAction.bind(this);
        b.addEventListener("touchcancel", this.reset.bind(this), false)
    }
    b.addEventListener("wheel", this.onWheelAction.bind(this), false);
    window.addEventListener("keydown", this.onKeyDownAction.bind(this), false);
    b.addEventListener("mousecancel", this.reset.bind(this), false)
};
SBInteraction.prototype.reset = function() {
    this.touches = new Array();
    this.lastAction = {
        type: "none"
    };
    this.lastAgoAction = {
        type: "none"
    };
    this.lastTime = 0;
    this.lastPinchEndTime = 0;
    this.lastClickTime = 0;
    this.click = null
};
SBInteraction.prototype.onPointerAction = function(a) {
    if (!this.sbViewer.enabled) {
        return
    }
    var h = {
        id: 0,
        type: "",
        pointer: "",
        dx: 0,
        dy: 0,
        x: 0,
        y: 0,
        ctlKey: false,
        time: 0
    };
    h.time = new Date().getTime();
    if ("pointerType" in a) {
        if (a.button > 0) {
            return
        }
        h.id = a.pointerId;
        switch (a.type) {
            case "pointerdown":
                h.action = "down";
                break;
            case "pointerup":
                h.action = "up";
                break;
            case "pointerout":
                h.action = "up";
                break;
            case "pointermove":
                h.action = "move";
                break;
            case "touchend":
                h.action = "up";
                break;
            case "touchmove":
                h.action = "move";
                break
        }
        h.x = a.x;
        h.y = a.y;
        h.ctlKey = ((a.ctrlKey && !a.metaKey) || (!a.ctrlKey && a.metaKey))
    } else {
        if (a.data.originalEvent.button > 0) {
            return
        }
        h.id = "identifier" in a.data ? a.data.identifier : "mouse";
        switch (a.type) {
            case "mousedown":
                h.action = "down";
                h.pointer = "mouse";
                break;
            case "touchstart":
                h.action = "down";
                h.pointer = "touch";
                break;
            case "mouseup":
                h.action = "up";
                h.pointer = "mouse";
                break;
            case "touchend":
                h.action = "up";
                h.pointer = "touch";
                break;
            case "mouseupoutside":
                h.action = "up";
                h.pointer = "mouse";
                break;
            case "touchendoutside":
                h.action = "up";
                h.pointer = "touch";
                break;
            case "mousemove":
                h.action = "move";
                h.pointer = "mouse";
                break;
            case "touchmove":
                h.action = "move";
                h.pointer = "touch";
                break
        }
        h.x = a.data.global.x;
        h.y = a.data.global.y;
        var g = a.data.originalEvent;
        h.ctlKey = ((g.ctrlKey && !g.metaKey) || (!g.ctrlKey && g.metaKey))
    }
    if (this.overlapObject) {
        if (this.interactionManager.processInteractive(new PIXI.Point(h.x, h.y), this.overlapObject, function() {}, true)) {
            if (this.lastAction.type != "dragStart" && this.lastAction.type != "drag" && this.lastAction.type != "pinchStart" && this.lastAction.type != "pinch") {
                return
            }
        }
    }
    var d = {
        type: "none",
        x: 0,
        y: 0,
        dx: 0,
        dy: 0,
        sc: 1,
        spx: 0,
        spy: 0,
        ctl: h.ctlKey,
        pointer: h.pointer
    };
    if ("id" + h.id in this.touches) {
        this.touches["id" + h.id].x = h.x;
        this.touches["id" + h.id].y = h.y;
        h.dx = this.touches["id" + h.id].dx;
        h.dy = this.touches["id" + h.id].dy
    }
    if (h.action == "down") {
        if (Object.keys(this.touches).length < 2 && !("id" + h.id in this.touches)) {
            this.dragStartSensitive = this.sbViewer.config.barHeight / 4;
            this.draggingSensitive = 1;
            if (Object.keys(this.touches).length == 0) {
                this.lastAction.type = "none"
            }
            h.dx = h.x;
            h.dy = h.y;
            this.touches["id" + h.id] = h;
            d.type = "down";
            d.x = h.x;
            d.y = h.y
        }
        this.sbViewer.userInteractiveAction()
    } else {
        if (h.action == "up") {
            if ("id" + h.id in this.touches) {
                if (Object.keys(this.touches).length == 1 && this.lastAction.type != "pinch") {
                    if (this.lastAction.type == "down") {
                        if (this.click && Math.pow(h.x - this.lastAction.x, 2) + Math.pow(h.y - this.lastAction.y, 2) < Math.pow(this.sbViewer.config.barHeight * 2, 2)) {
                            clearTimeout(this.click);
                            this.click = null;
                            d.type = "dblClick"
                        } else {
                            d.type = "firstClick"
                        }
                        d.x = h.x;
                        d.y = h.y;
                        this.lastClickTime = h.time
                    } else {
                        if (this.lastAction.type == "drag") {
                            d.type = "dragEnd";
                            d.x = h.x;
                            d.y = h.y;
                            d.dx = h.dx;
                            d.dy = h.dy;
                            if (h.time - this.lastTime < 100 && this.lastAgoAction.type == "drag") {
                                d.spx = this.lastAgoAction.spx;
                                d.spy = this.lastAgoAction.spy
                            }
                        }
                    }
                } else {
                    d.type = "pinchEnd";
                    d.sc = this.lastAction.sc
                }
                delete this.touches["id" + h.id]
            }
            if (Object.keys(this.touches).length == 0) {
                this.lastAction.type = "none"
            }
        } else {
            if (h.action == "move") {
                if ("id" + h.id in this.touches) {
                    if (Object.keys(this.touches).length == 1) {
                        if (Math.pow(h.x - this.lastAction.x, 2) + Math.pow(h.y - this.lastAction.y, 2) >= Math.pow(this.draggingSensitive, 2)) {
                            if (h.ctlKey) {
                                if (this.lastAction.type == "pinchStart" || this.lastAction.type == "pinch") {
                                    d.type = "pinch"
                                } else {
                                    d.type = "pinchStart"
                                }
                                d.ctl = true;
                                d.x = this.sbViewer.config.screenSize.width / 2;
                                d.y = this.sbViewer.config.screenSize.height / 2;
                                d.dx = this.sbViewer.config.screenSize.width / 2;
                                d.dy = this.sbViewer.config.screenSize.height / 2;
                                d.sc = Math.sqrt(Math.pow(h.x - d.dx, 2) + Math.pow(h.y - d.dy, 2)) / Math.sqrt(Math.pow(h.dx - d.dx, 2) + Math.pow(h.dy - d.dy, 2))
                            } else {
                                if (this.lastAction.type == "pinch" && this.lastAction.ctl) {
                                    d.type = "pinchEnd";
                                    d.sc = this.lastAction.sc
                                } else {
                                    if (this.lastAction.type == "dragStart" || this.lastAction.type == "drag") {
                                        d.type = "drag";
                                        var c = h.time - this.lastTime;
                                        if (h.time - this.lastPinchEndTime > 100) {
                                            d.spx = parseInt((h.x - this.lastAction.x) * 1000 / c);
                                            d.spy = parseInt((h.y - this.lastAction.y) * 1000 / c);
                                            var b = Math.sqrt(Math.pow(d.spx, 2) + Math.pow(d.spy, 2));
                                            if (b > this.sbViewer.config.screenSize.width * 8) {
                                                d.spx *= this.sbViewer.config.screenSize.width * 8 / b;
                                                d.spy *= this.sbViewer.config.screenSize.width * 8 / b
                                            } else {
                                                if (b < this.sbViewer.config.barHeight * 3) {
                                                    d.spx = d.spy = 0
                                                }
                                            }
                                        }
                                    } else {
                                        if (Math.pow(h.x - h.dx, 2) + Math.pow(h.y - h.dy, 2) >= Math.pow(this.dragStartSensitive, 2)) {
                                            d.type = "dragStart";
                                            h.dx = h.x;
                                            h.dy = h.y;
                                            this.touches["id" + h.id] = h
                                        }
                                    }
                                    d.ctl = false;
                                    d.x = h.x;
                                    d.y = h.y;
                                    d.dx = h.dx;
                                    d.dy = h.dy
                                }
                            }
                        }
                    } else {
                        if (this.lastAction.type == "pinchStart" || this.lastAction.type == "pinch") {
                            d.type = "pinch"
                        } else {
                            d.type = "pinchStart";
                            h.dx = h.x;
                            h.dy = h.y;
                            this.touches["id" + h.id] = h
                        }
                        var j = this.touches[Object.keys(this.touches)[0]];
                        var i = this.touches[Object.keys(this.touches)[1]];
                        d.x = parseInt((j.x + i.x) / 2);
                        d.y = parseInt((j.y + i.y) / 2);
                        d.dx = parseInt((j.dx + i.dx) / 2);
                        d.dy = parseInt((j.dy + i.dy) / 2);
                        d.sc = Math.sqrt(Math.pow(j.x - i.x, 2) + Math.pow(j.y - i.y, 2)) / Math.sqrt(Math.pow(j.dx - i.dx, 2) + Math.pow(j.dy - i.dy, 2))
                    }
                }
            }
        }
    }
    if (d.type != "none") {
        if (this.callbackObserver.interactiveCallback.call(this.callbackObserver, d) && d.type == "firstClick") {
            var f = this;
            this.click = setTimeout(function() {
                f.click = null;
                d.type = "click";
                f.callbackObserver.interactiveCallback.call(f.callbackObserver, d)
            }, 300)
        }
        this.lastAgoAction = this.lastAction;
        this.lastAction = d;
        this.lastTime = h.time;
        if (d.type == "pinchEnd") {
            this.lastPinchEndTime = h.time
        }
    }
    if (SBDebug.ENABLED) {
        g_SBDebug.mouseEvent.type = a.type + ":" + h.pointer + h.action;
        g_SBDebug.mouseEvent.x = h.x;
        g_SBDebug.mouseEvent.y = h.y
    }
};

SBInteraction.prototype.onWheelAction = function(a) {
    if (this.overlapObject) {
        if (this.interactionManager.processInteractive(new PIXI.Point(a.x, a.y), this.overlapObject, function() {}, true)) {
            return
        }
    }
    var c = a.deltaY ? -(a.deltaY) : a.wheelDelta ? a.wheelDelta : -(a.detail);
    var b = {
        type: "wheel",
        x: a.x,
        y: a.y,
        dx: 0,
        dy: 0,
        sc: 0,
        spx: 0,
        spy: 0,
        ctl: false,
        pointer: "mouse"
    };
    b.ctl = ((a.ctrlKey && !a.metaKey) || (!a.ctrlKey && a.metaKey));
    if (c < 0) {
        a.preventDefault();
        b.sc = 1
    } else {
        if (c > 0) {
            a.preventDefault();
            b.sc = -1
        }
    }
    if (b.sc != 0) {
        this.callbackObserver.interactiveCallback.call(this.callbackObserver, b)
    }
    this.sbViewer.userInteractiveAction()
};
SBInteraction.prototype.onKeyDownAction = function(a) {
    if (this.sbViewer.enabled) {
        var b = {
            type: "keydown",
            x: 0,
            y: 0,
            dx: 0,
            dy: 0,
            sc: 0,
            spx: 0,
            spy: 0,
            ctl: false,
            pointer: ""
        };
        b.ctl = ((a.ctrlKey && !a.metaKey) || (!a.ctrlKey && a.metaKey));
        if (a.keyCode == 27) {
            b.pointer = "esc"
        }
        if (!this.sbViewer.sbFigure.textEditMode) {
            if (a.keyCode == 37) {
                b.pointer = "left"
            }
            if (a.keyCode == 39) {
                b.pointer = "right"
            }
            if (a.keyCode == 38) {
                b.pointer = "up"
            }
            if (a.keyCode == 40) {
                b.pointer = "down"
            }
            if (a.keyCode == 32) {
                b.pointer = "space"
            }
            if (a.keyCode == 13) {
                b.pointer = "enter"
            }
            if (a.keyCode == 107 || a.keyCode == 187) {
                b.pointer = "plus"
            }
            if (a.keyCode == 109 || a.keyCode == 189) {
                b.pointer = "minus"
            }
        }
        if (b.pointer != "") {
            a.preventDefault();
            this.callbackObserver.interactiveCallback.call(this.callbackObserver, b)
        }
        this.sbViewer.userInteractiveAction()
    }
};
var SBAnime = function(a) {
    this.sprite = a;
    this.playing = true;
    this.startTime = -1;
    this.positionTo = null;
    this.alphaTo = -1;
    this.functionTo = null;
    this.duration = 1000;
    this.onEnd = null;
    this.acceleration = SBAnime.Linear;
    this.isSkip = false
};
SBAnime.prototype.draw = function() {
    var a = new Date().getTime();
    if (this.startTime == -1) {
        this.startTime = new Date().getTime();
        this.startPos = new PIXI.Point(this.sprite.position.x, this.sprite.position.y);
        this.startAlpha = this.sprite.alpha;
        this.playing = true
    }
    var b;
    if (this.duration <= 0) {
        b = 1
    } else {
        b = (a - this.startTime) / this.duration
    }
    b = this.acceleration(b);
    if (b > 1) {
        b = 1
    }
    if (this.isSkip) {
        b = 1
    }
    if (this.positionTo != null) {
        this.sprite.position.x = this.startPos.x + (this.positionTo.x - this.startPos.x) * b;
        this.sprite.position.y = this.startPos.y + (this.positionTo.y - this.startPos.y) * b
    }
    if (this.alphaTo >= 0) {
        this.sprite.alpha = this.startAlpha + (this.alphaTo - this.startAlpha) * b
    }
    if (this.functionTo != null) {
        this.functionTo(b)
    }
    if (b == 1) {
        this.playing = false;
        if (this.onEnd != null) {
            this.onEnd(this.sprite)
        }
    }
};
SBAnime.prototype.skip = function() {
    this.isSkip = true;
    this.draw()
};
SBAnime.Linear = function(a) {
    return a
};
SBAnime.EaseInQuad = function(a) {
    return a * a
};
SBAnime.EaseOutQuad = function(a) {
    return a * (2 - a)
};
SBAnime.EaseInOutQuad = function(a) {
    return a < 0.5 ? 2 * a * a : -1 + (4 - 2 * a) * a
};
SBAnime.EaseInCubic = function(a) {
    return a * a * a
};
SBAnime.EaseOutCubic = function(a) {
    return (--a) * a * a + 1
};
SBAnime.EaseInOutCubic = function(a) {
    return a < 0.5 ? 4 * a * a * a : (a - 1) * (2 * a - 2) * (2 * a - 2) + 1
};
SBAnime.EaseInQuart = function(a) {
    return a * a * a * a
};
SBAnime.EaseOutQuart = function(a) {
    return 1 - (--a) * a * a * a
};
SBAnime.EaseInOutQuart = function(a) {
    return a < 0.5 ? 8 * a * a * a * a : 1 - 8 * (--a) * a * a * a
};
SBAnime.EaseInQuint = function(a) {
    return a * a * a * a * a
};
SBAnime.EaseOutQuint = function(a) {
    return 1 + (--a) * a * a * a * a
};
SBAnime.EaseInOutQuint = function(a) {
    return a < 0.5 ? 16 * a * a * a * a * a : 1 + 16 * (--a) * a * a * a * a
};
var SBPageManager = function(a) {
    this.sbViewer = a;
    this.pageSpritePool = new Array();
    this.curlShadows = new Object();
    this.referencePage = 0;
    this.loader = new PIXI.loaders.Loader(a.docInfo.DATAURL + a.docInfo.FILCD + "/", 2);
    this.curlShadow = this.createCurlShadow();
    this.pageLoadingTexture = null;
    this.pageBackTexture = null
};
SBPageManager.prototype.getPageSprite = function(f) {
    var c = 0;
    var b = 0;
    for (var e = 0; e < this.pageSpritePool.length; e++) {
        if (this.pageSpritePool[e].page == f) {
            return this.pageSpritePool[e]
        }
        var a = Math.abs(this.pageSpritePool[e].page - this.referencePage);
        if (a > b) {
            c = e;
            b = a
        }
    }
    var d = this.pageSpritePool[c];
    d.hasImage = false;
    if ("page" + d.page in this.loader.resources) {
        if (this.loader.resources["page" + d.page].texture !== undefined) {
            this.loader.resources["page" + d.page].texture.destroy(true)
        }
        delete this.loader.resources["page" + d.page]
    }
    this.setPageChildrenFrame(d);
    this.loadSpriteTexture(d, f);
    return d
};
SBPageManager.prototype.loadSpriteTexture = function(a, b) {
    a.page = b;
    a.layLoading.visible = true;
    if (b > 0 && b <= this.sbViewer.docInfo.FILPAGE) {
        this.loader.add("page" + b, "image/" + ("00000" + b).slice(-6) + "." + this.sbViewer.docInfo.IMGEXT + "?ver=" + this.sbViewer.docInfo.FILFVER).before(function(c) {
            if (c.name in this.loader.resources) {
                c.load(function(g) {
                    if (g.name == "page" + g.sprite.page) {
                        var f = new PIXI.BaseTexture(g.data, null, PIXI.utils.getResolutionOfUrl(g.url));
                        f.imageUrl = g.url;
                        g.texture = new PIXI.Texture(f);
                        PIXI.utils.BaseTextureCache[g.url] = f;
                        PIXI.utils.TextureCache[g.url] = g.texture;
                        var d = g.sprite;
                        d.layPage.texture = g.texture;
                        d.hasImage = true;
                        this.setPageChildrenFrame(d);
                        var e = new SBAnime(d.layLoading);
                        e.alphaTo = 0;
                        e.duration = 300;
                        e.acceleration = SBAnime.EaseInCubic;
                        e.onEnd = function() {
                            d.layLoading.visible = false
                        };
                        this.sbViewer.addAnimation(e, "page" + g.sprite.page, 1)
                    }
                    delete g.sprite;
                    g._dequeue()
                }.bind(this))
            } else {
                delete c.sprite;
                c._dequeue()
            }
        }.bind(this)).load();
        a.layLoading.alpha = 1;
        this.loader.resources["page" + b].sprite = a
    } else {
        a.hasImage = true;
        a.layPage.texture = SBFunction.createEmptyTexture(this.pageBackTexture.width, this.pageBackTexture.height, 0, 1);
        a.layLoading.alpha = 0
    }
    return a
};
SBPageManager.prototype.setPageChildrenFrame = function(e) {
    var b = e.layPage;
    e.texture = this.pageBackTexture;
    e.layLoading.texture = this.pageLoadingTexture;
    e.width = e.texture.width;
    e.height = e.texture.height;
    var a = e.width;
    var d = e.height;
    b.x = b.y = 0;
    if (e.hasImage) {
        var c = b.texture.width;
        var f = b.texture.height;
        if (a / d > c / f) {
            b.height = d;
            b.width = parseInt(d * c / f);
            b.x = parseInt((a - b.width) / 2);
            b.y = 0
        } else {
            b.width = a;
            b.height = parseInt(a * f / c);
            b.x = 0;
            b.y = parseInt((d - b.height) / 2)
        }
        this.sbViewer.sbLinkAnnot.draw(e.page, e.layLink, b.x, b.y, b.width, b.height);
        this.sbViewer.sbFigure.draw(e.page, e.layFigure, b.x, b.y, b.width, b.height)
    } else {
        e.layLink.clear();
        b.texture = this.pageBackTexture
    }
};
SBPageManager.prototype.setSize = function(d, a) {
    if (this.pageLoadingTexture != null) {
        this.pageLoadingTexture.destroy(true)
    }
    if (this.pageBackTexture != null) {
        this.pageBackTexture.destroy(true)
    }
    this.pageLoadingTexture = SBFunction.createEmptyTexture(d, a, 4210752, 1);
    this.pageBackTexture = SBFunction.createEmptyTexture(d, a, 16777215, 1);
    for (var c = 0; c < this.pageSpritePool.length; c++) {
        this.setPageChildrenFrame(this.pageSpritePool[c])
    }
    this.curlShadow.scale = new PIXI.Point(1, 1);
    this.curlShadow.pivot = new PIXI.Point(0, 0);
    this.curlShadow.texture.destroy(true);
    this.curlShadow.texture = SBFunction.createEmptyTexture(d, a, 0, 0);
    this.curlShadow.textureLT.position = new PIXI.Point(0, 0);
    this.curlShadow.textureLM.position = new PIXI.Point(0, 0);
    this.curlShadow.textureLB.position = new PIXI.Point(0, this.curlShadow.height);
    this.curlShadow.textureMT.position = new PIXI.Point(0, 0);
    this.curlShadow.textureMB.position = new PIXI.Point(0, this.curlShadow.height);
    this.curlShadow.textureRT.position = new PIXI.Point(this.curlShadow.width, 0);
    this.curlShadow.textureRM.position = new PIXI.Point(this.curlShadow.width, 0);
    this.curlShadow.textureRB.position = new PIXI.Point(this.curlShadow.width, this.curlShadow.height);
    this.curlShadow.textureLM.height = this.curlShadow.height;
    this.curlShadow.textureMT.width = this.curlShadow.width;
    this.curlShadow.textureMB.width = this.curlShadow.width;
    this.curlShadow.textureRM.height = this.curlShadow.height;
    var b = parseInt((d < a ? d : a) / 12);
    this.curlShadow.shadowWidth = b;
    var e = new PIXI.Point(b / 64, b / 64);
    this.curlShadow.textureLT.scale = e;
    this.curlShadow.textureLM.scale.x = e.x;
    this.curlShadow.textureLB.scale = e;
    this.curlShadow.textureMT.scale.y = e.y;
    this.curlShadow.textureMB.scale.y = e.y;
    this.curlShadow.textureRT.scale = e;
    this.curlShadow.textureRM.scale.x = e.x;
    this.curlShadow.textureRB.scale = e;
    this.curlShadow.textureMS.width = b * 4;
    this.curlShadow.textureFS.width = b / 1;
    this.curlShadow.outer.alpha = 0.3;
    this.curlShadow.textureMS.alpha = 0.8
};
SBPageManager.prototype.createCurlShadow = function() {
    var b = this.sbViewer.resLoader.resources.shadow.texture;
    var a = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    a.textureLT = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(0, 0, 64, 64)));
    a.textureLM = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(0, 64, 64, 64)));
    a.textureLB = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(0, 128, 64, 64)));
    a.textureMT = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(64, 0, 64, 64)));
    a.textureMB = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(64, 128, 64, 64)));
    a.textureRT = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(128, 0, 64, 64)));
    a.textureRM = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(128, 64, 64, 64)));
    a.textureRB = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(128, 128, 64, 64)));
    a.textureLT.pivot = new PIXI.Point(64, 64);
    a.textureLM.pivot = new PIXI.Point(64);
    a.textureLB.pivot = new PIXI.Point(64, 0);
    a.textureMT.pivot = new PIXI.Point(0, 64);
    a.textureMB.pivot = new PIXI.Point(0, 0);
    a.textureRT.pivot = new PIXI.Point(0, 64);
    a.textureRM.pivot = new PIXI.Point(0, 0);
    a.textureRB.pivot = new PIXI.Point(0, 0);
    a.outer = new PIXI.Sprite(PIXI.Texture.EMPTY);
    a.addChild(a.outer);
    a.outer.addChild(a.textureLT);
    a.outer.addChild(a.textureLM);
    a.outer.addChild(a.textureLB);
    a.outer.addChild(a.textureMT);
    a.outer.addChild(a.textureMB);
    a.outer.addChild(a.textureRT);
    a.outer.addChild(a.textureRM);
    a.outer.addChild(a.textureRB);
    a.textureMS = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(65, 65, 63, 63)));
    a.textureFS = new PIXI.Sprite(new PIXI.Texture(b, new PIXI.Rectangle(128, 64, 64, 64)));
    a.textureMS.pivot = new PIXI.Point(63, 32);
    a.textureFS.pivot = new PIXI.Point(0, 32);
    a.addChild(a.textureMS);
    a.addChild(a.textureFS);
    a.outerMask = new PIXI.Graphics();
    a.addChild(a.outerMask);
    a.outer.mask = a.outerMask;
    a.msMask = new PIXI.Graphics();
    a.addChild(a.msMask);
    a.textureMS.mask = a.msMask;
    a.visible = false;
    return a
};
SBPageManager.createPageSprite = function(b) {
    var a = new PIXI.Sprite(PIXI.Texture.EMPTY);
    a.layPage = new PIXI.Sprite(PIXI.Texture.EMPTY);
    a.layFigure = new PIXI.Container();
    a.layLink = new PIXI.Graphics();
    a.layLoading = new PIXI.Sprite(PIXI.Texture.EMPTY);
    a.selfMask = new PIXI.Graphics();
    a.addChild(a.layPage);
    a.addChild(a.layFigure);
    a.addChild(a.layLink);
    a.addChild(a.layLoading);
    a.addChild(a.selfMask);
    a.mask = a.selfMask;
    a.page = -9999;
    a.visible = false;
    a.hasImage = false;
    a.layLink.interactive = true;
    a.layLink.buttonMode = true;
    return a
};
var SBThumbView = function(a, b, c, d) {
    this.sbViewer = a;
    this.container = b;
    this.isSlider = d;
    this.selectPage = 0;
    this.currentPage = 0;
    this.width = 0;
    this.height = 0;
    this.gridWidth = 0;
    this.gridHeight = 0;
    this.margin = 0;
    this.padding = 0;
    this.labelHeight = 0;
    this.pageSize = new PIXI.Rectangle();
    this.innerContainer = new PIXI.Container();
    this.container.addChild(this.innerContainer);
    this.mask = new PIXI.Graphics();
    this.container.addChild(this.mask);
    this.innerContainer.mask = this.mask;
    this.container.interactive = true;
    this.container.buttonMode = true;
    this.grid = new PIXI.Graphics();
    this.innerContainer.addChild(this.grid);
    this.container.mousedown = this.container.touchstart = this._onPointerAction.bind(this);
    this.container.mouseup = this.container.touchend = this._onPointerAction.bind(this);
    this.container.mousemove = this.container.touchmove = this._onPointerAction.bind(this);
    this.container.mouseupoutside = this.container.touchendoutside = this._onPointerAction.bind(this);
    this.touchStart = null;
    this.dragStart = new PIXI.Point(0, 0);
    this.lastAction = {
        type: "none"
    };
    this.callbackObserver = c;
    this.pageSpritePool = new Array();
    this.referencePage = 0;
    this.loader = new PIXI.loaders.Loader(a.docInfo.DATAURL + a.docInfo.FILCD + "/", 2);
    this.pageBackTexture = null;
    for (var f = 0; f < 32; f++) {
        var e = SBThumbView.createPageSprite();
        this.innerContainer.addChild(e);
        this.pageSpritePool[f] = e
    }
};
SBThumbView.prototype.destroy = function() {};
SBThumbView.prototype.setValue = function(a) {
    if (this.currentPage != a) {
        if (this.sbViewer.docInfo.DIRECTION == 0) {
            this.innerContainer.x = parseInt((this.width - this.gridWidth - this.margin * 2) / 2 - (a - 1) * this.gridWidth)
        } else {
            this.innerContainer.x = parseInt((this.width - this.gridWidth - this.margin * 2) / 2 - (this.sbViewer.docInfo.FILPAGE - a) * this.gridWidth)
        }
        this.drawPages()
    }
};
SBThumbView.prototype._onPointerAction = function(a) {
    if (!this.sbViewer.enabled) {
        return
    }
    if (a.data.originalEvent.button > 0) {
        return
    }
    var e = {
        type: "none",
        x: a.data.global.x,
        y: a.data.global.y,
        dx: 0,
        dy: 0,
        spx: 0,
        spy: 0,
        time: 0
    };
    e.time = new Date().getTime();
    switch (a.type) {
        case "mousedown":
        case "touchstart":
            if (this.lastAction.type == "down") {
                break
            }
            this.touch = true;
            e.type = "down";
            this.dragStart = this.innerContainer.toLocal(new PIXI.Point(e.x, e.y), this.container);
            this.touchStart = new PIXI.Point(e.x, e.y);
            this.skipClick = false;
            if ("thumbScroll" in this.sbViewer.animationPool) {
                delete this.sbViewer.animationPool.thumbScroll;
                this.skipClick = true
            }
            this.sbViewer.userInteractiveAction();
            break;
        case "mouseup":
        case "touchend":
        case "mouseupoutside":
        case "touchendoutside":
            if (this.touch) {
                e.type = "up";
                this.touch = false;
                var c = Math.sqrt(Math.pow(this.lastAction.spx, 2) + Math.pow(this.lastAction.spy, 2));
                if (c > 200) {
                    var g = this.innerContainer.toLocal(new PIXI.Point(e.x, e.y), this.container);
                    g.x = -g.x + e.x;
                    g.y = -g.y + e.y;
                    var b = new PIXI.Point(this.lastAction.spx, this.lastAction.spy);
                    var f = new SBAnime(this.innerContainer);
                    f.duration = 1500;
                    f.acceleration = SBAnime.EaseOutCubic;
                    f.functionTo = function(j) {
                        this.innerContainer.x = g.x + b.x / 3 * j;
                        this._draw()
                    }.bind(this);
                    this.sbViewer.addAnimation(f, "thumbScroll", 0)
                } else {
                    if (this.lastAction.type != "move" && !this.skipClick) {
                        var i = this.getDevPageFromPoint(new PIXI.Point(e.x, e.y));
                        var h = this.convDevPageFromPage(i);
                        if (i > 0) {
                            this.selectPage = h;
                            this._drawCursor(i);
                            if (this.isSlider) {
                                this.callbackObserver.onThumbSliderClick.call(this.callbackObserver, h)
                            } else {
                                this.callbackObserver.onThumbClick.call(this.callbackObserver, h)
                            }
                        }
                    }
                }
                this.touchStart = null
            }
            break;
        case "mousemove":
        case "touchmove":
            if (this.touch) {
                if (this.lastAction.type == "move" && Math.pow(e.x - this.lastAction.x, 2) + Math.pow(e.y - this.lastAction.y, 2) >= 1) {
                    var d = e.time - this.lastAction.time;
                    e.type = "move";
                    e.spx = parseInt((e.x - this.lastAction.x) * 1000 / d);
                    e.spy = parseInt((e.y - this.lastAction.y) * 1000 / d);
                    this.innerContainer.x = -this.dragStart.x + e.x;
                    this._draw();
                    this.sbViewer.refresh()
                } else {
                    if (this.touchStart && this.lastAction.type == "down" && Math.pow(e.x - this.touchStart.x, 2) + Math.pow(e.y - this.touchStart.y, 2) >= Math.pow(this.sbViewer.config.barHeight / 4, 2)) {
                        e.type = "move"
                    }
                }
            }
            break
    }
    if (e.type != "none") {
        this.lastAction = e
    }
};
SBThumbView.prototype._draw = function() {
    if (this.chkOuterFrame()) {
        if ("thumbScroll" in this.sbViewer.animationPool) {
            delete this.sbViewer.animationPool.thumbScroll
        }
    }
    var a = this.convDevPageFromPage(this.getCenterDevPage());
    if (a != this.currentPage) {
        if (this.isSlider) {
            this.callbackObserver.onThumbSliderChange.call(this.callbackObserver, this.currentPage)
        }
        this.drawPages()
    }
};
SBThumbView.prototype._drawCursor = function(a) {
    this.grid.clear();
    this.grid.beginFill(16777215, 0.3);
    this.grid.drawRect(this.margin + (a - 1) * this.gridWidth, 0, this.gridWidth, this.gridHeight + this.margin * 2);
    this.grid.endFill()
};
SBThumbView.prototype.setSize = function(c, a) {
    this.width = c;
    this.height = a;
    this.margin = parseInt(a * 0.05);
    this.gridHeight = a - this.margin * 2;
    this.gridWidth = parseInt(this.gridHeight * 0.9);
    this.padding = parseInt(this.gridHeight * 0.025);
    this.labelHeight = parseInt(this.gridHeight * 0.1);
    this.pageSize.width = this.gridWidth - this.padding * 2;
    this.pageSize.height = this.gridHeight - this.padding * 2;
    this.buffer = parseInt((c - this.margin * 2) / this.gridWidth / 2 + 1);
    SBFunction.setEmptyGraphics(this.mask, c, a, 16711680, 1);
    if (this.pageBackTexture != null) {
        this.pageBackTexture.destroy(true)
    }
    this.pageBackTexture = SBFunction.createEmptyTexture(this.pageSize.width, this.pageSize.height, 4210752, 0);
    for (var b = 0; b < this.pageSpritePool.length; b++) {
        this.setPageChildrenFrame(this.pageSpritePool[b])
    }
    this.drawPages();
    this.sbViewer.refresh()
};
SBThumbView.prototype.drawPages = function() {
    var d = this.convDevPageFromPage(this.getCenterDevPage());
    if (this.currentPage == d) {
        return
    }
    this.currentPage = d;
    this.referencePage = d;
    for (var c = 0; c < this.pageSpritePool.length; c++) {
        this.pageSpritePool[c].visible = false
    }
    var b = this.currentPage - this.buffer - 1;
    var a = this.currentPage + this.buffer + 1;
    if (this.selectPage == 0) {
        this.selectPage = d
    }
    this.grid.clear();
    for (var c = b; c <= a; c++) {
        this.drawPage(c);
        if (c == this.selectPage) {
            this._drawCursor(this.convDevPageFromPage(c))
        }
    }
    this.sbViewer.refresh()
};
SBThumbView.prototype.drawPage = function(b) {
    if (b > 0 && b <= this.sbViewer.docInfo.FILPAGE) {
        var a = this.getPageSprite(b);
        if (this.sbViewer.docInfo.DIRECTION == 0) {
            a.x = this.margin + this.padding + (b - 1) * this.gridWidth
        } else {
            a.x = this.margin + this.padding + (this.sbViewer.docInfo.FILPAGE - b) * this.gridWidth
        }
        a.y = this.margin + this.padding;
        a.width = this.pageSize.width;
        a.height = this.pageSize.height;
        a.visible = true
    }
};
SBThumbView.prototype.chkOuterFrame = function() {
    var c = false;
    var a = (this.width - this.gridWidth) / 2 - this.margin;
    var b = (this.width + this.gridWidth) / 2 - this.margin - this.gridWidth * this.sbViewer.docInfo.FILPAGE;
    if (this.innerContainer.x > a) {
        this.innerContainer.x = a;
        c = true
    }
    if (this.innerContainer.x < b) {
        this.innerContainer.x = b;
        c = true
    }
    return c
};
SBThumbView.prototype.convDevPageFromPage = function(a) {
    return this.sbViewer.docInfo.DIRECTION == 0 ? a : this.sbViewer.docInfo.FILPAGE - a + 1
};
SBThumbView.prototype.getDevPageFromPoint = function(a) {
    var b = Math.ceil((a.x - this.margin / 2 - this.innerContainer.x) / this.gridWidth);
    if (b <= 0 || b > this.sbViewer.docInfo.FILPAGE) {
        b = 0
    }
    return b
};
SBThumbView.prototype.getCenterDevPage = function() {
    var a = Math.ceil((this.width / 2 - this.margin - this.innerContainer.x) / this.gridWidth);
    return a
};
SBThumbView.prototype.getPageSprite = function(f) {
    var c = 0;
    var b = 0;
    for (var e = 0; e < this.pageSpritePool.length; e++) {
        if (this.pageSpritePool[e].page == f) {
            return this.pageSpritePool[e]
        }
        var a = Math.abs(this.pageSpritePool[e].page - this.referencePage);
        if (a > b) {
            c = e;
            b = a
        }
    }
    var d = this.pageSpritePool[c];
    d.hasImage = false;
    if ("thumb" + d.page in this.loader.resources) {
        if (this.loader.resources["thumb" + d.page].texture !== undefined) {
            this.loader.resources["thumb" + d.page].texture.destroy(true)
        }
        delete this.loader.resources["thumb" + d.page]
    }
    d.page = f;
    this.setPageChildrenFrame(d);
    this.loadSpriteTexture(d, f);
    return d
};
SBThumbView.prototype.loadSpriteTexture = function(a, b) {
    if (b > 0 && b <= this.sbViewer.docInfo.FILPAGE) {
        this.loader.add("thumb" + b, "thumb/" + ("00000" + b).slice(-6) + ".jpg?ver=" + this.sbViewer.docInfo.FILFVER).before(function(c) {
            if (c.name in this.loader.resources) {
                c.load(function(g) {
                    if (g.name == "thumb" + g.sprite.page) {
                        var f = new PIXI.BaseTexture(g.data, null, PIXI.utils.getResolutionOfUrl(g.url));
                        f.imageUrl = g.url;
                        g.texture = new PIXI.Texture(f);
                        PIXI.utils.BaseTextureCache[g.url] = f;
                        PIXI.utils.TextureCache[g.url] = g.texture;
                        var d = g.sprite;
                        d.layPage.texture = g.texture;
                        d.hasImage = true;
                        d.layPage.visible = true;
                        d.layPage.alpha = 0;
                        this.setPageChildrenFrame(d);
                        var e = new SBAnime(d.layPage);
                        e.alphaTo = 1;
                        e.duration = 300;
                        this.sbViewer.addAnimation(e, "thumb" + g.sprite.page, 1)
                    }
                    delete g.sprite;
                    g._dequeue()
                }.bind(this))
            } else {
                delete c.sprite;
                c._dequeue()
            }
        }.bind(this)).load();
        a.layPage.visible = false;
        this.loader.resources["thumb" + b].sprite = a
    }
    return a
};
SBThumbView.prototype.setPageChildrenFrame = function(e) {
    var b = e.layPage;
    e.texture = this.pageBackTexture;
    e.width = e.texture.width;
    e.height = e.texture.height;
    var a = e.width;
    var d = e.height - this.labelHeight;
    e.label.text = e.page;
    e.label.style = SBFunction.getFont(this.labelHeight * SBViewer.RETINASCALE, 16777215);
    e.label.scale.x = e.label.scale.y = 1 / SBViewer.RETINASCALE;
    e.label.x = parseInt((a - e.label.width) / 2);
    e.label.y = e.height - this.labelHeight;
    b.x = b.y = 0;
    if (e.hasImage) {
        var c = b.texture.width;
        var f = b.texture.height;
        if (a / d > c / f) {
            b.height = d;
            b.width = parseInt(d * c / f);
            b.x = parseInt((a - b.width) / 2);
            b.y = 0
        } else {
            b.width = a;
            b.height = parseInt(a * f / c);
            b.x = 0;
            b.y = parseInt((d - b.height) / 2)
        }
    }
};
SBThumbView.createPageSprite = function() {
    var a = new PIXI.Sprite(PIXI.Texture.EMPTY);
    a.layPage = new PIXI.Sprite(PIXI.Texture.EMPTY);
    a.label = new PIXI.Text("", {});
    a.addChild(a.layPage);
    a.addChild(a.label);
    a.page = -9999;
    a.visible = false;
    a.hasImage = false;
    return a
};
var SBControl = function(a) {
    this.sbViewer = a;
    this.control = new PIXI.Container();
    this.titleBar = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    this.toolBar = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    this.infoBox = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    this.slider = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    this.thumbSlider = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    this.thumbView = null;
    this.control.addChild(this.infoBox);
    this.control.addChild(this.thumbSlider);
    this.control.addChild(this.slider);
    this.control.addChild(this.titleBar);
    this.control.addChild(this.toolBar);
    this.control.interactive = true;
    this.visible = true;
    this.titleBar.title = new PIXI.Text("", {});
    this.titleBar.addChild(this.titleBar.title);
    this.infoBox.pagelabel = new PIXI.Text("", {});
    this.infoBox.addChild(this.infoBox.pagelabel);
    this.slider.slider = new SBSlider(a, this.slider, this);
    this.thumbSlider.sbThumb = new SBThumbView(a, this.thumbSlider, this, true);
    this.thumbSlider.visible = false;
    this.titleBar.btExit = new SBButton(a, "", null, 1, 16777215, 0, 0);
    this.titleBar.addChild(this.titleBar.btExit.button);
    this.titleBar.btMeeting = new SBButton(a, "", null, 1, 16777215, 3174655, 1);
    this.titleBar.addChild(this.titleBar.btMeeting.button);
    this.titleBar.btDownload = new SBButton(a, null, a.resLoader.resources.download.texture, 0, 0, 0, 0);
    this.titleBar.addChild(this.titleBar.btDownload.button);
    this.toolBar.btFullScr = new SBButton(a, null, a.resLoader.resources.fullscreen1.texture, 0, 0, 0, 0);
    this.toolBar.addChild(this.toolBar.btFullScr.button);
    this.toolBar.btCurl = new SBButton(a, "", null, 1, 16777215, 0, 0);
    this.toolBar.addChild(this.toolBar.btCurl.button);
    this.toolBar.btCover = new SBButton(a, "", null, 1, 16777215, 0, 0);
    this.toolBar.addChild(this.toolBar.btCover.button);
    this.toolBar.btMargin = new SBButton(a, "", null, 1, 16777215, 0, 0);
    this.toolBar.addChild(this.toolBar.btMargin.button);
    this.toolBar.btDirection = new SBButton(a, "", null, 1, 16777215, 0, 0);
    this.toolBar.addChild(this.toolBar.btDirection.button);
    this.toolBar.btResolution = new SBButton(a, "", null, 1, 16777215, 0, 0);
    this.toolBar.addChild(this.toolBar.btResolution.button);
    this.titleBar.btDownload.button.visible = this.sbViewer.docInfo.DOWNLOADABLE;
    if (this.sbViewer.docInfo.FILMODE < 400) {
        this.titleBar.btDownload.setEnabled(false)
    }
    this.infoBox.btSearch = new SBButton(a, null, a.resLoader.resources.search.texture, 0, 0, 0, 0);
    this.infoBox.addChild(this.infoBox.btSearch.button);
    this.infoBox.btFigure = new SBButton(a, null, a.resLoader.resources.figure_edit.texture, 0, 0, 0, 0);
    this.infoBox.addChild(this.infoBox.btFigure.button);
    this.infoBox.btBookmark = new SBButton(a, null, a.resLoader.resources.bookmark.texture, 0, 0, 0, 0);
    this.infoBox.addChild(this.infoBox.btBookmark.button);
    this.infoBox.btIndex = new SBButton(a, null, a.resLoader.resources.pageindex.texture, 0, 0, 0, 0);
    this.infoBox.addChild(this.infoBox.btIndex.button);
    this.titleBar.btMeeting.button.visible = false;
    this.wheelEvent = this.onWheelAction.bind(this);
    this.slider.mouseover = this.thumbSlider.mouseover = this.onMouseSlider.bind(this);
    this.slider.mouseout = this.thumbSlider.mouseout = this.onMouseSlider.bind(this)
};
SBControl.prototype.setSize = function(b, h) {
    this.width = b;
    this.height = h;
    var g = this.sbViewer.config.barHeight;
    this.titleBar.texture.destroy(true);
    this.titleBar.texture = SBControl.createTitleBarTexture(b, g);
    this.titleBar.originPoint = new PIXI.Point(0, 0);
    this.titleBar.position = this.titleBar.originPoint;
    this.toolBar.texture.destroy(true);
    this.toolBar.texture = SBControl.createToolBarTexture(b, g);
    this.toolBar.originPoint = new PIXI.Point(0, h - g);
    this.toolBar.position = this.toolBar.originPoint;
    var j = g * 7;
    this.infoBox.texture.destroy(true);
    this.infoBox.texture = SBControl.createInfoBoxTexture(j, g);
    this.infoBox.originPoint = new PIXI.Point(parseInt((b - j) / 2), parseInt(g * 1.5));
    this.infoBox.position = this.infoBox.originPoint;
    var d = b - g * 2;
    this.slider.texture.destroy(true);
    this.slider.texture = SBControl.createInfoBoxTexture(d, g);
    this.slider.originPoint = new PIXI.Point(parseInt((b - d) / 2), parseInt(h - g * 2.5));
    this.slider.position = this.slider.originPoint;
    this.slider.slider.resize();
    this.slider.slider.direction = this.sbViewer.docInfo.DIRECTION;
    this.slider.slider.max = this.sbViewer.config.totalDevPages;
    this.slider.slider.setValue(this.sbViewer.getCenterDevPage());
    this.thumbSlider.texture.destroy(true);
    this.thumbSlider.texture = SBControl.createInfoBoxTexture(b, g * 3);
    this.thumbSlider.originPoint = new PIXI.Point(0, parseInt(h - g * 2.5) - parseInt(g * 3) - parseInt(g * 0.25));
    this.thumbSlider.position = this.thumbSlider.originPoint;
    this.thumbSlider.sbThumb.setSize(b, parseInt(g * 3));
    this.thumbSlider.sbThumb.setValue(this.sbViewer.config.currentPage);
    this.titleBar.btExit.setSize(0, parseInt(g * 0.75));
    this.titleBar.btExit.button.x = parseInt(g * 0.2);
    this.titleBar.btExit.button.y = parseInt((g - parseInt(g * 0.75)) / 2);
    this.titleBar.btExit.button.visible = window.opener ? true : false;
    this.titleBar.btDownload.setSize(parseInt(g * 0.75), parseInt(g * 0.75));
    this.titleBar.btDownload.button.x = this.titleBar.btDownload.button.visible ? b - parseInt(g * 0.2) - parseInt(g * 0.75) : b;
    this.titleBar.btDownload.button.y = parseInt((g - parseInt(g * 0.75)) / 2);
    var e = this.titleBar.btMeeting.setSize(0, parseInt(g * 0.75));
    this.titleBar.btMeeting.button.x = this.titleBar.btDownload.button.x - parseInt(g * 0.2) - e;
    this.titleBar.btMeeting.button.y = parseInt((g - parseInt(g * 0.75)) / 2);
    this.titleBar.title.text = this.sbViewer.docInfo.TITLE;
    this.titleBar.title.style = SBFunction.getFont(parseInt(g * 0.5) * SBViewer.RETINASCALE, 16777215);
    this.titleBar.title.scale.x = this.titleBar.title.scale.y = 1 / SBViewer.RETINASCALE;
    var c = parseInt(b - this.titleBar.btExit.button.width - this.titleBar.btMeeting.button.width - g * 0.8);
    if (this.titleBar.title.width > c) {
        this.titleBar.title.width = c
    }
    this.titleBar.title.position = new PIXI.Point(parseInt((b - this.titleBar.title.width) / 2), parseInt((g - this.titleBar.title.height) / 2));
    var i = parseInt(g * 0.2);
    this.toolBar.btFullScr.setSize(parseInt(g * 0.75), parseInt(g * 0.75));
    this.toolBar.btFullScr.button.x = i;
    this.toolBar.btFullScr.button.y = parseInt((g - parseInt(g * 0.75)) / 2);
    var f = SBFunction.getFullScreenStatus();
    this.toolBar.btFullScr.button.visible = f.enabled;
    if (f.value) {
        this.toolBar.btFullScr.changeTexture(this.sbViewer.resLoader.resources.fullscreen1.texture, false)
    } else {
        this.toolBar.btFullScr.changeTexture(this.sbViewer.resLoader.resources.fullscreen2.texture, false)
    }
    if (window.devicePixelRatio && window.devicePixelRatio >= 2) {
        this.toolBar.btResolution.button.visible = false
    }
    var a = this.toolBar.btResolution.setSize(0, parseInt(g * 0.75));
    if (f.enabled) {
        i += (parseInt(g * 0.75) + parseInt(g * 0.15))
    }
    this.toolBar.btResolution.button.x = i;
    this.toolBar.btResolution.button.y = parseInt((g - parseInt(g * 0.75)) / 2);
    i = b;
    i -= (this.toolBar.btCurl.setSize(0, parseInt(g * 0.75)) + parseInt(g * 0.15));
    this.toolBar.btCurl.button.x = i;
    this.toolBar.btCurl.button.y = parseInt((g - parseInt(g * 0.75)) / 2);
    i -= (this.toolBar.btCover.setSize(0, parseInt(g * 0.75)) + parseInt(g * 0.15));
    this.toolBar.btCover.button.x = i;
    this.toolBar.btCover.button.y = parseInt((g - parseInt(g * 0.75)) / 2);
    this.toolBar.btMargin.button.visible = false;
    this.toolBar.btMargin.button.x = i;
    this.toolBar.btMargin.button.y = parseInt((g - parseInt(g * 0.75)) / 2);
    i -= (this.toolBar.btDirection.setSize(0, parseInt(g * 0.75)) + parseInt(g * 0.15));
    this.toolBar.btDirection.button.x = i;
    this.toolBar.btDirection.button.y = parseInt((g - parseInt(g * 0.75)) / 2);
    this.toolBar.btResolution.button.visible = !(this.toolBar.btResolution.button.x + a + parseInt(g * 0.15) > this.toolBar.btDirection.button.x) && SBViewer.PIXELRATIO < 2;
    this.infoBox.pagelabel.style = SBFunction.getFont(parseInt(g * 0.4) * SBViewer.RETINASCALE, 16777215);
    this.infoBox.pagelabel.scale.x = this.infoBox.pagelabel.scale.y = 1 / SBViewer.RETINASCALE;
    this.infoBox.pagelabel.y = parseInt((g - this.infoBox.pagelabel.height) / 2);
    this.infoBox.btSearch.setSize(parseInt(g * 0.6), parseInt(g * 0.6));
    this.infoBox.btSearch.button.x = parseInt(g * 0.2);
    this.infoBox.btSearch.button.y = parseInt((g - parseInt(g * 0.6)) / 2);
    this.infoBox.btFigure.setSize(parseInt(g * 0.6), parseInt(g * 0.6));
    this.infoBox.btFigure.button.x = parseInt(g + g * 0.2);
    this.infoBox.btFigure.button.y = parseInt((g - parseInt(g * 0.6)) / 2);
    this.infoBox.btBookmark.setSize(parseInt(g * 0.6), parseInt(g * 0.6));
    this.infoBox.btBookmark.button.x = parseInt(this.infoBox.width - g * 0.2 - g * 0.6);
    this.infoBox.btBookmark.button.y = parseInt((g - parseInt(g * 0.6)) / 2);
    this.infoBox.btIndex.setSize(parseInt(g * 0.6), parseInt(g * 0.6));
    this.infoBox.btIndex.button.x = parseInt(this.infoBox.width - g * 0.2 - g - g * 0.6);
    this.infoBox.btIndex.button.y = parseInt((g - parseInt(g * 0.6)) / 2)
};
SBControl.prototype.setVisible = function(h) {
    if (this.visible == h) {
        return
    }
    this.control.visible = true;
    var f = 300;
    var e = new SBAnime(this.titleBar);
    var d = new SBAnime(this.toolBar);
    var g = new SBAnime(this.infoBox);
    var b = new SBAnime(this.slider);
    var c = new SBAnime(this.thumbSlider);
    e.duration = f;
    d.duration = f;
    g.duration = f;
    b.duration = f;
    c.duration = f;
    e.acceleration = SBAnime.EaseOutCubic;
    d.acceleration = SBAnime.EaseOutCubic;
    g.acceleration = SBAnime.EaseOutCubic;
    b.acceleration = SBAnime.EaseOutCubic;
    c.acceleration = SBAnime.EaseOutCubic;
    if (h) {
        this.titleBar.position = new PIXI.Point(this.titleBar.originPoint.x, -this.titleBar.height);
        e.positionTo = this.titleBar.originPoint.clone();
        this.toolBar.position = new PIXI.Point(this.toolBar.originPoint.x, this.height);
        d.positionTo = this.toolBar.originPoint.clone();
        this.infoBox.position = new PIXI.Point(this.infoBox.originPoint.x, -this.infoBox.height);
        g.positionTo = this.infoBox.originPoint.clone();
        this.slider.position = new PIXI.Point(this.slider.originPoint.x, this.height);
        b.positionTo = this.slider.originPoint.clone()
    } else {
        this.titleBar.position = this.titleBar.originPoint.clone();
        e.positionTo = new PIXI.Point(this.titleBar.originPoint.x, -this.titleBar.height);
        this.toolBar.position = this.toolBar.originPoint.clone();
        d.positionTo = new PIXI.Point(this.toolBar.originPoint.x, this.height);
        this.infoBox.position = this.infoBox.originPoint.clone();
        g.positionTo = new PIXI.Point(this.infoBox.originPoint.x, -this.infoBox.height);
        this.slider.position = this.slider.originPoint.clone();
        b.positionTo = new PIXI.Point(this.slider.originPoint.x, this.height);
        this.thumbSlider.position = this.thumbSlider.originPoint.clone();
        c.positionTo = new PIXI.Point(this.thumbSlider.originPoint.x, this.height);
        e.onEnd = function() {
            this.thumbSlider.visible = false;
            this.control.visible = false
        }.bind(this)
    }
    var a = this.sbViewer.getDevPageMember(this.sbViewer.getCenterDevPage());
    this.sbViewer.setPageInfo(a);
    this.sbViewer.addAnimation(e, "titleAnime", 0);
    this.sbViewer.addAnimation(d, "toolAnime", 0);
    this.sbViewer.addAnimation(g, "infoAnime", 0);
    this.sbViewer.addAnimation(b, "sliderAnime", 0);
    this.sbViewer.addAnimation(c, "thumbSliderAnime", 0);
    this.visible = h
};
SBControl.prototype.showThumbView = function() {
    this.thumbView = new PIXI.Graphics();
    SBFunction.setEmptyGraphics(this.thumbView, this.width, this.height, 0, 0.7);
    this.control.addChild(this.thumbView);
    this.thumbView.container = new PIXI.Graphics();
    var a = parseInt(this.width * 0.9);
    var b = parseInt(this.height * 0.5);
    SBFunction.setEmptyGraphics(this.thumbView.container, a, b, 3158064, 1);
    this.thumbView.addChild(this.thumbView.container);
    this.thumbView.container.x = parseInt((this.width - a) / 2);
    this.thumbView.container.y = parseInt((this.height - b) / 2);
    this.thumbView.sbThumb = new SBThumbView(this.sbViewer, this.thumbView.container, this, false);
    this.thumbView.sbThumb.setSize(a, b);
    this.thumbView.sbThumb.setValue(this.sbViewer.config.currentPage);
    this.sbViewer.refresh()
};
SBControl.prototype.dismissThumbView = function() {
    this.thumbView.sbThumb.destroy();
    this.thumbView.removeChildren();
    this.control.removeChild(this.thumbView);
    this.thumbView.destroy();
    this.thumbView = null
};
SBControl.prototype.onSliderDown = function() {
    if (this.thumbSlider.visible == false) {
        this.thumbSlider.visible = true;
        this.thumbSlider.position = this.thumbSlider.originPoint;
        this.thumbSlider.alpha = 0;
        var b = new SBAnime(this.thumbSlider);
        b.duration = 300;
        b.alphaTo = 1;
        this.sbViewer.addAnimation(b, "thumbSliderAnime", 0);
        this.thumbSlider.sbThumb.selectPage = this.sbViewer.config.currentPage;
        var a = this.sbViewer.getDevPageMember(this.thumbSlider.sbThumb.selectPage);
        this.thumbSlider.sbThumb.setValue(a.firstPage)
    }
};
SBControl.prototype.onSliderChange = function(b) {
    var a = this.sbViewer.getDevPageMember(b);
    this.thumbSlider.sbThumb.setValue(a.firstPage)
};
SBControl.prototype.onSliderEnd = function(b) {
    var a = this.sbViewer.getDevPageMember(b);
    this.thumbSlider.sbThumb.setValue(a.firstPage)
};
SBControl.prototype.onThumbSliderChange = function(b) {
    var a = this.sbViewer.getDevPageMember(this.sbViewer.convDevPageFromPage(b));
    this.slider.slider.setValue(a.devPage)
};
SBControl.prototype.onThumbSliderClick = function(a) {
    this.sbViewer.movePage(a)
};
SBControl.prototype.onMouseSlider = function(b) {
    var a = this.sbViewer.renderer.view;
    if (b.type == "mouseover") {
        a.addEventListener("wheel", this.wheelEvent, false)
    } else {
        if (b.type == "mouseout") {
            a.removeEventListener("wheel", this.wheelEvent, false)
        }
    }
};
SBControl.prototype.onWheelAction = function(b) {
    this.onSliderDown();
    var d = b.deltaY ? -(b.deltaY) : b.wheelDelta ? b.wheelDelta : -(b.detail);
    b.preventDefault();
    var c = this.slider.slider.value;
    var a = c;
    if (this.sbViewer.docInfo.DIRECTION == 1) {
        d *= -1
    }
    if (d < 0) {
        a++
    } else {
        if (d > 0) {
            a--
        }
    }
    if (a > this.sbViewer.config.totalDevPages) {
        a = this.sbViewer.config.totalDevPages
    }
    if (a < 1) {
        a = 1
    }
    if (c != a) {
        this.slider.slider.setValue(a);
        this.onSliderChange(this.slider.slider.value)
    }
};
SBControl.prototype.onThumbClick = function(a) {
    this.sbViewer.movePage(a);
    this.dismissThumbView()
};
SBControl.createTitleBarTexture = function(c, a) {
    var b = new PIXI.Graphics();
    SBFunction.setEmptyGraphics(b, c, a, 3158064, 1);
    var d = b.generateTexture();
    b.destroy();
    return d
};
SBControl.createToolBarTexture = function(c, a) {
    var b = new PIXI.Graphics();
    SBFunction.setEmptyGraphics(b, c, a, 3158064, 1);
    var d = b.generateTexture();
    b.destroy();
    return d
};
SBControl.createInfoBoxTexture = function(c, a) {
    var b = new PIXI.Graphics();
    SBFunction.setEmptyGraphics(b, c, a, 3158064, 1);
    var d = b.generateTexture();
    b.destroy();
    return d
};
var SBFigControl = function(a) {
    this.sbViewer = a;
    this.control = new PIXI.Container();
    this.toolBox = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    this.control.addChild(this.toolBox);
    this.toolBar = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    this.control.addChild(this.toolBar);
    this.control.interactive = true;
    this.visible = false;
    this.toolBoxVisible = false;
    this.control.visible = false;
    this.figColors = [16711680, 65280, 255, 16777215, 16776960, 65535, 16711935, 0];
    this.figWidths = [1, 2, 5, 8, 11, 14];
    this.figConfig = {};
    this.figConfig.figType = this.sbViewer.sbStorage.config.figType;
    this.figConfig.figColor = this.sbViewer.sbStorage.config.figColor;
    this.figConfig.figWidth = this.sbViewer.sbStorage.config.figWidth;
    this.figConfig.figAlpha = this.sbViewer.sbStorage.config.figAlpha;
    this.figConfig.isSelectMode = false;
    this.toolBar.btExit = new SBButton(a, "", null, 1, 16777215, 0, 0);
    this.toolBar.addChild(this.toolBar.btExit.button);
    this.toolBar.btClear = new SBButton(a, "", null, 1, 16777215, 0, 0);
    this.toolBar.addChild(this.toolBar.btClear.button);
    this.toolBar.btMode = new SBButton(a, null, a.resLoader.resources.figure_type_pen.texture, 1, 0, 6316128, 1);
    this.toolBar.addChild(this.toolBar.btMode.button);
    this.toolBar.btMode.setOverLayImage(new PIXI.Sprite(a.resLoader.resources.figure_type_line_state.texture));
    this.toolBar.btSelect = new SBButton(a, null, a.resLoader.resources.figure_select.texture, 1, 0, 0, 0);
    this.toolBar.addChild(this.toolBar.btSelect.button);
    this.toolBar.btUndo = new SBButton(a, null, a.resLoader.resources.figure_undo.texture, 0, 0, 0, 0);
    this.toolBar.addChild(this.toolBar.btUndo.button);
    this.toolBar.btRedo = new SBButton(a, null, a.resLoader.resources.figure_redo.texture, 0, 0, 0, 0);
    this.toolBar.addChild(this.toolBar.btRedo.button);
    this.toolBox.btTypes = [];
    this.toolBox.btTypes[0] = new SBButton(a, null, a.resLoader.resources.figure_type_pen.texture, 0, 0, 0, 0);
    this.toolBox.btTypes[0].value = 1;
    this.toolBox.addChild(this.toolBox.btTypes[0].button);
    this.toolBox.btTypes[1] = new SBButton(a, null, a.resLoader.resources.figure_type_line.texture, 0, 0, 0, 0);
    this.toolBox.btTypes[1].value = 0;
    this.toolBox.addChild(this.toolBox.btTypes[1].button);
    this.toolBox.btTypes[2] = new SBButton(a, null, a.resLoader.resources.figure_type_keyboard.texture, 0, 0, 0, 0);
    this.toolBox.btTypes[2].value = 2;
    this.toolBox.addChild(this.toolBox.btTypes[2].button);
    this.toolBox.btTypes[3] = new SBButton(a, null, a.resLoader.resources.figure_type_pict.texture, 0, 0, 0, 0);
    this.toolBox.btTypes[3].value = 3;
    this.toolBox.addChild(this.toolBox.btTypes[3].button);
    this.toolBox.btColors = [];
    for (var b = 0; b < 8; b++) {
        this.toolBox.btColors[b] = new SBButton(a, null, SBFunction.createEmptyTexture(20, 10, this.figColors[b], 1), 0, 0, 0, 0);
        this.toolBox.btColors[b].value = this.figColors[b];
        this.toolBox.addChild(this.toolBox.btColors[b].button)
    }
    this.toolBox.btPens = [];
    for (var b = 0; b < 6; b++) {
        this.toolBox.btPens[b] = new SBButton(a, null, a.resLoader.resources.figure_pen.texture, 0, 0, 0, 0);
        this.toolBox.btPens[b].value = this.figWidths[b];
        this.toolBox.addChild(this.toolBox.btPens[b].button)
    }
    this.toolBox.btAlphas = [];
    this.toolBox.btAlphas[0] = new SBButton(a, null, a.resLoader.resources.figure_alpha2.texture, 0, 0, 0, 0);
    this.toolBox.btAlphas[0].value = 255;
    this.toolBox.addChild(this.toolBox.btAlphas[0].button);
    this.toolBox.btAlphas[1] = new SBButton(a, null, a.resLoader.resources.figure_alpha1.texture, 0, 0, 0, 0);
    this.toolBox.btAlphas[1].value = 127;
    this.toolBox.addChild(this.toolBox.btAlphas[1].button);
    if (this.sbViewer.docInfo.USRDATA == 0) {
        this.toolBox.btTypes[3].setEnabled(false)
    }
    this.setControlButtonEvents()
};
SBFigControl.prototype.setControlButtonEvents = function() {
    this.toolBar.btExit.label = this.sbViewer.lang.resStr.btn_Done;
    this.toolBar.btExit.onClick = function() {
        this.sbViewer.sbFigure.unSelectAllfigure();
        this.sbViewer.sbFigure.fixedFigureEdit();
        this.sbViewer.sbFigure.drawPages();
        this.sbViewer.changeFigureEditMode(false)
    }.bind(this);
    this.toolBar.btClear.label = this.sbViewer.lang.resStr.btn_Clear;
    this.toolBar.btClear.onClick = function() {
        this.sbViewer.sbFigure.deleteFigure()
    }.bind(this);
    this.toolBar.btMode.onClick = function() {
        this.sbViewer.sbFigure.unSelectAllfigure();
        this.sbViewer.sbFigure.drawPages();
        this.figConfig.isSelectMode = false;
        this.setToolBoxVisible(!this.toolBoxVisible);
        this.drawToolBoxSetting()
    }.bind(this);
    this.toolBar.btSelect.onClick = function() {
        if (this.sbViewer.sbFigure.textEditMode) {
            this.sbViewer.sbFigure.endTextEdit()
        }
        this.figConfig.isSelectMode = true;
        this.setToolBoxVisible(false);
        this.drawToolBoxSetting()
    }.bind(this);
    this.toolBar.btUndo.onClick = function() {
        this.sbViewer.sbFigure.execUndo()
    }.bind(this);
    this.toolBar.btRedo.onClick = function() {
        this.sbViewer.sbFigure.execRedo()
    }.bind(this);
    for (var a = 0; a < 4; a++) {
        this.toolBox.btTypes[a].onClick = function(b) {
            this.figConfig.figType = b.value;
            this.figConfig.isSelectMode = false;
            this.drawToolBoxSetting()
        }.bind(this)
    }
    for (var a = 0; a < 8; a++) {
        this.toolBox.btColors[a].onClick = function(b) {
            this.figConfig.figColor = b.value;
            this.figConfig.isSelectMode = false;
            this.drawToolBoxSetting()
        }.bind(this)
    }
    for (var a = 0; a < 6; a++) {
        this.toolBox.btPens[a].onClick = function(b) {
            this.figConfig.figWidth = b.value;
            this.figConfig.isSelectMode = false;
            this.drawToolBoxSetting()
        }.bind(this)
    }
    for (var a = 0; a < 2; a++) {
        this.toolBox.btAlphas[a].onClick = function(b) {
            this.figConfig.figAlpha = b.value;
            this.figConfig.isSelectMode = false;
            this.drawToolBoxSetting()
        }.bind(this)
    }
};
SBFigControl.prototype.setSize = function(a, o) {
    this.width = a;
    this.height = o;
    var n = this.sbViewer.config.barHeight;
    this.toolBar.texture.destroy(true);
    this.toolBar.texture = SBControl.createToolBarTexture(a, n);
    this.toolBar.originPoint = new PIXI.Point(0, o - n);
    this.toolBar.position = this.toolBar.originPoint;
    this.toolBar.btExit.setSize(0, parseInt(n * 0.75));
    this.toolBar.btExit.button.x = parseInt(n * 0.2);
    this.toolBar.btExit.button.y = parseInt((n - parseInt(n * 0.75)) / 2);
    var p = a;
    p -= (this.toolBar.btClear.setSize(0, parseInt(n * 0.75)) + parseInt(n * 0.15));
    this.toolBar.btClear.button.x = p;
    this.toolBar.btClear.button.y = parseInt((n - parseInt(n * 0.75)) / 2);
    var b = 0;
    b += this.toolBar.btMode.setSize(parseInt(n * 0.8), parseInt(n * 0.75)) + parseInt(n * 0.4);
    b += this.toolBar.btSelect.setSize(parseInt(n * 0.8), parseInt(n * 0.75)) + parseInt(n * 1);
    b += this.toolBar.btUndo.setSize(parseInt(n * 0.6), parseInt(n * 0.75)) + parseInt(n * 0.4);
    b += this.toolBar.btRedo.setSize(parseInt(n * 0.6), parseInt(n * 0.75));
    var g = parseInt((a - b) / 2);
    this.toolBar.btMode.button.x = g;
    this.toolBar.btMode.button.y = parseInt((n - parseInt(n * 0.75)) / 2);
    g += this.toolBar.btMode.button.width + parseInt(n * 0.4);
    this.toolBar.btSelect.button.x = g;
    this.toolBar.btSelect.button.y = parseInt((n - parseInt(n * 0.75)) / 2);
    g += this.toolBar.btSelect.button.width + parseInt(n * 1);
    this.toolBar.btUndo.button.x = g;
    this.toolBar.btUndo.button.y = parseInt((n - parseInt(n * 0.75)) / 2);
    g += this.toolBar.btUndo.button.width + parseInt(n * 0.4);
    this.toolBar.btRedo.button.x = g;
    this.toolBar.btRedo.button.y = parseInt((n - parseInt(n * 0.75)) / 2);
    var f = n * 6;
    var h = n * 4.5;
    this.toolBox.texture.destroy(true);
    this.toolBox.texture = SBControl.createInfoBoxTexture(f, h);
    this.toolBox.originPoint = new PIXI.Point(parseInt((a - f) / 2), o - n - parseInt(n * 0.1) - h);
    this.toolBox.position = this.toolBox.originPoint;
    this.drawToolBoxSetting();
    var m = parseInt(n * 0.15);
    var k = parseInt((f - m) / 4) - m;
    var d = parseInt((h - m) / 5) - m;
    for (var e = 0; e < 4; e++) {
        this.toolBox.btTypes[e].setSize(k, d);
        this.toolBox.btTypes[e].button.x = (m + k) * e + m;
        this.toolBox.btTypes[e].button.y = m
    }
    for (var c = 0; c < 2; c++) {
        for (var e = 0; e < 4; e++) {
            var l = c * 4 + e;
            this.toolBox.btColors[l].paddingWidth = parseInt(n * 0.1);
            this.toolBox.btColors[l].setSize(k, d);
            this.toolBox.btColors[l].button.x = (m + k) * e + m;
            this.toolBox.btColors[l].button.y = (m + d) * (c + 1) + m
        }
    }
    for (var e = 0; e < 4; e++) {
        this.toolBox.btPens[e].paddingHeight = (d - (e + 2) * d / 6.5 + d * 0.25) / 2;
        this.toolBox.btPens[e].setSize(k, d);
        this.toolBox.btPens[e].button.x = (m + k) * e + m;
        this.toolBox.btPens[e].button.y = (m + d) * 3 + m
    }
    for (var e = 4; e < 6; e++) {
        this.toolBox.btPens[e].paddingHeight = (d - (e + 2) * d / 6.5 + d * 0.25) / 2;
        this.toolBox.btPens[e].setSize(k, d);
        this.toolBox.btPens[e].button.x = (m + k) * (e - 4) + m;
        this.toolBox.btPens[e].button.y = (m + d) * 4 + m
    }
    for (var e = 0; e < 2; e++) {
        this.toolBox.btAlphas[e].setSize(k, d);
        this.toolBox.btAlphas[e].button.x = (m + k) * (e + 2) + m;
        this.toolBox.btAlphas[e].button.y = (m + d) * 4 + m
    }
};
SBFigControl.prototype.drawToolBoxSetting = function() {
    for (var a = 0; a < 4; a++) {
        this.toolBox.btTypes[a].borderWidth = this.figConfig.figType == this.toolBox.btTypes[a].value ? 2 : 0;
        this.toolBox.btTypes[a].refresh()
    }
    for (var a = 0; a < 8; a++) {
        this.toolBox.btColors[a].borderWidth = this.figConfig.figColor == this.toolBox.btColors[a].value ? 2 : 0;
        this.toolBox.btColors[a].refresh()
    }
    for (var a = 0; a < 6; a++) {
        this.toolBox.btPens[a].borderWidth = this.figConfig.figWidth == this.toolBox.btPens[a].value ? 2 : 0;
        this.toolBox.btPens[a].refresh()
    }
    for (var a = 0; a < 2; a++) {
        this.toolBox.btAlphas[a].borderWidth = this.figConfig.figAlpha == this.toolBox.btAlphas[a].value ? 2 : 0;
        this.toolBox.btAlphas[a].refresh()
    }
    if (this.figConfig.isSelectMode) {
        this.toolBar.btMode.borderWidth = 1;
        this.toolBar.btSelect.borderWidth = 2;
        this.toolBar.btMode.borderColor = 8421504;
        this.toolBar.btSelect.borderColor = 16777215
    } else {
        this.toolBar.btMode.borderWidth = 2;
        this.toolBar.btSelect.borderWidth = 1;
        this.toolBar.btMode.borderColor = 16777215;
        this.toolBar.btSelect.borderColor = 8421504
    }
    this.setPenButtonTexture(this.toolBar.btMode);
    this.toolBar.btMode.refresh();
    this.toolBar.btSelect.refresh();
    this.sbViewer.sbStorage.config.figType = this.figConfig.figType;
    this.sbViewer.sbStorage.config.figColor = this.figConfig.figColor;
    this.sbViewer.sbStorage.config.figWidth = this.figConfig.figWidth;
    this.sbViewer.sbStorage.config.figAlpha = this.figConfig.figAlpha;
    this.sbViewer.sbStorage.saveConfig()
};
SBFigControl.prototype.setVisible = function(c) {
    if (this.visible == c) {
        return
    }
    this.control.visible = true;
    var b = 300;
    var a = new SBAnime(this.toolBar);
    a.duration = b;
    a.acceleration = SBAnime.EaseOutCubic;
    if (c) {
        this.figConfig.isSelectMode = false;
        this.drawToolBoxSetting();
        this.toolBar.position = new PIXI.Point(this.toolBar.originPoint.x, this.height);
        a.positionTo = this.toolBar.originPoint.clone()
    } else {
        this.toolBar.position = this.toolBar.originPoint.clone();
        a.positionTo = new PIXI.Point(this.toolBar.originPoint.x, this.height);
        a.onEnd = function() {
            this.control.visible = false
        }.bind(this)
    }
    this.sbViewer.addAnimation(a, "figToolAnime", 0);
    this.setToolBoxVisible(c);
    this.visible = c
};
SBFigControl.prototype.setToolBoxVisible = function(c) {
    if (this.toolBoxVisible == c) {
        return
    }
    this.toolBox.visible = true;
    var b = 300;
    var a = new SBAnime(this.toolBox);
    a.duration = b;
    a.acceleration = SBAnime.EaseOutCubic;
    if (c) {
        this.toolBox.position = new PIXI.Point(this.toolBox.originPoint.x, this.height);
        a.positionTo = this.toolBox.originPoint.clone()
    } else {
        this.toolBox.position = this.toolBox.originPoint.clone();
        a.positionTo = new PIXI.Point(this.toolBox.originPoint.x, this.height);
        a.onEnd = function() {
            this.toolBox.visible = false
        }.bind(this)
    }
    this.sbViewer.addAnimation(a, "figToolBoxAnime", 0);
    this.toolBoxVisible = c
};
SBFigControl.prototype.setEnableUndoRedoButton = function() {
    this.toolBar.btUndo.setEnabled(this.sbViewer.sbFigure.historyCurrent > 0);
    this.toolBar.btRedo.setEnabled(this.sbViewer.sbFigure.historyCurrent < this.sbViewer.sbFigure.historyCount)
};
SBFigControl.prototype.setPenButtonTexture = function(c) {
    var b = this.figWidths.indexOf(this.figConfig.figWidth);
    var a = c.button.height;
    c.overLay.visible = false;
    switch (this.figConfig.figType) {
        case 0:
            c.changeTexture(this.sbViewer.resLoader.resources.figure_pen.texture, false);
            c.paddingHeight = (a - (b + 2) * a / 6.5 + a * 0.25) / 2;
            c.tint = this.figConfig.figColor;
            c.overLay.visible = true;
            break;
        case 1:
            c.changeTexture(this.sbViewer.resLoader.resources.figure_pen.texture, false);
            c.paddingHeight = (a - (b + 2) * a / 6.5 + a * 0.25) / 2;
            c.tint = this.figConfig.figColor;
            break;
        case 2:
            c.changeTexture(this.sbViewer.resLoader.resources.figure_type_keyboard.texture, false);
            c.paddingHeight = (a - (b + 2) * a / 6.5 + a * 0) / 2;
            c.tint = this.figConfig.figColor;
            break;
        case 3:
            c.changeTexture(this.sbViewer.resLoader.resources.figure_type_pict.texture, false);
            c.paddingHeight = 0;
            c.tint = 16777215;
            break
    }
    c.alpha = this.figConfig.figAlpha / 255
};
SBFigControl.createToolBarTexture = function(c, a) {
    var b = new PIXI.Graphics();
    SBFunction.setEmptyGraphics(b, c, a, 3158064, 1);
    var d = b.generateTexture();
    b.destroy();
    return d
};
var SBSlider = function(a, b, c) {
    this.sbViewer = a;
    this.container = b;
    this.callbackObserver = c;
    this.value = 0;
    this.max = 100;
    this.direction = 0;
    this.touch = false;
    this.container.interactive = true;
    this.container.buttonMode = true;
    this.track = new PIXI.Graphics();
    b.addChild(this.track);
    this._effect = new PIXI.Graphics();
    b.addChild(this._effect);
    this.handle = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    b.addChild(this.handle);
    this.width = 0;
    this.height = 0;
    this.container.mousedown = this.container.touchstart = this._onPointerAction.bind(this);
    this.container.mouseup = this.container.touchend = this._onPointerAction.bind(this);
    this.container.mousemove = this.container.touchmove = this._onPointerAction.bind(this);
    this.container.mouseupoutside = this.container.touchendoutside = this._onPointerAction.bind(this)
};
SBSlider.prototype.resize = function() {
    this.width = this.container.width;
    this.height = this.container.height;
    var a = parseInt(this.height / 2);
    this.handle.texture.destroy(true);
    this.handle.texture = SBSlider._createHandleTexture(parseInt(a * 0.6) * SBViewer.RETINASCALE);
    this.handle.anchor.x = 0.5;
    this.handle.anchor.y = 0.5;
    this.handle.scale.x = this.handle.scale.y = 1 / SBViewer.RETINASCALE;
    this.handle.position.y = a
};
SBSlider.prototype.setValue = function(c) {
    var a = parseInt(this.height / 2);
    this.track.clear();
    this.track.lineStyle(parseInt(this.height / 16), 10526880, 1);
    this.track.moveTo(a, a);
    this.track.lineTo(this.width - a, a);
    this.track.lineStyle(parseInt(this.height / 16), 5279999, 1);
    var b = 0;
    if (this.max > 1) {
        b = parseInt((this.width - a * 2) * ((c - 1) / (this.max - 1)))
    }
    if (this.direction == 0) {
        this.track.moveTo(a, a)
    } else {
        if (this.max == 1) {
            b = (this.width - a * 2)
        }
        this.track.moveTo(this.width - a, a)
    }
    this.track.lineTo(a + b, a);
    this.handle.position.x = a + b;
    this.value = c
};
SBSlider.prototype._onPointerAction = function(a) {
    if (!this.sbViewer.enabled) {
        return
    }
    if (a.data.originalEvent.button > 0) {
        return
    }
    switch (a.type) {
        case "mousedown":
        case "touchstart":
            this.touch = true;
            this._startAnimation();
            this.callbackObserver.onSliderDown.call(this.callbackObserver);
            this.sbViewer.userInteractiveAction();
            break;
        case "mouseup":
        case "touchend":
        case "mouseupoutside":
        case "touchendoutside":
            if (this.touch) {
                this.touch = false;
                this._checkValue(a);
                this.callbackObserver.onSliderEnd.call(this.callbackObserver, this.value)
            }
            break;
        case "mousemove":
        case "touchmove":
            if (this.touch) {
                if (this._checkValue(a)) {
                    this.callbackObserver.onSliderChange.call(this.callbackObserver, this.value)
                }
            }
            break
    }
};
SBSlider.prototype._startAnimation = function() {
    var a = new SBAnime(this._effect);
    a.duration = 1000;
    a.acceleration = SBAnime.EaseOutCubic;
    a.alphaTo = 0;
    a.functionTo = function(b) {
        this._effect.visible = true;
        this._effect.clear();
        this._effect.beginFill(16777215, 0.7);
        this._effect.drawCircle(this.handle.x, this.handle.y, this.height * b + this.handle.width);
        this._effect.endFill()
    }.bind(this);
    a.onEnd = function(b) {
        this._effect.alpha = 1;
        this._effect.visible = false
    }.bind(this);
    this.sbViewer.addAnimation(a, "buttonClick", 1)
};
SBSlider.prototype._checkValue = function(c) {
    var a = false;
    var e = this.container.toLocal(new PIXI.Point(c.data.global.x, c.data.global.y));
    var b = parseInt(this.height / 2);
    var d = parseInt((this.max - 1) * (e.x - b) / (this.width - b * 2) + 1.5);
    if (d < 1) {
        d = 1
    }
    if (d > this.max) {
        d = this.max
    }
    if (d != this.value) {
        this.setValue(d);
        a = true
    }
    return a
};
SBSlider._createHandleTexture = function(b) {
    var a = new PIXI.Graphics();
    a.beginFill(16777215, 1);
    a.drawCircle(b, b, b);
    a.endFill();
    var c = a.generateTexture();
    a.destroy();
    return c
};
var SBButton = function(a, d, f, c, e, g, b) {
    this.sbViewer = a;
    this.texture = f;
    this.borderWidth = c;
    this.borderColor = 12632256;
    this.label = d;
    this.onClick = function() {};
    this.textColor = e;
    this.bgColor = g;
    this.bgAlpha = b;
    this.alpha = 1;
    this.tint = 16777215;
    this.paddingWidth = 0;
    this.paddingHeight = 0;
    this.button = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    this.buttonID = SBButton.IDENTITY++;
    this._outline = new PIXI.Graphics();
    this.button.addChild(this._outline);
    this._effect = new PIXI.Graphics();
    this.button.addChild(this._effect);
    this._img = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 1));
    this.button.addChild(this._img);
    this._title = new PIXI.Text("", {});
    this.button.addChild(this._title);
    this.overLay = null;
    if (this.texture != null) {
        this._img.texture.destroy(true);
        this._img.texture = f;
        this._img.tint = this.tint
    }
    this._outline.interactive = true;
    this._outline.buttonMode = true;
    this._outline.mousedown = this._outline.touchstart = this._onPointerAction.bind(this);
    this._outline.mouseup = this._outline.touchend = this._onPointerAction.bind(this);
    this.lastAction = {
        type: "none"
    }
};
SBButton.IDENTITY = 0;
SBButton.prototype.setOverLayImage = function(a) {
    this.overLay = a;
    this.button.addChild(this.overLay)
};
SBButton.prototype.setEnabled = function(a) {
    this._outline.interactive = a;
    this._outline.buttonMode = a;
    this.button.alpha = a ? 1 : 0.3
};
SBButton.prototype.setSize = function(e, b) {
    var g = e;
    this._title.visible = (this.label != null && this.label != "");
    this._img.visible = (this.texture != null);
    if (this._title.visible) {
        this._title.tint = this.tint;
        this._title.alpha = this.alpha;
        this._title.text = this.label;
        this._title.style = SBFunction.getFont(parseInt(b * 0.5) * SBViewer.RETINASCALE, this.textColor);
        this._title.scale.x = this._title.scale.y = 1 / SBViewer.RETINASCALE;
        this._title.y = parseInt((b - this._title.height) / 2);
        if (e > 0) {
            if (e < this._title.width * 1.25) {
                this._title.scale.x = e / (this._title.width * 1.25)
            }
        } else {
            g = parseInt(this._title.width * 1.25)
        }
        this._title.x = parseInt((g - this._title.width) / 2)
    }
    if (this._img.visible) {
        this._img.tint = this.tint;
        this._img.alpha = this.alpha;
        var a = e - this.paddingWidth * 2;
        var d = b - this.paddingHeight * 2;
        var c = this.texture.width;
        var f = this.texture.height;
        if (a / d > c / f) {
            this._img.height = d;
            this._img.width = parseInt(d * c / f);
            this._img.x = parseInt((a - this._img.width) / 2) + this.paddingWidth;
            this._img.y = this.paddingHeight
        } else {
            this._img.width = a;
            this._img.height = parseInt(a * f / c);
            this._img.x = this.paddingWidth;
            this._img.y = parseInt((d - this._img.height) / 2) + this.paddingHeight
        }
    }
    if (this.overLay) {
        var a = e;
        var d = b;
        var c = this.texture.width;
        var f = this.texture.height;
        if (a / d > c / f) {
            this.overLay.height = d;
            this.overLay.width = parseInt(d * c / f);
            this.overLay.x = parseInt((a - this.overLay.width) / 2);
            this.overLay.y = 0
        } else {
            this.overLay.width = a;
            this.overLay.height = parseInt(a * f / c);
            this.overLay.x = 0;
            this.overLay.y = parseInt((d - this.overLay.height) / 2)
        }
    }
    this.button.texture.destroy(true);
    this.button.texture = SBFunction.createEmptyTexture(g, b, 0, 0);
    SBButton._setButtonGraphics(this._outline, g, b, this.borderWidth / SBViewer.PIXELRATIO, this.borderColor, this.bgColor, this.bgAlpha);
    return g
};
SBButton.prototype.refresh = function() {
    this.setSize(this.button.width, this.button.height)
};
SBButton.prototype.changeTexture = function(a, b) {
    if (b) {
        this._img.texture.destroy(true)
    }
    this._img.texture = a;
    this.texture = a
};
SBButton.prototype._onPointerAction = function(a) {
    if (!this.sbViewer.enabled) {
        return
    }
    if (a.data.originalEvent.button > 0) {
        return
    }
    if (this.lastAction.type == a.type && this.lastAction.time + 100 > new Date().getTime()) {
        return
    }
    switch (a.type) {
        case "mousedown":
        case "touchstart":
            this._startAnimation();
            this.sbViewer.userInteractiveAction();
            break;
        case "mouseup":
        case "touchend":
            this.onClick(this);
            break
    }
    this.lastAction = {
        type: a.type,
        time: new Date().getTime()
    }
};
SBButton.prototype._startAnimation = function() {
    var a = new SBAnime(this._effect);
    a.duration = 1000;
    a.acceleration = SBAnime.EaseOutCubic;
    a.alphaTo = 0;
    a.functionTo = function(b) {
        this._effect.visible = true;
        this._effect.clear();
        this._effect.beginFill(16777215, 0.7);
        this._effect.drawCircle(this.button.texture.width / 2, this.button.texture.height / 2, this.button.texture.height * b * 1.5 + this.button.texture.height);
        this._effect.endFill()
    }.bind(this);
    a.onEnd = function(b) {
        this._effect.alpha = 1;
        this._effect.visible = false
    }.bind(this);
    this.sbViewer.addAnimation(a, "buttonClick" + (this.buttonID), 1)
};
SBButton._setButtonGraphics = function(d, e, a, c, g, f, b) {
    d.clear();
    d.lineStyle(c, g, 1);
    d.beginFill(f, b);
    d.drawRect(0, 0, e, a);
    d.endFill()
};
var SBFunction = new Object();
SBFunction.createEmptyTexture = function(c, a, e, f) {
    var b = new PIXI.Graphics();
    SBFunction.setEmptyGraphics(b, c, a, e, f);
    var d = b.generateTexture();
    b.destroy();
    return d
};
SBFunction.setEmptyGraphics = function(b, c, a, d, e) {
    b.clear();
    b.beginFill(d, e);
    b.drawRect(0, 0, c, a);
    b.endFill()
};
SBFunction.getFont = function(b, a) {
    style = {
        font: parseInt(b) + "px 'Lato', 'Hiragino Kaku Gothic ProN', '繝偵Λ繧ｮ繝手ｧ偵ざ ProN W3', Meiryo, 繝｡繧､繝ｪ繧ｪ, sans-serif",
        fill: a
    };
    return style
};
SBFunction.matchAllWithHandler = function(d, e, c) {
    if (c === undefined) {
        c = function(f) {
            return f
        }
    }
    var b = [],
        a;
    while ((a = d.exec(e))) {
        b.push(c(a))
    }
    return b
};
SBFunction.substituteTemplate = function(b, a) {
    return b.replace(/\{(.+?)\}/g, function(c, d) {
        return a[d]
    })
};
SBFunction.normalize = function(a, b) {
    return parseInt(a * 10000 / b)
};
SBFunction.deNormalize = function(a, b) {
    return (a * b) / 10000
};
SBFunction.getNormalizePointFromSprite = function(c, b, a) {
    var e = c.layPage.toLocal(a, b);
    var d = c.layPage.texture;
    var f = d.width > d.height ? d.width : d.height;
    e.x = parseInt(e.x * 10000 / f);
    e.y = parseInt(e.y * 10000 / f);
    return e
};
SBFunction.convPointToRect = function(a, b) {
    return new PIXI.Rectangle(a.x - b, a.y - b, b * 2, b * 2)
};
SBFunction.rectIntersectRect = function(b, a) {
    return (b.x <= a.x + a.width && a.x <= b.x + b.width && b.y <= a.y + a.height && a.y <= b.y + b.height)
};
SBFunction.rectIntersectLine = function(l, j, h) {
    var g = j.x;
    var e = h.x;
    var n = l.x + l.width;
    var d = l.y + l.height;
    if (j.x > h.x) {
        g = h.x;
        e = j.x
    }
    if (e > n) {
        e = n
    }
    if (g < l.x) {
        g = l.x
    }
    if (g > e) {
        return false
    }
    var f = j.y;
    var c = h.y;
    var o = parseInt(h.x - j.x);
    if (Math.abs(o) > 1e-7) {
        var m = (h.y - j.y) / o;
        var k = j.y - m * j.x;
        f = m * g + k;
        c = m * e + k
    }
    if (f > c) {
        var i = c;
        c = f;
        f = i
    }
    if (c > d) {
        c = d
    }
    if (f < l.y) {
        f = l.y
    }
    if (f > c) {
        return false
    }
    return true
};
SBFunction.getFullScreenStatus = function() {
    var a = {
        enabled: false,
        value: false
    };
    a.enabled = (document.fullscreenEnabled || document.msFullscreenEnabled || document.mozFullscreenEnabled || document.webkitFullscreenEnabled) ? true : false;
    a.value = (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement);
    if (a.enabled) {
        var b = navigator.userAgent;
        if (SBFunction.chkEdgeTablet()) {
            a.enabled = false
        }
        if (b.indexOf("Safari") > 0 && (b.indexOf("iPad") > 0 || b.indexOf("iPhone") > 0)) {
            a.enabled = false
        }
        if (b.indexOf("Safari") > 0 && b.indexOf("Macintosh") > 0 && ("ontouchend" in document)) {
            a.enabled = false
        }
    }
    return a
};
SBFunction.setFullScreen = function(a) {
    if (a) {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen()
        } else {
            if (document.documentElement.msRequestFullscreen) {
                document.documentElement.msRequestFullscreen()
            } else {
                if (document.documentElement.mozRequestFullScreen) {
                    document.documentElement.mozRequestFullScreen()
                } else {
                    if (document.documentElement.webkitRequestFullscreen) {
                        document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT)
                    } else {}
                }
            }
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen()
        } else {
            if (document.msExitFullscreen) {
                document.msExitFullscreen()
            } else {
                if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen()
                } else {
                    if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen()
                    }
                }
            }
        }
    }
};
SBFunction.chkEdgeTablet = function() {
    var b = false;
    if (navigator.userAgent.match(/Edge\/([\.\d]+)/)) {
        var c = document.createElement("div");
        c.style.cssText = "width:100px;height:100px;overflow:scroll !important;position:absolute;top:-9999px";
        document.body.appendChild(c);
        var a = c.offsetWidth - c.clientWidth;
        document.body.removeChild(c);
        b = (a == 0)
    }
    return b
};
SBFunction.Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function(j) {
        var m = "";
        var d, b, g, p, c, l, k;
        var h = 0;
        j = this._utf8_encode(j);
        while (h < j.length) {
            d = j.charCodeAt(h++);
            b = j.charCodeAt(h++);
            g = j.charCodeAt(h++);
            p = d >> 2;
            c = (d & 3) << 4 | b >> 4;
            l = (b & 15) << 2 | g >> 6;
            k = g & 63;
            if (isNaN(b)) {
                l = k = 64
            } else {
                if (isNaN(g)) {
                    k = 64
                }
            }
            m = m + this._keyStr.charAt(p) + this._keyStr.charAt(c) + this._keyStr.charAt(l) + this._keyStr.charAt(k)
        }
        return m
    },
    decode: function(j) {
        var m = "";
        var d, b, g;
        var p, c, l, k;
        var h = 0;
        j = j.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (h < j.length) {
            p = this._keyStr.indexOf(j.charAt(h++));
            c = this._keyStr.indexOf(j.charAt(h++));
            l = this._keyStr.indexOf(j.charAt(h++));
            k = this._keyStr.indexOf(j.charAt(h++));
            d = p << 2 | c >> 4;
            b = (c & 15) << 4 | l >> 2;
            g = (l & 3) << 6 | k;
            m = m + String.fromCharCode(d);
            if (l != 64) {
                m = m + String.fromCharCode(b)
            }
            if (k != 64) {
                m = m + String.fromCharCode(g)
            }
        }
        m = this._utf8_decode(m);
        return m
    },
    _utf8_encode: function(c) {
        c = c.replace(/\r\n/g, "\n");
        var a = "";
        for (var d = 0; d < c.length; d++) {
            var b = c.charCodeAt(d);
            if (b < 128) {
                a += String.fromCharCode(b)
            } else {
                if (b > 127 && b < 2048) {
                    a += String.fromCharCode(b >> 6 | 192);
                    a += String.fromCharCode(b & 63 | 128)
                } else {
                    a += String.fromCharCode(b >> 12 | 224);
                    a += String.fromCharCode(b >> 6 & 63 | 128);
                    a += String.fromCharCode(b & 63 | 128)
                }
            }
        }
        return a
    },
    _utf8_decode: function(c) {
        var a = "";
        var d = 0;
        var b = c1 = c2 = 0;
        while (d < c.length) {
            b = c.charCodeAt(d);
            if (b < 128) {
                a += String.fromCharCode(b);
                d++
            } else {
                if (b > 191 && b < 224) {
                    c2 = c.charCodeAt(d + 1);
                    a += String.fromCharCode((b & 31) << 6 | c2 & 63);
                    d += 2
                } else {
                    c2 = c.charCodeAt(d + 1);
                    c3 = c.charCodeAt(d + 2);
                    a += String.fromCharCode((b & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                    d += 3
                }
            }
        }
        return a
    }
};
var SBIndex = function(a, c) {
    this.sbViewer = a;
    try {
        this.indexData = SBIndex.parser(c.getElementsByTagName("array")[0], 0)
    } catch (d) {
        this.indexData = []
    }
    if (this.indexData.length > 0) {
        for (var b = 0; b < this.indexData.length; b++) {
            this.initializeIndexList(this.indexData[b], $("#index_root"))
        }
    } else {
        $("#index_root").text(this.sbViewer.lang.resStr.msg_NoIndex)
    }
};
SBIndex.parser = function(e, g) {
    var h = $(e).children();
    var d = [];
    for (var c = 0; c < h.length; c++) {
        var b = $(h[c]).children();
        var f = {
            page: 0,
            title: "",
            isOpen: 0,
            depth: g,
            child: []
        };
        for (var a = 0; a < b.length; a += 2) {
            if ($(b[a]).text() == "title") {
                f.title = $(b[a + 1]).text()
            }
            if ($(b[a]).text() == "pageNumber") {
                f.page = parseInt($(b[a + 1]).text())
            }
            if ($(b[a]).text() == "Kids") {
                f.child = SBIndex.parser(b[a + 1], g + 1)
            }
        }
        d.push(f)
    }
    return d
};
SBIndex.prototype.initializeIndexList = function(h, g) {
    var b = SBFunction.substituteTemplate(SBIndex.INDEX_ITEM_TMPL, {
        has_children: h.child.length ? SBIndex.EXPAND_BUTTON_TMPL : "",
        index_title: h.title,
        page_num: h.page
    });
    var c = $(b),
        d = h.depth;
    if (!h.child.length) {
        d += 1
    }
    $(c).css({
        "padding-left": 16 * d
    }).appendTo(g);
    if (h.child.length) {
        var j = $("<div />", g).css({
            display: "none"
        });
        for (var e = 0; e < h.child.length; e++) {
            this.initializeIndexList(h.child[e], j)
        }
        $(g).append(j);
        var a = false;
        $(c).find(".expandBtn").click(function(i) {
            i.stopImmediatePropagation();
            if (!a) {
                $(this).parent().next().slideDown();
                $(this).addClass("rotated");
                a = true
            } else {
                $(this).parent().next().slideUp();
                $(this).removeClass("rotated");
                a = false
            }
        })
    }
    var f = this.sbViewer;
    $(c).click(function() {
        var i = $(this).data("pageNum");
        $.fancybox.close();
        f.movePage(i, false)
    })
};
SBIndex.INDEX_ITEM_TMPL = '<div class="indexItem" data-page-num="{page_num}">   {has_children}   <div class="indexTitle">{index_title}</div>   <div class="indexPageNum">{page_num}</div>   <div class="clear"></div></div>';
SBIndex.EXPAND_BUTTON_TMPL = '<div class="expandBtn">   <img src="./js/res//expandBtn.png"></div>';
var SBLinkAnnot = function(a) {
    this.linkAnnots = SBLinkAnnot.parseData(a)
};
SBLinkAnnot.borderColor = 8421631;
SBLinkAnnot.fillColor = 255;
SBLinkAnnot.fillAlpha = 0.15;
SBLinkAnnot.borderWidth = 0.5;
SBLinkAnnot.prototype.draw = function(f, d, b, h, e, a) {
    d.clear();
    var c = this.linkAnnots[f];
    if (!c) {
        return
    }
    var g = e > a ? e : a;
    c.forEach(function(k) {
        d.lineStyle(SBLinkAnnot.borderWidth, SBLinkAnnot.borderColor, 1);
        d.beginFill(SBLinkAnnot.fillColor, SBLinkAnnot.fillAlpha);
        var i = SBLinkAnnot.convNormToDev(k.posTL, g);
        var j = SBLinkAnnot.convNormToDev(k.posBR, g);
        d.drawRect(b + i.x, h + i.y, j.x - i.x, j.y - i.y);
        d.endFill()
    })
};
SBLinkAnnot.prototype.getLinkAnnot_bak = function(d, c) {
    var a = this.linkAnnots[d];
    var b = null;
    a.some(function(f) {
        var e = new PIXI.Rectangle(f.posTL.x, f.posTL.y, f.posBR.x - f.posTL.x, f.posBR.y - f.posTL.y);
        if (SBFunction.rectIntersectRect(c, e)) {
            b = f;
            return true
        }
    });
    return b
};
SBLinkAnnot.prototype.getLinkAnnot = function(d, i, e) {
    var f = null;
    var k = d.getPageSpriteFromPoint(i);
    if (k) {
        var a = new PIXI.Point(i.x - e, i.y - e);
        var g = new PIXI.Point(i.x + e, i.y + e);
        var b = SBFunction.getNormalizePointFromSprite(k, d.sbContainer.container, a);
        var j = SBFunction.getNormalizePointFromSprite(k, d.sbContainer.container, g);
        var c = new PIXI.Rectangle(b.x, b.y, j.x - b.x, j.y - b.y);
        if (k.page in this.linkAnnots) {
            var h = this.linkAnnots[k.page];
            h.some(function(m) {
                var l = new PIXI.Rectangle(m.posTL.x, m.posTL.y, m.posBR.x - m.posTL.x, m.posBR.y - m.posTL.y);
                if (SBFunction.rectIntersectRect(c, l)) {
                    f = m;
                    return true
                }
            })
        }
    }
    return f
};
SBLinkAnnot.convNormToDev = function(c, b) {
    var a = {
        x: 0,
        y: 0
    };
    a.x = parseInt((c.x * b) / 10000);
    a.y = parseInt((c.y * b) / 10000);
    return a
};
SBLinkAnnot.parseData = function(b) {
    var c = [];
    var a = /^(\d+)\t(\d+),(\d+),(\d+),(\d+)\t(-?\d+)\t(.*)$/gm;
    SBFunction.matchAllWithHandler(a, b, function(d) {
        var g = d[1],
            h = {
                x: parseInt(d[2], 10),
                y: parseInt(d[3], 10)
            },
            i = {
                x: parseInt(d[4], 10),
                y: parseInt(d[5], 10)
            },
            e = parseInt(d[6], 10),
            f = d[7];
        if (e != -1 && f !== "") {
            throw new Error('"uri" must be an empty string!')
        }
        if (e == -1 && f === "") {
            throw new Error('Missing "uri"!')
        }
        if (!c[g]) {
            c[g] = []
        }
        c[g].push({
            posTL: h,
            posBR: i,
            page: e,
            uri: f
        })
    });
    c.forEach(function(d) {
        d.sort(function(g, f) {
            var i = g.posBR.x - g.posTL.x,
                h = f.posBR.x - f.posTL.x,
                e = g.posBR.y - g.posTL.y,
                j = f.posBR.y - f.posTL.y;
            return i - h + e - j
        })
    });
    return c
};
var SBStorage = function(b, a, c) {
    this.keyPath = b;
    this.userID = a;
    this.filseq = c;
    this.config = {
        version: 2,
        curlMode: true,
        highResolution: false,
        figType: 1,
        figColor: 16711680,
        figWidth: 2,
        figAlpha: 255
    };
    this.history = [];
    this.figure = [];
    this.loadAll();
    this.currentHistory = this.getHistory(c);
    if (this.currentHistory == null) {
        this.currentHistory = {
            seq: c,
            pg: 1,
            dir: -1,
            cv: -1,
            mg: -1,
            ts: 0
        }
    }
};
SBStorage.prototype.loadAll = function() {
    this.loadConfig();
    this.loadHistory();
    this.loadFigure()
};
SBStorage.prototype.loadConfig = function() {
    var a = SBStorage.getItemFromStorage(this.keyPath + "U" + this.userID + "_cfg", this.config);
    if (!a.version) {
        a.version = 0
    }
    if (a.version < this.config.version) {
        var b = Object.keys(this.config);
        b.forEach(function(c) {
            if (!(c in a)) {
                a[c] = this.config[c]
            }
        }.bind(this));
        a.version = this.config.version;
        this.config = a;
        this.saveConfig()
    }
    this.config = a
};
SBStorage.prototype.loadHistory = function() {
    this.history = SBStorage.getItemFromStorage(this.keyPath + "U" + this.userID + "_his", this.history)
};
SBStorage.prototype.loadFigure = function() {
    this.figure = SBStorage.getItemFromStorage(this.keyPath + "U" + this.userID + "_fig_" + this.filseq, this.figure)
};
SBStorage.prototype.getHistory = function(b) {
    var a = this.history.filter(function(d, c) {
        if (d.seq == b) {
            return true
        }
    });
    if (a.length == 0) {
        return null
    } else {
        return a[0]
    }
};
SBStorage.prototype.setHistory = function(a) {
    var d = this.history.filter(function(f, e) {
        if (f.seq == a.seq) {
            return true
        }
    });
    if (d.length > 0) {
        var c = d[0];
        var b = Object.keys(c);
        b.forEach(function(e) {
            if (e in a) {
                c[e] = a[e]
            }
        })
    } else {
        this.history.unshift(a)
    }
};
SBStorage.prototype.sortHistory = function() {
    this.history.sort(function(d, c) {
        if (d.ts > c.ts) {
            return -1
        }
        if (d.ts < c.ts) {
            return 1
        }
        return 0
    });
    var a = 300;
    if (this.history.length > a) {
        this.history = this.history.slice(0, a - 1)
    }
};
SBStorage.prototype.saveAll = function() {
    this.saveConfig();
    this.saveHistory();
    this.saveFigure()
};
SBStorage.prototype.saveConfig = function() {
    SBStorage.setItemToStorage(this.keyPath + "U" + this.userID + "_cfg", this.config)
};
SBStorage.prototype.saveHistory = function() {
    SBStorage.setItemToStorage(this.keyPath + "U" + this.userID + "_his", this.history)
};
SBStorage.prototype.saveFigure = function() {
    SBStorage.setItemToStorage(this.keyPath + "U" + this.userID + "_fig_" + this.filseq, this.figure)
};
SBStorage.setItemToStorage = function(b, c) {
    var a = JSON.stringify(c);
    try {
        localStorage.setItem(b, a)
    } catch (d) {}
};
SBStorage.getItemFromStorage = function(b, c) {
    var a = localStorage.getItem(b);
    if (!a) {
        return c
    }
    return JSON.parse(a)
};
var SBFigure = function(b) {
    this.sbFigControl = b;
    this.sbViewer = b.sbViewer;
    if (this.sbViewer.docInfo.USRDATA > 0) {
        var d = this.sbViewer.resLoader.resources.figureData.data;
        this.pages = d != "" ? SBFigure.parseData(JSON.parse(d)["figures"]) : []
    } else {
        this.pages = SBFigure.parseData(this.sbViewer.sbStorage.figure)
    }
    this.loader = new PIXI.loaders.Loader(this.sbViewer.docInfo.DATAURL + this.sbViewer.docInfo.FILCD + "/", 2);
    this.touchBefore = {};
    this.editingPageInfo = null;
    this.editingFigure = null;
    this.editingPage = 0;
    this.lineStartPage = 0;
    this.lineStartPoint = new PIXI.Point(0, 0);
    this.pickedFigure = null;
    this.selectAndpickFigureAfterEdit = null;
    this.originFigure = null;
    this.textEditMode = false;
    this.historyCurrent = 0;
    this.historyCount = 0;
    this.selectCount = 0;
    this.pickerBand = {
        x1: 0,
        y1: 0,
        x2: 0,
        y2: 0,
        layer: new PIXI.Graphics()
    };
    this.sbViewer.sbContainer.drawingContainer.addChild(this.pickerBand.layer);
    this.dragPinID = -1;
    this.dragPin = new PIXI.Container();
    this.dragPin.pins = [];
    for (var c = 0; c < 4; c++) {
        var a = new PIXI.Sprite(this.sbViewer.resLoader.resources.dragpin.texture);
        a.anchor.x = a.anchor.y = 0.5;
        a.interactive = true;
        a.buttonMode = true;
        this.dragPin.addChild(a);
        this.dragPin.pins.push(a)
    }
    this.dragPin.visible = false;
    this.sbViewer.sbContainer.drawingContainer.addChild(this.dragPin);
    $(this.sbViewer.renderer.view).parent().append("<textarea id='textMemo' style='background-color:transparent;position:absolute;resize:none;overflow:hidden;display:none'></textarea>");
    this.textBox = $("#textMemo");
    $(this.sbViewer.renderer.view).parent().append("<input id='fileDlg' style='display:none' type='file' accept='image/jpeg, image/gif, image/png' name='file'>");
    this.fileDlg = $("#fileDlg")
};
SBFigure.parseData = function(d) {
    var b = [];
    var e = d.length;
    for (var c = 0; c < e; c++) {
        var a = SBFigure.createFigureFromJson(d[c]);
        if (!b[a.page]) {
            b[a.page] = []
        }
        b[a.page].push(a)
    }
    return b
};
SBFigure.createJsonFigure = function(a) {
    var c = {};
    c.FIGCOLOR = (a.alpha << 24) + a.figColor;
    c.FILPG = a.page;
    c.FIGTYPE = a.figType;
    c.FIGWIDTH = a.figWidth;
    c.FIGPOINT = "";
    switch (a.figType) {
        case 0:
        case 1:
            for (var b = 0; b < a.aryPoint.length; b++) {
                c.FIGPOINT += a.aryPoint[b][0] + "," + a.aryPoint[b][1] + ":"
            }
            break;
        case 2:
        case 3:
            c.FIGPOINT = a.aryPoint[0][0] + "," + a.aryPoint[0][1] + ":" + a.aryPoint[1][0] + "," + a.aryPoint[1][1] + ":";
            if (a.figType == 2) {
                c.FIGPOINT += SBFigure.base64EncodeSafe(a.text)
            } else {
                c.FIGPOINT += a.file
            }
            break
    }
    return c
};
SBFigure.createFigureFromJson = function(h) {
    var f = {};
    var b = parseInt(h.FIGCOLOR) >>> 0;
    f.page = parseInt(h.FILPG);
    f.figType = parseInt(h.FIGTYPE);
    f.figColor = b & 16777215;
    f.alpha = (b & 4278190080) >>> 24;
    f.figWidth = parseInt(h.FIGWIDTH);
    f.aryPoint = [];
    f.hisIn = 0;
    f.hisOut = 0;
    var g = h.FIGPOINT;
    var e = g.split(":");
    switch (f.figType) {
        case 0:
        case 1:
            f.layer = new PIXI.Graphics();
            for (var d = 0; d < e.length; d++) {
                var c = e[d].split(",");
                if (c.length == 2) {
                    f.aryPoint.push([parseInt(c[0]), parseInt(c[1])])
                }
            }
            break;
        case 2:
        case 3:
            var i = e[0].split(",");
            var a = e[1].split(",");
            f.aryPoint.push([parseInt(i[0]), parseInt(i[1])], [parseInt(a[0]), parseInt(a[1])]);
            f.layer = new PIXI.Container();
            if (f.figType == 2) {
                f.layer.picture = new PIXI.Text("", {});
                f.text = SBFigure.base64DecodeSafe(e[2])
            } else {
                f.layer.picture = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 0.5));
                f.file = e[2]
            }
            f.layer.addChild(f.layer.picture);
            f.layer.overwrap = new PIXI.Graphics();
            f.layer.addChild(f.layer.overwrap);
            break
    }
    return f
};

SBFigure.prototype.saveData = function() {
    this.sbViewer.loading(true);
    var c = [];
    var a = Object.keys(this.pages);
    a.forEach(function(d) {
        var e = this.pages[d];
        e.forEach(function(f) {
            if (this.historyCurrent >= f.hisIn && (f.hisOut == 0 || this.historyCurrent < f.hisOut)) {
                c.push(SBFigure.createJsonFigure(f))
            }
        }.bind(this))
    }.bind(this));
    if (this.sbViewer.docInfo.USRDATA > 0) {
        var b = this;
        $.ajax({
            url: "../json/postFigureData.php?filseq=" + b.sbViewer.docInfo.FILSEQ + "&web=1&api=3",
            type: "POST",
            data: {
                data: JSON.stringify(c)
            },
            complete: function() {
                this.sbViewer.loading(false)
            }.bind(b)
        })
    } else {
        this.sbViewer.sbStorage.figure = c;
        this.sbViewer.sbStorage.saveFigure();
        this.sbViewer.loading(false)
    }
};

SBFigure.prototype.fixedFigureEdit = function() {
    var b = Object.keys(this.pages);
    b.forEach(function(c) {
        var d = this.pages[c];
        var e = [];
        d.forEach(function(f) {
            if (this.historyCurrent >= f.hisIn && (f.hisOut == 0 || this.historyCurrent < f.hisOut)) {
                f.hisIn = 0;
                f.hisOut = 0;
                e.push(f)
            }
        }.bind(this));
        this.pages[c] = e
    }.bind(this));
    var a = (this.historyCurrent > 0);
    this.historyCurrent = 0;
    this.historyCount = 0;
    if (a) {
        this.saveData()
    }
};
SBFigure.prototype.interactiveCallback = function(a) {
    if (this.sbFigControl.figConfig.isSelectMode) {
        this.interactiveForSelect(a)
    } else {
        this.interactiveForEdit(a)
    }
};
SBFigure.prototype.interactiveForSelect = function(f) {
    switch (f.type) {
        case "down":
            this.selectAndpickFigureAfterEdit = null;
            this.touchBefore.x = f.x;
            this.touchBefore.y = f.y;
            if (this.textEditMode) {
                this.endTextEdit();
                this.sbViewer.refresh()
            }
            break;
        case "firstClick":
            if (this.selectAndpickFigureAfterEdit != null) {
                this.pickedFigure = this.selectAndpickFigureAfterEdit;
                this.selectCount = 1;
                this.pickedFigure.select = true;
                this.drawDragPin();
                this.selectAndpickFigureAfterEdit = null
            } else {
                var e = this.pickedFigure;
                this.unSelectAllfigure();
                var a = parseInt(this.sbViewer.config.barHeight * 0.15);
                this.pickerBand.x1 = f.x - a;
                this.pickerBand.x2 = f.x + a;
                this.pickerBand.y1 = f.y - a;
                this.pickerBand.y2 = f.y + a;
                this.pickedFigure = this.selectFigureInPickerBand(true);
                if (this.pickedFigure) {
                    if (this.pickedFigure.figType == 2 && this.pickedFigure === e) {
                        this.startTextEdit()
                    }
                    this.drawDragPin()
                }
            }
            this.drawPages();
            this.sbViewer.refresh();
            break;
        case "dragStart":
            if (this.pickedFigure) {
                this.dragPinID = -1;
                for (var d = 0; d < 4; d++) {
                    if (this.dragPin.pins[d].containsPoint(new PIXI.Point(this.touchBefore.x, this.touchBefore.y))) {
                        this.dragPinID = d;
                        this.startFigureAttrEdit();
                        break
                    }
                }
                if (this.dragPinID < 0) {
                    var a = parseInt(this.sbViewer.config.barHeight * 0.15);
                    this.pickerBand.x1 = f.x - a;
                    this.pickerBand.x2 = f.x + a;
                    this.pickerBand.y1 = f.y - a;
                    this.pickerBand.y2 = f.y + a;
                    if (this.hitTestFigureInPickerBand(this.pickedFigure)) {
                        this.startFigureAttrEdit()
                    } else {
                        this.unSelectAllfigure()
                    }
                }
            } else {
                this.unSelectAllfigure()
            }
            break;
        case "drag":
            if (this.pickedFigure) {
                if (this.dragPinID >= 0) {
                    var c = this.sbViewer.sbPageManager.getPageSprite(this.pickedFigure.page);
                    this.pinDragging(c, new PIXI.Point(f.x, f.y))
                } else {
                    var c = this.sbViewer.getPageSpriteFromPoint(new PIXI.Point(f.x, f.y));
                    if (c.page == this.pickedFigure.page) {
                        var g = SBFunction.getNormalizePointFromSprite(c, this.sbViewer.sbContainer.container, new PIXI.Point(f.x, f.y));
                        var b = SBFunction.getNormalizePointFromSprite(c, this.sbViewer.sbContainer.container, new PIXI.Point(f.dx, f.dy));
                        this.pickedFigure.aryPoint[0][0] = this.pickedFigure.orgPoint[0][0] + (g.x - b.x);
                        this.pickedFigure.aryPoint[0][1] = this.pickedFigure.orgPoint[0][1] + (g.y - b.y);
                        var h = c.layPage.width > c.layPage.height ? c.layPage.width : c.layPage.height;
                        this.pickedFigure.layer.x = parseInt(SBFunction.deNormalize(this.pickedFigure.aryPoint[0][0], h) + c.layPage.x);
                        this.pickedFigure.layer.y = parseInt(SBFunction.deNormalize(this.pickedFigure.aryPoint[0][1], h) + c.layPage.y);
                        this.drawDragPin();
                        this.sbViewer.refresh()
                    }
                }
            } else {
                if (f.x < f.dx) {
                    this.pickerBand.x1 = f.x;
                    this.pickerBand.x2 = f.dx
                } else {
                    this.pickerBand.x1 = f.dx;
                    this.pickerBand.x2 = f.x
                }
                if (f.y < f.dy) {
                    this.pickerBand.y1 = f.y;
                    this.pickerBand.y2 = f.dy
                } else {
                    this.pickerBand.y1 = f.dy;
                    this.pickerBand.y2 = f.y
                }
                this.pickerBand.layer.clear();
                this.pickerBand.layer.lineStyle(1, 33023, 0.4);
                this.pickerBand.layer.beginFill(419481855, 0.2);
                this.pickerBand.layer.drawRect(this.pickerBand.x1, this.pickerBand.y1, this.pickerBand.x2 - this.pickerBand.x1, this.pickerBand.y2 - this.pickerBand.y1);
                this.pickerBand.layer.endFill();
                this.sbViewer.refresh()
            }
            break;
        case "dragEnd":
            if (!this.pickedFigure) {
                this.pickedFigure = this.selectFigureInPickerBand(false)
            }
            this.pickerBand.layer.clear();
            this.drawDragPin();
            this.drawPages();
            this.sbViewer.refresh();
            break
    }
};

function setImage() {
    for (var b = 0; b < this.files.length; b++) {
        var a = this.files[b];
        fr = new FileReader();
        fr.onload = function(d) {
            var c = $("<img>");
            c.attr("src", d.target.result);
            c.css("height", "160px");
            $("#results").append(c)
        };
        fr.readAsDataURL(a)
    }
}

SBFigure.prototype.interactiveForEdit = function(c) {
    var i = this.sbViewer.getPageSpriteFromPoint(new PIXI.Point(c.x, c.y));
    var f = i != null ? SBFunction.getNormalizePointFromSprite(i, this.sbViewer.sbContainer.container, new PIXI.Point(c.x, c.y)) : null;
    var b = i == null ? 0 : i.page;
    switch (c.type) {
        case "down":
            this.sbFigControl.setToolBoxVisible(false);
            this.touchBefore.x = c.x;
            this.touchBefore.y = c.y;
            this.editingPage = b;
            if (this.textEditMode) {
                this.endTextEdit();
                if (this.selectAndpickFigureAfterEdit != null) {
                    this.sbFigControl.figConfig.isSelectMode = true;
                    this.sbFigControl.drawToolBoxSetting()
                }
                this.sbViewer.refresh()
            } else {
                if (b > 0) {
                    if (this.sbFigControl.figConfig.figType == 0 || this.sbFigControl.figConfig.figType == 1) {
                        this.sbViewer.sbInteraction.dragStartSensitive = 1;
                        this.sbViewer.sbInteraction.draggingSensitive = 1
                    } else {
                        if (this.sbFigControl.figConfig.figType == 2) {
                            this.pickedFigure = {};
                            this.pickedFigure.page = b;
                            this.pickedFigure.figType = this.sbFigControl.figConfig.figType;
                            this.pickedFigure.figColor = this.sbFigControl.figConfig.figColor;
                            this.pickedFigure.alpha = this.sbFigControl.figConfig.figAlpha;
                            this.pickedFigure.figWidth = (this.sbFigControl.figConfig.figWidth + 2) * 40;
                            this.pickedFigure.hisIn = this.historyCurrent;
                            this.pickedFigure.hisOut = 0;
                            this.pickedFigure.aryPoint = [];
                            this.pickedFigure.aryPoint.push([f.x, f.y], [this.pickedFigure.figWidth * 8, this.pickedFigure.figWidth]);
                            this.pickedFigure.layer = new PIXI.Container();
                            this.pickedFigure.layer.picture = new PIXI.Text("", {});
                            this.pickedFigure.text = "";
                            this.startTextEdit()
                        }
                    }
                }
            }
            break;
        case "firstClick":
            if (b > 0) {
                if (this.sbFigControl.figConfig.figType == 3) {
                    var j = {
                        x: f.x,
                        y: f.y
                    };
                    $(this.fileDlg).val("");
                    $(this.fileDlg).off("change").on("change", function(n) {
                        this.sbViewer.loading(true);
                        var o = this;
                        var m = $(this.fileDlg)[0].files[0];
                        var l = new FormData();
                        l.append("file", m);
                        l.append("filseq", this.sbViewer.docInfo.FILSEQ);
                        $.ajax({
                            url: "../ajax/ajax_PostFromFigurePicture.php",
                            type: "POST",
                            dataType: "json",
                            data: l,
                            processData: false,
                            contentType: false,
                            success: function(r) {
                                if (!r) {
                                    return this.sbViewer.loading(false)
                                }
                                var p = nH = 2000;
                                var s = parseInt(r[1]) / parseInt(r[2]);
                                if (s > 1) {
                                    nH = parseInt(p / s)
                                } else {
                                    p = parseInt(nH * s)
                                }
                                this.removeFigureUndoBuffer();
                                this.historyCurrent++;
                                this.historyCount = this.historyCurrent;
                                this.sbFigControl.setEnableUndoRedoButton();
                                this.editingFigure = {};
                                this.editingFigure.page = b;
                                this.editingFigure.figType = 3;
                                this.editingFigure.figColor = this.sbFigControl.figConfig.figColor;
                                this.editingFigure.alpha = this.sbFigControl.figConfig.figAlpha;
                                this.editingFigure.figWidth = 10;
                                this.editingFigure.hisIn = this.historyCurrent;
                                this.editingFigure.hisOut = 0;
                                this.editingFigure.aryPoint = [
                                    [j.x - parseInt(p / 2), j.y - parseInt(nH / 2)],
                                    [p, nH]
                                ];
                                this.editingFigure.layer = new PIXI.Graphics();
                                this.editingFigure.layer.picture = new PIXI.Sprite(SBFunction.createEmptyTexture(1, 1, 0, 0.5));
                                this.editingFigure.file = r[0];
                                this.editingFigure.layer.addChild(this.editingFigure.layer.picture);
                                this.editingFigure.layer.overwrap = new PIXI.Graphics();
                                this.editingFigure.layer.addChild(this.editingFigure.layer.overwrap);
                                var q = this.editingFigure;
                                this.reflectEditingFigure(i);
                                this.saveData();
                                this.sbFigControl.figConfig.isSelectMode = true;
                                this.sbFigControl.drawToolBoxSetting();
                                this.pickedFigure = q;
                                this.selectCount = 1;
                                this.pickedFigure.select = true;
                                this.drawDragPin()
                            }.bind(o),
                            error: function() {
                                this.sbViewer.loading(false)
                            }.bind(o)
                        })
                    }.bind(this));
                    $(this.fileDlg).click()
                }
            }
            break;
        case "drag":
            if (this.sbFigControl.figConfig.figType == 0 || this.sbFigControl.figConfig.figType == 1) {
                if (b > 0) {
                    if (this.editingPage > 0 && b != this.editingPage && this.sbFigControl.figConfig.figType == 1) {
                        var e = this.sbViewer.sbPageManager.getPageSprite(this.editingPage);
                        var g = SBFunction.getNormalizePointFromSprite(e, this.sbViewer.sbContainer.container, new PIXI.Point(c.x, c.y));
                        this.editingFigure.aryPoint.push([g.x, g.y]);
                        this.editingFigure.aryPoint.push([g.x, g.y]);
                        this.reflectEditingFigure(e)
                    }
                    if (this.editingFigure == null) {
                        this.removeFigureUndoBuffer();
                        this.historyCurrent++;
                        this.historyCount = this.historyCurrent;
                        this.sbFigControl.setEnableUndoRedoButton();
                        this.editingFigure = {};
                        this.editingFigure.page = b;
                        this.editingFigure.figType = this.sbFigControl.figConfig.figType;
                        this.editingFigure.figColor = this.sbFigControl.figConfig.figColor;
                        this.editingFigure.alpha = this.sbFigControl.figConfig.figAlpha;
                        this.editingFigure.figWidth = (this.sbFigControl.figConfig.figWidth - 1) * 16;
                        if (this.editingFigure.figWidth <= 0) {
                            this.editingFigure.figWidth = 8
                        }
                        this.editingFigure.hisIn = this.historyCurrent;
                        this.editingFigure.hisOut = 0;
                        this.editingFigure.layer = new PIXI.Graphics();
                        this.sbViewer.sbContainer.drawingContainer.addChild(this.editingFigure.layer);
                        var k = SBFunction.getNormalizePointFromSprite(i, this.sbViewer.sbContainer.container, new PIXI.Point(this.touchBefore.x, this.touchBefore.y));
                        this.editingFigure.aryPoint = new Array([k.x, k.y]);
                        this.editingFigure.aryPoint.push([f.x, f.y]);
                        this.lineStartPage = b;
                        this.lineStartPoint = new PIXI.Point(this.touchBefore.x, this.touchBefore.y)
                    }
                    if (this.sbFigControl.figConfig.figType == 0) {
                        var d = (Math.pow(this.lineStartPoint.x - c.x, 2) + Math.pow(this.lineStartPoint.y - c.y, 2) < Math.pow(12, 2)) ? 0 : 1;
                        if (d == 1 && Math.abs(this.lineStartPoint.x - c.x) < Math.abs(this.lineStartPoint.y - c.y)) {
                            d = 2
                        }
                        if (d == 1) {
                            this.lineStartPoint.y = c.y
                        } else {
                            if (d == 2) {
                                this.lineStartPoint.x = c.x
                            }
                        }
                        var a = this.sbViewer.getPageSpriteFromPoint(this.lineStartPoint);
                        i = this.sbViewer.sbPageManager.getPageSprite(this.lineStartPage);
                        if ((a && a.page == this.lineStartPage) || b == this.lineStartPage) {
                            if (this.lineStartPage != b) {
                                f = SBFunction.getNormalizePointFromSprite(i, this.sbViewer.sbContainer.container, new PIXI.Point(c.x, c.y))
                            }
                            var h = SBFunction.getNormalizePointFromSprite(i, this.sbViewer.sbContainer.container, new PIXI.Point(this.lineStartPoint.x, this.lineStartPoint.y));
                            this.editingFigure.aryPoint[0] = [h.x, h.y];
                            this.editingFigure.aryPoint[1] = [f.x, f.y]
                        }
                    } else {
                        this.editingFigure.aryPoint.push([f.x, f.y])
                    }
                    this.drawEditingFigure(i);
                    this.sbViewer.refresh()
                } else {
                    if (this.editingPage > 0 && this.editingFigure.aryPoint.length == 1) {
                        var e = this.sbViewer.sbPageManager.getPageSprite(this.editingPage);
                        var g = SBFunction.getNormalizePointFromSprite(e, this.sbViewer.sbContainer.container, new PIXI.Point(c.x, c.y));
                        this.editingFigure.aryPoint.push([g.x, g.y]);
                        this.editingFigure.aryPoint.push([g.x, g.y]);
                        this.reflectEditingFigure(e);
                        this.sbViewer.refresh()
                    }
                }
            }
            this.touchBefore.x = c.x;
            this.touchBefore.y = c.y;
            this.editingPage = b;
            break;
        case "dragEnd":
            if (this.editingFigure && this.editingFigure.figType == 1) {
                this.editingFigure.aryPoint.push([f.x, f.y])
            }
            if (this.sbFigControl.figConfig.figType == 0) {
                i = this.sbViewer.sbPageManager.getPageSprite(this.lineStartPage)
            }
            this.reflectEditingFigure(i);
            this.sbViewer.refresh();
            this.editingPage = 0;
            break
    }
};

SBFigure.prototype.removeFigureUndoBuffer = function() {
    var a = Object.keys(this.pages);
    a.forEach(function(b) {
        var c = this.pages[b];
        var d = [];
        c.forEach(function(e) {
            if (this.historyCurrent >= e.hisIn) {
                if (this.historyCurrent < e.hisOut) {
                    e.hisOut = 0
                }
                d.push(e)
            }
        }.bind(this));
        this.pages[b] = d
    }.bind(this))
};

SBFigure.prototype.execUndo = function() {
    if (this.textEditMode) {
        this.endTextEdit()
    }
    if (this.historyCurrent > 0) {
        this.historyCurrent--;
        this.unSelectAllfigure();
        this.drawPages();
        this.sbFigControl.setEnableUndoRedoButton()
    }
};
SBFigure.prototype.execRedo = function() {
    if (this.textEditMode) {
        this.endTextEdit()
    }
    if (this.historyCurrent < this.historyCount) {
        this.historyCurrent++;
        this.unSelectAllfigure();
        this.drawPages();
        this.sbFigControl.setEnableUndoRedoButton()
    }
};
SBFigure.prototype.deleteFigure = function() {
    if (this.textEditMode) {
        this.endTextEdit()
    }
    var b = false;
    this.removeFigureUndoBuffer();
    this.historyCurrent++;
    this.historyCount = this.historyCurrent;
    if (this.selectCount > 0) {
        var c = Object.keys(this.pages);
        c.forEach(function(d) {
            var e = this.pages[d];
            e.forEach(function(f) {
                if (f.select && (f.hisOut == 0 || f.hisOut > this.historyCurrent)) {
                    f.hisOut = this.historyCurrent;
                    f.select = false;
                    b = true
                }
            }.bind(this))
        }.bind(this))
    } else {
        var a = this.sbViewer.getDevPageMember(this.sbViewer.getCenterDevPage());
        if (a.leftPage > 0 && a.leftPage in this.pages) {
            this.pages[a.leftPage].forEach(function(d) {
                if (d.hisOut == 0 || d.hisOut > this.historyCurrent) {
                    d.hisOut = this.historyCurrent;
                    b = true
                }
            }.bind(this))
        }
        if (a.rightPage > 0 && a.leftPage != a.rightPage && a.rightPage in this.pages) {
            this.pages[a.rightPage].forEach(function(d) {
                if (d.hisOut == 0 || d.hisOut > this.historyCurrent) {
                    d.hisOut = this.historyCurrent;
                    b = true
                }
            }.bind(this))
        }
    }
    if (b) {
        this.sbFigControl.setEnableUndoRedoButton();
        this.unSelectAllfigure();
        this.drawPages()
    } else {
        this.historyCurrent--;
        this.historyCount = this.historyCurrent
    }
};
SBFigure.prototype.selectFigureInPickerBand = function(n) {
    var b = this.sbViewer.getPageFromPoint(new PIXI.Point(this.pickerBand.x1, this.pickerBand.y1));
    var a = this.sbViewer.getPageFromPoint(new PIXI.Point(this.pickerBand.x2, this.pickerBand.y2));
    if (b > a) {
        var e = b;
        b = a;
        a = e
    }
    var g = null;
    this.selectCount = 0;
    for (var d = b; d <= a; d++) {
        var m = this.sbViewer.sbPageManager.getPageSprite(d);
        var l = SBFunction.getNormalizePointFromSprite(m, this.sbViewer.sbContainer.container, new PIXI.Point(this.pickerBand.x1, this.pickerBand.y1));
        var k = SBFunction.getNormalizePointFromSprite(m, this.sbViewer.sbContainer.container, new PIXI.Point(this.pickerBand.x2, this.pickerBand.y2));
        var h = new PIXI.Rectangle(l.x, l.y, k.x - l.x, k.y - l.y);
        var c = this.pages[d];
        if (c) {
            for (var j = c.length - 1; j >= 0; j--) {
                var f = c[j];
                if (this.historyCurrent >= f.hisIn && (f.hisOut == 0 || this.historyCurrent < f.hisOut)) {
                    if (SBFigure.judgeFigureIntrsect(f, h)) {
                        this.selectCount++;
                        f.select = true;
                        if (f.figType == 2 || f.figType == 3) {
                            g = f
                        }
                        if (n) {
                            break
                        }
                    }
                }
            }
        }
    }
    if (this.selectCount != 1) {
        g = null
    }
    return g
};
SBFigure.prototype.hitTestFigureInPickerBand = function(f) {
    var d = false;
    var b = this.sbViewer.getPageFromPoint(new PIXI.Point(this.pickerBand.x1, this.pickerBand.y1));
    var a = this.sbViewer.getPageFromPoint(new PIXI.Point(this.pickerBand.x2, this.pickerBand.y2));
    if (b > a) {
        var e = b;
        b = a;
        a = e
    }
    for (var c = b; c <= a; c++) {
        var k = this.sbViewer.sbPageManager.getPageSprite(c);
        var j = SBFunction.getNormalizePointFromSprite(k, this.sbViewer.sbContainer.container, new PIXI.Point(this.pickerBand.x1, this.pickerBand.y1));
        var h = SBFunction.getNormalizePointFromSprite(k, this.sbViewer.sbContainer.container, new PIXI.Point(this.pickerBand.x2, this.pickerBand.y2));
        var g = new PIXI.Rectangle(j.x, j.y, h.x - j.x, h.y - j.y);
        if (this.historyCurrent >= f.hisIn && (f.hisOut == 0 || this.historyCurrent < f.hisOut)) {
            d = SBFigure.judgeFigureIntrsect(f, g)
        }
    }
    return d
};
SBFigure.prototype.unSelectAllfigure = function() {
    if (this.textEditMode) {
        this.endTextEdit()
    }
    this.pickerBand.layer.clear();
    if (this.selectCount == 0) {
        return
    }
    var a = Object.keys(this.pages);
    a.forEach(function(b) {
        var c = this.pages[b];
        c.forEach(function(d) {
            d.select = false
        })
    }.bind(this));
    this.selectCount = 0;
    this.pickedFigure = null;
    this.drawDragPin()
};
SBFigure.prototype.startFigureAttrEdit = function() {
    this.removeFigureUndoBuffer();
    this.historyCurrent++;
    this.historyCount = this.historyCurrent;
    this.sbFigControl.setEnableUndoRedoButton();
    var c = this.pickedFigure;
    this.pickedFigure = SBFigure.createFigureFromJson(SBFigure.createJsonFigure(this.pickedFigure));
    this.pickedFigure.hisIn = this.historyCurrent;
    this.pickedFigure.select = c.select;
    this.pickedFigure.orgPoint = c.aryPoint;
    c.hisOut = this.historyCurrent;
    c.select = false;
    if (!this.pages[this.pickedFigure.page]) {
        this.pages[this.pickedFigure.page] = []
    }
    var a = this.pages[this.pickedFigure.page].indexOf(c);
    if (a >= 0) {
        this.pages[this.pickedFigure.page].splice(a, 0, this.pickedFigure)
    } else {
        this.pages[this.pickedFigure.page].push(this.pickedFigure)
    }
    var b = this.sbViewer.sbPageManager.getPageSprite(this.pickedFigure.page);
    this.draw(b.page, b.layFigure, b.layPage.x, b.layPage.y, b.layPage.width, b.layPage.height);
    return c
};
SBFigure.prototype.resizeTextEditBox = function() {
    var d = this.sbViewer.sbPageManager.getPageSprite(this.pickedFigure.page);
    var f = d.layPage.width > d.layPage.height ? d.layPage.width : d.layPage.height;
    var c = SBFunction.deNormalize(this.pickedFigure.figWidth, f);
    var a = (d.x + SBFunction.deNormalize(this.pickedFigure.aryPoint[0][0], f) + d.layPage.x) * this.sbViewer.sbContainer.scale + this.sbViewer.sbContainer.innerContainer.x;
    var g = (d.y + SBFunction.deNormalize(this.pickedFigure.aryPoint[0][1], f) + d.layPage.y) * this.sbViewer.sbContainer.scale + this.sbViewer.sbContainer.innerContainer.y;
    var b = SBFunction.deNormalize(this.pickedFigure.aryPoint[1][0], f) * this.sbViewer.sbContainer.scale;
    var e = SBFunction.deNormalize(this.pickedFigure.aryPoint[1][1], f) * this.sbViewer.sbContainer.scale;
    $(this.textBox).css("left", a - 1);
    $(this.textBox).css("top", g - 1);
    $(this.textBox).css("width", b + 2);
    $(this.textBox).css("height", e + 2);
    $(this.textBox).css("font-family", "Consolas, Courier, Monaco,monospace");
    $(this.textBox).css("font-size", c * this.sbViewer.sbContainer.scale);
    $(this.textBox).css("line-height", 1);
    $(this.textBox).css("padding", 0);
    $(this.textBox).css("border", "border: 1px solid #0080ff;");
    $(this.textBox).css("color", "#" + ("000000" + this.pickedFigure.figColor.toString(16)).slice(-6));
    $(this.textBox).val(this.pickedFigure.text);
    $(this.textBox).off("keyup").on("keyup", function() {
        this.pickedFigure.text = $(this.textBox).val();
        var h = SBFigure.setFigureText(this.pickedFigure, c, SBFunction.deNormalize(this.pickedFigure.aryPoint[1][0], f));
        $(this.textBox).css("height", h * c * this.sbViewer.sbContainer.scale);
        this.pickedFigure.aryPoint[1][1] = SBFunction.normalize(h * c, f)
    }.bind(this))
};
SBFigure.prototype.startTextEdit = function() {
    this.textEditMode = true;
    this.resizeTextEditBox();
    $("body").css("overflow", "auto");
    $(this.textBox).css("display", "");
    $(this.textBox).focus();
    this.pickedFigure.orgText = this.pickedFigure.text;
    this.pickedFigure.layer.visible = false
};
SBFigure.prototype.endTextEdit = function() {
    this.pickedFigure.select = false;
    if (this.pickedFigure.orgText != this.pickedFigure.text) {
        if (this.pickedFigure.text == "") {
            this.removeFigureUndoBuffer();
            this.historyCurrent++;
            this.historyCount = this.historyCurrent;
            this.sbFigControl.setEnableUndoRedoButton();
            this.pickedFigure.text = this.pickedFigure.orgText;
            this.pickedFigure.hisOut = this.historyCurrent;
            this.selectAndpickFigureAfterEdit = null
        } else {
            var a = this.pickedFigure.orgText;
            var d = this.startFigureAttrEdit();
            var c = this.sbViewer.sbPageManager.getPageSprite(this.pickedFigure.page);
            var f = c.layPage.width > c.layPage.height ? c.layPage.width : c.layPage.height;
            var b = SBFunction.deNormalize(this.pickedFigure.figWidth, f);
            d.text = a;
            delete this.pickedFigure.orgText;
            var e = SBFigure.setFigureText(d, b, SBFunction.deNormalize(d.aryPoint[1][0], f));
            d.aryPoint[1][1] = SBFunction.normalize(e * b, f);
            d.layer.visible = true;
            this.selectAndpickFigureAfterEdit = this.pickedFigure
        }
    } else {
        if (this.pickedFigure.text != "") {
            this.selectAndpickFigureAfterEdit = this.pickedFigure
        }
    }
    $(this.textBox).val("");
    $(this.textBox).blur();
    $(this.textBox).css("display", "none");
    $("body").css("overflow", "hidden");
    this.pickedFigure.layer.visible = true;
    this.pickedFigure = null;
    this.textEditMode = false;
    this.sbViewer.lastTextEditTime = new Date().getTime()
};
SBFigure.prototype.drawDragPin = function() {
    if (this.pickedFigure && !this.textEditMode) {
        this.dragPin.visible = true;
        for (var c = 0; c < 4; c++) {
            var a = this.dragPin.pins[c];
            a.width = a.height = parseInt(this.sbViewer.config.barHeight * 0.7)
        }
        var b = this.sbViewer.sbPageManager.getPageSprite(this.pickedFigure.page);
        var e = {};
        var d = {};
        e.x = (b.x + this.pickedFigure.layer.x) * this.sbViewer.sbContainer.scale + this.sbViewer.sbContainer.innerContainer.x;
        e.y = (b.y + this.pickedFigure.layer.y) * this.sbViewer.sbContainer.scale + this.sbViewer.sbContainer.innerContainer.y;
        d.x = e.x + this.pickedFigure.layer.size.width * this.sbViewer.sbContainer.scale;
        d.y = e.y + this.pickedFigure.layer.size.height * this.sbViewer.sbContainer.scale;
        this.dragPin.pins[0].position = e;
        this.dragPin.pins[1].position = new PIXI.Point(d.x, e.y);
        this.dragPin.pins[2].position = new PIXI.Point(e.x, d.y);
        this.dragPin.pins[3].position = d
    } else {
        this.dragPin.visible = false
    }
};
SBFigure.prototype.pinDragging = function(g, f) {
    if (!g) {
        return
    }
    var h = SBFunction.getNormalizePointFromSprite(g, this.sbViewer.sbContainer.container, f);
    var e = {
        x: this.pickedFigure.aryPoint[0][0],
        y: this.pickedFigure.aryPoint[0][1],
        r: this.pickedFigure.aryPoint[0][0] + this.pickedFigure.aryPoint[1][0],
        b: this.pickedFigure.aryPoint[0][1] + this.pickedFigure.aryPoint[1][1]
    };
    var a = this.pickedFigure.figType == 3 ? this.pickedFigure.layer.picture.texture.width / this.pickedFigure.layer.picture.texture.height : 0;
    var d = this.pickedFigure.figWidth * 1.05 < 500 ? 500 : this.pickedFigure.figWidth * 1.05;
    switch (this.dragPinID) {
        case 0:
            if (a > 0) {
                if ((e.r - h.x) / (e.b - h.y) > a) {
                    e.x = h.x;
                    if (e.r - e.x < d) {
                        e.x = e.r - d
                    }
                    e.y = e.b - (e.r - e.x) / a
                } else {
                    e.y = h.y;
                    if (e.b - e.y < d / a) {
                        e.y = e.b - d / a
                    }
                    e.x = e.r - (e.b - e.y) * a
                }
            } else {
                e.x = h.x;
                if (e.r - e.x < d) {
                    e.x = e.r - d
                }
            }
            break;
        case 1:
            if (a > 0) {
                if ((h.x - e.x) / (e.b - h.y) > a) {
                    e.r = h.x;
                    if (e.r - e.x < d) {
                        e.r = e.x + d
                    }
                    e.y = e.b - (e.r - e.x) / a
                } else {
                    e.y = h.y;
                    if (e.b - e.y < d / a) {
                        e.y = e.b - d / a
                    }
                    e.r = e.x + (e.b - e.y) * a
                }
            } else {
                e.r = h.x;
                if (e.r - e.x < d) {
                    e.r = e.x + d
                }
                if (this.pickedFigure.figType != 2) {
                    e.y = h.y;
                    if (e.b - e.y < d) {
                        e.y = e.b - d
                    }
                }
            }
            break;
        case 2:
            if (a > 0) {
                if ((e.r - h.x) / (h.y - e.y) > a) {
                    e.x = h.x;
                    if (e.r - e.x < d) {
                        e.x = e.r - d
                    }
                    e.b = e.y + (e.r - e.x) / a
                } else {
                    e.b = h.y;
                    if (e.b - e.y < d / a) {
                        e.b = e.y + d / a
                    }
                    e.x = e.r - (e.b - e.y) * a
                }
            } else {
                e.x = h.x;
                if (e.r - e.x < d) {
                    e.x = e.r - d
                }
                e.b = h.y;
                if (e.b - e.y < d) {
                    e.b = e.y + d
                }
            }
            break;
        case 3:
            if (a > 0) {
                if ((h.x - e.x) / (h.y - e.y) > a) {
                    e.r = h.x;
                    if (e.r - e.x < d) {
                        e.r = e.x + d
                    }
                    e.b = e.y + (e.r - e.x) / a
                } else {
                    e.b = h.y;
                    if (e.b - e.y < d / a) {
                        e.b = e.y + d / a
                    }
                    e.r = e.x + (e.b - e.y) * a
                }
            } else {
                e.r = h.x;
                e.b = h.y;
                if (e.r - e.x < d) {
                    e.r = e.x + d
                }
                if (e.b - e.y < d) {
                    e.b = e.y + d
                }
            }
            break
    }
    this.pickedFigure.aryPoint[0][0] = e.x;
    this.pickedFigure.aryPoint[0][1] = e.y;
    this.pickedFigure.aryPoint[1][0] = e.r - e.x;
    this.pickedFigure.aryPoint[1][1] = e.b - e.y;
    if (this.pickedFigure.figType == 2) {
        var c = g.layPage.width > g.layPage.height ? g.layPage.width : g.layPage.height;
        var b = SBFunction.deNormalize(this.pickedFigure.figWidth, c);
        var i = SBFigure.setFigureText(this.pickedFigure, b, SBFunction.deNormalize(this.pickedFigure.aryPoint[1][0], c));
        this.pickedFigure.aryPoint[1][1] = this.pickedFigure.figWidth * i
    }
    this.drawSub(this.pickedFigure, g.layPage.x, g.layPage.y, g.layPage.width, g.layPage.height);
    this.drawDragPin();
    this.sbViewer.refresh()
};
SBFigure.prototype.drawPages = function() {
    var b = this.sbViewer.sbPageManager.pageSpritePool;
    for (var a = 0; a < b.length; a++) {
        this.sbViewer.sbPageManager.setPageChildrenFrame(b[a])
    }
};
SBFigure.prototype.draw = function(f, c, b, g, d, a) {
    c.removeChildren();
    var e = this.pages[f];
    if (!e) {
        return
    }
    e.forEach(function(h) {
        if (this.historyCurrent >= h.hisIn && (h.hisOut == 0 || this.historyCurrent < h.hisOut)) {
            c.addChild(h.layer);
            this.drawSub(h, b, g, d, a)
        }
    }.bind(this))
};
SBFigure.prototype.drawSub = function(e, h, f, a, k) {
    if (this.sbViewer.renderer.type == PIXI.RENDERER_TYPE.CANVAS) {
        this.sbViewer.renderer.context.lineJoin = "round";
        this.sbViewer.renderer.context.lineCap = "round"
    }
    var c = a > k ? a : k;
    var b = SBFunction.deNormalize(e.figWidth, c);
    if (b == 0) {
        b = 1
    }
    switch (e.figType) {
        case 0:
        case 1:
            e.layer.clear();
            if (e.select) {
                e.layer.lineStyle(b * 2, 33023, 0.5);
                for (var d = 0; d < e.aryPoint.length; d++) {
                    var j = e.aryPoint[d];
                    if (d == 0) {
                        e.layer.moveTo(SBFunction.deNormalize(j[0], c) + h, SBFunction.deNormalize(j[1], c) + f)
                    } else {
                        if (e.figType == 0) {
                            e.layer.lineTo(SBFunction.deNormalize(j[0], c) + h, SBFunction.deNormalize(j[1], c) + f)
                        } else {
                            var g = e.aryPoint[d - 1];
                            e.layer.quadraticCurveTo(SBFunction.deNormalize(g[0], c) + h, SBFunction.deNormalize(g[1], c) + f, SBFunction.deNormalize((j[0] + g[0]) / 2, c) + h, SBFunction.deNormalize((j[1] + g[1]) / 2, c) + f)
                        }
                    }
                }
                e.layer.lineStyle(b, 16777215, 0.5)
            } else {
                e.layer.lineStyle(b, e.figColor, e.alpha / 255)
            }
            for (var d = 0; d < e.aryPoint.length; d++) {
                var j = e.aryPoint[d];
                if (d == 0) {
                    e.layer.moveTo(SBFunction.deNormalize(j[0], c) + h, SBFunction.deNormalize(j[1], c) + f)
                } else {
                    if (e.figType == 0) {
                        e.layer.lineTo(SBFunction.deNormalize(j[0], c) + h, SBFunction.deNormalize(j[1], c) + f)
                    } else {
                        var g = e.aryPoint[d - 1];
                        e.layer.quadraticCurveTo(SBFunction.deNormalize(g[0], c) + h, SBFunction.deNormalize(g[1], c) + f, SBFunction.deNormalize((j[0] + g[0]) / 2, c) + h, SBFunction.deNormalize((j[1] + g[1]) / 2, c) + f)
                    }
                }
            }
            break;
        case 2:
            e.layer.position.x = parseInt(SBFunction.deNormalize(e.aryPoint[0][0], c) + h);
            e.layer.position.y = parseInt(SBFunction.deNormalize(e.aryPoint[0][1], c) + f);
            e.layer.size = {};
            e.layer.size.width = parseInt(SBFunction.deNormalize(e.aryPoint[1][0], c));
            e.layer.size.height = parseInt(SBFunction.deNormalize(e.aryPoint[1][1], c));
            SBFigure.setFigureText(e, b, SBFunction.deNormalize(e.aryPoint[1][0], c));
            e.layer.overwrap.clear();
            if (e.select) {
                e.layer.overwrap.lineStyle(SBFunction.deNormalize(20, c), 33023, 0.5);
                e.layer.overwrap.beginFill(52735, 0.3);
                e.layer.overwrap.drawRect(0, 0, e.layer.size.width, e.layer.size.height);
                e.layer.overwrap.endFill()
            }
            break;
        case 3:
            e.layer.position.x = parseInt(SBFunction.deNormalize(e.aryPoint[0][0], c) + h);
            e.layer.position.y = parseInt(SBFunction.deNormalize(e.aryPoint[0][1], c) + f);
            e.layer.size = {};
            e.layer.size.width = parseInt(SBFunction.deNormalize(e.aryPoint[1][0], c));
            e.layer.size.height = parseInt(SBFunction.deNormalize(e.aryPoint[1][1], c));
            e.layer.picture.width = e.layer.size.width;
            e.layer.picture.height = e.layer.size.height;
            e.layer.picture.alpha = e.alpha / 255;
            this.loadSpriteTexture(e);
            e.layer.overwrap.clear();
            if (e.select) {
                e.layer.overwrap.lineStyle(b * 2, 33023, 0.5);
                e.layer.overwrap.beginFill(52735, 0.3);
                e.layer.overwrap.drawRect(0, 0, e.layer.size.width, e.layer.size.height);
                e.layer.overwrap.endFill()
            } else {
                e.layer.overwrap.lineStyle(b * 2, 33023, 0.5);
                e.layer.overwrap.drawRect(0, 0, e.layer.size.width, e.layer.size.height)
            }
            break
    }
};
SBFigure.prototype.drawEditingFigure = function(a) {
    var b = this.sbViewer.sbContainer.drawingContainer.toLocal(a.layPage.position, a);
    this.drawSub(this.editingFigure, b.x, b.y, a.layPage.width * this.sbViewer.sbContainer.scale, a.layPage.height * this.sbViewer.sbContainer.scale)
};
SBFigure.prototype.reflectEditingFigure = function(b) {
    if (!this.editingFigure) {
        return
    }
    var a = this.editingFigure.page;
    if (!this.pages[a]) {
        this.pages[a] = []
    }
    this.pages[a].push(this.editingFigure);
    this.sbViewer.sbContainer.drawingContainer.removeChild(this.editingFigure.layer);
    b.layFigure.addChild(this.editingFigure.layer);
    this.drawSub(this.editingFigure, b.layPage.x, b.layPage.y, b.layPage.width, b.layPage.height);
    this.editingFigure = null
};
SBFigure.prototype.loadSpriteTexture = function(a) {
    if ((a.file in this.loader.resources)) {
        var b = a.layer.picture.width;
        var c = a.layer.picture.height;
        a.layer.picture.texture = this.loader.resources[a.file].texture;
        a.layer.picture.width = b;
        a.layer.picture.height = c;
        this.sbViewer.refresh()
    } else {
        this.loader.add(a.file, "userFiles/" + a.file).on("load", function(d, g) {
            if (g.name == a.file) {
                var e = a.layer.picture.width;
                var f = a.layer.picture.height;
                a.layer.picture.texture = g.texture;
                a.layer.picture.width = e;
                a.layer.picture.height = f;
                this.sbViewer.refresh()
            }
        }.bind(this)).load()
    }
};
SBFigure.setFigureText = function(a, d, g) {
    a.layer.picture.scale.x = a.layer.picture.scale.y = 0.5;
    a.layer.picture.style = {
        font: parseInt(d * 2) + "px Consolas, Courier, Monaco,monospace",
        fill: a.figColor,
        lineHeight: parseInt(d * 2)
    };
    a.layer.picture.alpha = a.alpha / 255;
    var f = "";
    var c = 0;
    var b = 0;
    while (c < a.text.length) {
        if ((b + 2) * d > g) {
            a.layer.picture.text = f + a.text.charAt(c);
            if (a.layer.picture.width > g) {
                f += "\n" + a.text.charAt(c);
                b = 0
            } else {
                f += a.text.charAt(c);
                b++
            }
        } else {
            f += a.text.charAt(c);
            b++
        }
        c++
    }
    a.layer.picture.text = f;
    var e = f.match(/\r\n|\n/g);
    var h = 1;
    if (e != null) {
        h = e.length + 1
    }
    return h
};
SBFigure.judgeFigureIntrsect = function(a, d) {
    var b = false;
    switch (a.figType) {
        case 0:
        case 1:
            for (var c = 0; c < a.aryPoint.length; c++) {
                if (c > 0) {
                    var g = {
                        x: a.aryPoint[c - 1][0],
                        y: a.aryPoint[c - 1][1]
                    };
                    var f = {
                        x: a.aryPoint[c][0],
                        y: a.aryPoint[c][1]
                    };
                    b = SBFunction.rectIntersectLine(d, g, f);
                    if (b) {
                        break
                    }
                }
            }
            break;
        case 2:
        case 3:
            var e = new PIXI.Rectangle(a.aryPoint[0][0], a.aryPoint[0][1], a.aryPoint[1][0], a.aryPoint[1][1]);
            b = SBFunction.rectIntersectRect(d, e);
            break
    }
    return b
};
SBFigure.base64DecodeSafe = function(b) {
    var a = b.replace(/\_/g, "+").replace(/\-/g, "/").replace(/\./g, "=");
    return window.atob ? decodeURIComponent(escape(window.atob(a))) : SBFunction.Base64.decode(a)
};
SBFigure.base64EncodeSafe = function(b) {
    var a = window.atob ? window.btoa(unescape(encodeURIComponent(b))) : SBFunction.Base64.encode(b);
    return a.replace(/\+/g, "_").replace(/\//g, "-").replace(/\=/g, ".")
};
var SBLanguage = function() {
    this.language = this._getLanguage();
    this.resStr = this._getResString();
    console.log(this.language)
};
SBLanguage.prototype._getResString = function() {
    var a = {};
    if (this.language == "ja") {
        a.btn_Close = "髢峨§繧�";
        a.btn_Meeting = " 莨夊ｭｰ ";
        a.btn_Curl = "鬆√ａ縺上ｊ";
        a.btn_Slide = "繧ｹ繝ｩ繧､繝�";
        a.btn_hasCover = "陦ｨ邏吶≠繧�";
        a.btn_noCover = "陦ｨ邏吶↑縺�";
        a.btn_Margin = "髢馴囈:蠎�＞";
        a.btn_LtoR = "蟾ｦ笆ｶ蜿ｳ";
        a.btn_RtoL = "蟾ｦ笳蜿ｳ";
        a.btn_Done = " 螳御ｺ� ";
        a.btn_Clear = "繧ｯ繝ｪ繧｢";
        a.btn_HighReso = "鬮倡判雉ｪ";
        a.btn_NormalReso = "讓呎ｺ也判雉ｪ";
        a.msg_NoIndex = "逶ｮ谺｡縺ｯ縺ゅｊ縺ｾ縺帙ｓ"
    } else {
        a.btn_Close = "Close";
        a.btn_Meeting = "Meeting";
        a.btn_Curl = "Curl";
        a.btn_Slide = "Slide";
        a.btn_hasCover = "HasCover";
        a.btn_noCover = "NoCover";
        a.btn_Wide = "WideInterval";
        a.btn_LtoR = "L笆ｶR";
        a.btn_RtoL = "L笳R";
        a.btn_Done = "Done";
        a.btn_Clear = "Clear";
        a.btn_HighReso = "High-res";
        a.btn_NormalReso = "Default-res";
        a.msg_NoIndex = "There is no index for this book."
    }
    return a
};
SBLanguage.prototype._getLanguage = function() {
    try {
        return ((window.navigator.languages && window.navigator.languages[0]) || window.navigator.language || window.navigator.userLanguage || window.navigator.browserLanguage).substr(0, 2)
    } catch (a) {
        return ""
    }
};
var g_SBDebug = null;
var SBDebug = function(a) {
    if (SBDebug.ENABLED) {
        this.container = new PIXI.Container();
        this.infoList = new Array();
        this.count = 0;
        var b = new PIXI.Graphics();
        b = new PIXI.Graphics();
        b.beginFill(0);
        b.moveTo(0, 0);
        b.lineTo(10, 0);
        b.lineTo(10, 10);
        b.lineTo(0, 10);
        b.endFill();
        b.width = 450;
        b.alpha = 0.7;
        this.backView = b;
        a.addChild(this.backView);
        a.addChild(this.container)
    }
    this.mouseEvent = {
        type: "",
        action: "",
        x: 0,
        y: 0,
        event: ""
    }
};
SBDebug.ENABLED = false;
SBDebug.prototype.setInfo = function(b, c) {
    if (!SBDebug.ENABLED) {
        return
    }
    var a = b + ":" + c;
    if (b in this.infoList) {
        this.infoList[b].text = a
    } else {
        this.infoList[b] = new PIXI.Text(a, {
            font: "14px Consolas,'Courier New',Courier,Monaco,monospace",
            fill: 2162464,
            stroke: 2162464,
            align: "left",
            strokeThickness: 0.5
        });
        this.infoList[b].x = 6;
        this.infoList[b].y = this.count * 18 + 6;
        this.count++;
        this.container.addChild(this.infoList[b]);
        this.backView.height = this.infoList[b].y + 18
    }
};
console = {
    log: function() {}
};
init();