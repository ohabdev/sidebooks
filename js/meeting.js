	//会議クラス container 0:本棚画面 1:bookView 2:旧BookView(IE8)
	var Meeting = function(container, dataurl){
		this.container = container;
		this.hasFocus = true;
		this.objMetdlg = new Array();
		this.currentMetSeq = 0;
		this.opt = {"waitTime" : 10000, "autoOpen" : false, "dataurl" : dataurl};
		$.ajaxSetup({ cache: false });
		this.chkMeetingParticipate(0);
		$(window).focus(function(){
			if(this.hasFocus) return;
			this.hasFocus = true;
			this.chkMeetingParticipate(0);
		}.bind(this));
		$(window).blur(function(){
			this.hasFocus = false;
		}.bind(this));
	}
	Meeting.prototype.chkMeetingParticipate = function(typ){
		if(!this.hasFocus) return;
		var url = "./ajax/ajax_GetMeetingInfo.php";
		if(this.container > 0) url = "../ajax/ajax_GetMeetingInfo.php";
		$.ajax({
			url : url,
			type : "get",
			data : {"typ" : typ} ,
			dataType : "json",
			success: function(aryMeeting) {
				if("METINFO" in aryMeeting){
					//会議参加中
					this.currentMetSeq = aryMeeting["METINFO"]["METSEQ"];
					if(typ == 0){
						if(this.container == 0){
							$("#headerMeeting a").css("background", "#ffffff url(images/gradation04.png) repeat-x bottom center");
						}else if(this.container == 1){
							g_sbViewer.setMeetingStatus(aryMeeting["METINFO"]);
						}else{
							$("ul#header").css("margin-left", "-230px");
							$("li#headerMeeting").css("display", "list-item");
						}
						//2秒後に実行
						setTimeout(function(){this.chkMeetingParticipate(1);}.bind(this), 2000);
					}else if(typ == 1){
						//新しいメッセージを取得
						if("MSGLIST"  in aryMeeting){
							var aryMessageList = aryMeeting["MSGLIST"];
							if(aryMessageList.length > 0){
								var msg = aryMessageList[0];
								while(this.objMetdlg.length > 0){
									this.objMetdlg[0].close(false);
									this.objMetdlg.pop();
								}
								if(msg["MSGTYPE"] == 0){
									//メッセージとして受け取る
									this.objMetdlg.push(new MetDlg(aryMeeting["METINFO"]["METTITLE"], msg, this.container, this.opt));
								}else{
									//コマンドとして受け取る
								}
							}
						}
						//2秒後に実行
						setTimeout(function(){this.chkMeetingParticipate(1);}.bind(this), 2000);
					}
				}else{
					//参加中会議は無いまたは会議終了
					this.currentMetSeq = 0;
					if(this.container == 1){
						//標準ビューア
						g_sbViewer.setMeetingStatus(null);
					}else if(this.container == 2){
						//IE8用ビューア
						$("ul#header").css("margin-left", "-186px");
						$("li#headerMeeting").css("display", "none");
					}else{
						//本棚画面
						$("#headerMeeting a").css("background", "#ffffff url(images/gradation01.png) repeat-x bottom center");
					}
					if("STATUS" in aryMeeting){
						var aryParam = MetDlg.getURLParam(location.href);
						var folseq = ("folseq" in aryParam) ? parseInt(aryParam["folseq"]) : 0;
						var filseq = ("filseq" in aryParam) ? parseInt(aryParam["filseq"]) : 0;
						location.href = (this.container > 0 ? "../" : "./")+"login.php?status="+aryMeeting["STATUS"]+"&sub="+aryMeeting["SUB"]+"&folseq="+folseq+"&filseq="+filseq;
					}
				}
			}.bind(this)
		});
	}

	//会議通知メッセージクラス
	var MetDlg = function(title, dictMsg, container, opt){
		this.title = title;
		this.dictMsg = dictMsg;
		this.container = container;
		this.domObj = $("<div>",{"class" : "metBody"});
		$(this.domObj).attr("href", "javascript:void(0)");
		$(this.domObj).on("click", function(){event.stopPropagation();this.open();}.bind(this));
		$(this.domObj).html("<div class='metTitle'></div><div class='metImage'><img src=''></div><div class='metSubject'><span class='metFrom'></span><br><br>[<span class='metName'></span>]<br><span class='metMsg'></span></div><a class='metCancel'>キャンセル</a>");
		$(".metTitle", this.domObj).text(title);
		if(this.dictMsg["FILMOD"] > 0){
			$(".metImage img", this.domObj).attr("src", opt.dataurl+this.dictMsg["FxxCD"]+"/thumb/"+("000000"+this.dictMsg["FILPG"]).slice(-6)+".jpg");
		}else{
			$(".metImage img", this.domObj).attr("src", opt.dataurl+"../images/file_locked.png");
		}
		$(".metFrom", this.domObj).text("from: "+this.dictMsg["USRNM"]);
		$(".metName", this.domObj).text(this.dictMsg["MSGTITLE"]);
		$(".metMsg", this.domObj).text(this.dictMsg["FILPG"]+"ページを閲覧してください");
		$(".metCancel", this.domObj).attr("href", "javascript:void(0)");
		$(".metCancel", this.domObj).on("click", function(){event.stopPropagation();this.close(true);}.bind(this));
		$("body").append(this.domObj);
		$(this.domObj).animate({right:"10px"}, 400, "easeOutCubic", function(){
			this.timer = setTimeout(function(){
				if(opt.autoOpen){
					this.open();
				}else{
					this.close(true);
				}
			}.bind(this), opt.waitTime);
		}.bind(this));

	};
	MetDlg.prototype.open = function(){
		if(this.dictMsg["FILMOD"] > 0){
			if(this.container == 0){
				openEBook5(this.dictMsg["FILSEQ"], this.dictMsg["FILPG"]);
			}else{
				location.href = "./?filseq="+this.dictMsg["FILSEQ"]+"&page="+this.dictMsg["FILPG"];
			}
			this.close(false);
		}else{
			var title = this.title;
			$.Zebra_Dialog("このコンテンツは存在しないか、閲覧権限がありません",{type:"error",title:rpNonTag(title)});
		}
	}
	MetDlg.prototype.close = function(animated){
		clearTimeout(this.timer);
		if(animated){
			$(this.domObj).animate({right:"-290px"}, 400, "easeInCubic", function(){
				this.domObj.remove();
			}.bind(this));
		}else{
			this.domObj.remove();
		}
	};
	MetDlg.getURLParam = function(url){
		var parameters = url.split("?");
		var params = parameters.length > 1 ? parameters[1].split("&") : [];
		var paramsArray = [];
		for (var i = 0; i < params.length; i++) {
			neet = params[i].split("=");
			paramsArray.push(neet[0]);
			paramsArray[neet[0]] = neet[1];
		}
		return paramsArray;
	};
	( function() {
		var baseEasings = {};
		$.each( [ "Quad", "Cubic", "Quart", "Quint", "Expo" ], function( i, name ) {
			baseEasings[ name ] = function( p ) {
				return Math.pow( p, i + 2 );
			};
		} );
		$.each( baseEasings, function( name, easeIn ) {
			$.easing[ "easeIn" + name ] = easeIn;
			$.easing[ "easeOut" + name ] = function( p ) {
				return 1 - easeIn( 1 - p );
			};
			$.easing[ "easeInOut" + name ] = function( p ) {
				return p < 0.5 ?
					easeIn( p * 2 ) / 2 :
					1 - easeIn( p * -2 + 2 ) / 2;
			};
		} );
	} )();
